﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Schema;
using ActivityParser;
using NUnit.Framework;
using Retrotimer.Model;

namespace ActivityParserTest
{
	/// <summary>
	/// Summary description for ActivityXmlFormatterTests
	/// </summary>
	//    [TestClass]
	[TestFixture]
	public class ActivityXmlFormatterTest
	{
		ActivityXmlFormatter sut;
		XNamespace xmlNamespace = ActivityXmlFormatter.XmlNamespace;

		static string PathToAssetsDirectory => Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets");

		#region Additional test attributes

		[SetUp]
		public void ActivityXmlFormatterTestsInitialize()
		{
			sut = new ActivityXmlFormatter();
		}

		[TearDown]
		public void ActivityXmlFormatterTestsCleanup()
		{
			sut = null;
		}

		/// <summary>
		/// Help loading XmlSchemas for validation.
		/// </summary>
		/// <param name="path">Schema path relative to Debug folder. Example: "Assets\\FakeActivities.xsd"</param>
		/// <returns>Loaded XmlSchema</returns>
		XmlSchema ReadXmlSchema(string path)
		{
			string absolutePath = Path.Combine(Environment.CurrentDirectory, path);

			XmlSchema schema = null;
			using (StreamReader reader = File.OpenText(path))
			{
				schema = XmlSchema.Read(reader, null);
			}

			return schema;
		}

		IReadOnlyList<Activity> ReadActivitiesFromHtml(string pathToHtml)
		{
			var htmlContent = File.ReadAllText(pathToHtml);
			var result = ActivityPhpParser.Parse(htmlContent);

			return result;
		}

		#endregion

		[Test]
		public void FormatXmlFromActivities_Should_ThrowArgumentNullException_When_NullPassed()
		{

			// Act
			TestDelegate test = () => sut.FormatXmlFromActivities(null);

			// Assert
			Assert.Throws<ArgumentNullException>(test);
		}

		[Test]
		public void FormatXmlFromActivities_Should_CreateXmlWithProperDeclaration_When_EmptyListPassed()
		{
			// Arrange
			var input = new List<Activity>().AsEnumerable();

			// Act
			var result = sut.FormatXmlFromActivities(input);

			// Assert
			Assert.That(result.Declaration.Encoding, Is.EqualTo("utf-8"));
			Assert.That(result.Declaration.Version, Is.EqualTo("1.0"));
			Assert.That(result.Declaration.Standalone, Is.EqualTo("yes"));
		}

		[Test]
		public void FormatXmlFromActivities_Should_CreateXmlWithoutElements_When_EmptyListPassed()
		{
			// Arrange
			var input = new List<Activity>().AsEnumerable();

			// Act
			var result = sut.FormatXmlFromActivities(input);

			// Assert
			Assert.That(result.Root.Name.LocalName, Is.EqualTo("activities"));
		}

		[Test]
		public void FormatXmlFromActivities_Should_SerializeSingleActivityElement_When_SingleActivityPassed()
		{
			// Arrange
			var input = new List<Activity>
			{
				new Activity(0, RetroPhase.GatherData, "nametst")
			}
			.AsEnumerable();

			// Act
			var result = sut.FormatXmlFromActivities(input);

			// Assert
			Assert.That(result.Root.Descendants(), Has.Exactly(1).Matches<XElement>(x => x.Name.LocalName == "activity"));
		}

		[Test]
		public void FormatXmlFromActivities_Should_SerializeActivityElements_When_MultipleActivitiesPassed()
		{
			// Arrange
			var input = new List<Activity>
			{
				new Activity(0, RetroPhase.GatherData, "nametst"),
				new Activity(0, RetroPhase.GatherData, "nametst"),
				new Activity(0, RetroPhase.GatherData, "nametst")
			}
			.AsEnumerable();

			// Act
			var result = sut.FormatXmlFromActivities(input);

			// Assert
			Assert.That(result.Root.Descendants(xmlNamespace + "activity").Count(), Is.EqualTo(3));
		}

		[Test]
		public void FormatXmlFromActivities_Should_SerializeActivityIdAsAttribute()
		{
			// Arrange
			var testId = 34;
			var input = new List<Activity>
			{
				new Activity(testId, RetroPhase.GatherData, "nametst")
			}
			.AsEnumerable();

			// Act
			var result = sut.FormatXmlFromActivities(input);

			// Assert
			Assert.That(result.Root.Descendants().First().Attributes(),
				Has.Exactly(1).Matches<XAttribute>(x => x.Name == "activityId" && x.Value == testId.ToString()));
		}

		[Test]
		[TestCase(RetroPhase.SetTheStage)]
		[TestCase(RetroPhase.GatherData)]
		[TestCase(RetroPhase.GenerateInsights)]
		[TestCase(RetroPhase.DecideWhatToDo)]
		[TestCase(RetroPhase.CloseTheRetrospective)]
		public void FormatXmlFromActivities_Should_SerializeRetroPhase(RetroPhase phaseToSerialize)
		{
			// Arrange
			var input = new List<Activity>
			{
				new Activity(0, phaseToSerialize, "")
			}
				.AsEnumerable();

			// Act
			var result = sut.FormatXmlFromActivities(input);

			// Assert
			Assert.That(result.Root.Descendants().First().Element(xmlNamespace + "phase"),
				Has.Property("Value").EqualTo(phaseToSerialize.ToString()));
		}

		[Test]
		public void FormatXmlFromActivities_Should_SerializeActivityName()
		{
			// Arrange
			var testName = "Nice test name";
			var input = new List<Activity>
			{
				new Activity(0, RetroPhase.GatherData, testName)
			}
			.AsEnumerable();

			// Act
			var result = sut.FormatXmlFromActivities(input);

			// Assert
			Assert.That(result.Root.Descendants().First().Element(xmlNamespace + "name"),
				Has.Property("Value").EqualTo(testName));
		}

		[Test]
		public void FormatXmlFromActivities_Should_ThrowXmlSchemaValidationException_When_GeneratedXmlDoesNotConformToSchema()
		{
			// Arrange
			var path = Path.Combine(PathToAssetsDirectory, "FakeActivities.xsd");
			var input = new List<Activity>().AsEnumerable();
			var sutWithXsd = new ActivityXmlFormatter(ReadXmlSchema(path));

			// Act
			TestDelegate test = () => sutWithXsd.FormatXmlFromActivities(input);

			// Assert
			Assert.Throws<XmlSchemaValidationException>(test, "generated Xml does not conform to schema");
		}

		[Test]
		public void FormatXmlFromActivities_Should_SuccessfullyValidateAgainstSchema_When_GeneratingTrivialXml()
		{
			// Arrange
			var assetsPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets");
			var path = Path.Combine(assetsPath, "Activities.xsd");
			var input = new List<Activity>().AsEnumerable();
			var schema = ReadXmlSchema(path);
			var sutWithXsd = new ActivityXmlFormatter(schema);

			// Act
			var result = sutWithXsd.FormatXmlFromActivities(input);

			// Assert
			var set = new XmlSchemaSet();
			set.Add(schema);
			result.Validate(set, null);
		}

		[Test]
		public void FormatXmlFromActivities_Should_FormatActivityCollectionXml_When_ActivityCollectionPassed()
		{
			// Arrange
			var pathWebsite = Path.Combine(PathToAssetsDirectory, "RetromatWebsite.html");
			var pathSchema = Path.Combine(PathToAssetsDirectory, "Activities.xsd");
			var input = ReadActivitiesFromHtml(pathWebsite);
			var schema = ReadXmlSchema(pathSchema);
			var sutWithXsd = new ActivityXmlFormatter(schema);

			// Act
			var result = sutWithXsd.FormatXmlFromActivities(input);

			// Assert
			var assertion = from xEl in result.Root.Descendants(xmlNamespace + "activity")
							join activity in input
							on xEl.FirstAttribute.Value equals activity.ID.ToString()
							select new { XmlElem = xEl, Activity = activity };
			Assert.That(assertion.Count(), Is.EqualTo(input.Count), "all input Activities should be serialized");
		}
	}
}
