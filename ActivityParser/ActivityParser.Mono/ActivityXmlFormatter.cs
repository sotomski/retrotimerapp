﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Schema;
using Retrotimer.Model;

namespace ActivityParser
{
	public class ActivityXmlFormatter
    {
		readonly XmlSchemaSet schemaSet;

		public const string XmlNamespace = @"http://tempuri.org/Activities.xsd";

        public ActivityXmlFormatter()
            : this(null)
        { }

        public ActivityXmlFormatter(XmlSchema schema)
        {
            schemaSet = new XmlSchemaSet();

            if (schema != null)
            {
                schemaSet.Add(schema);
            }
        }

        public XDocument FormatXmlFromActivities(IEnumerable<Activity> activities)
        {
            if (activities == null)
            {
                throw new ArgumentNullException(nameof(activities));
            }

            XNamespace ns = XmlNamespace;

            var doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement(ns + "activities",
                    new XAttribute("xmlns", ns),
                    from activity in activities
                    select new XElement(ns + "activity",
                        new XAttribute("activityId", activity.ID),
                        new XElement(ns + "phase", activity.Phase.ToString()),
                        new XElement(ns + "name", activity.Name)
				                       )
				            )
			);

            // This will throw on failed validation.
            doc.Validate(schemaSet, null);

            return doc;
        }
    }
}
