﻿using System;
using System.IO;
using System.Xml.Schema;

namespace ActivityParser
{
    class Program
    {
        const string AssetsFolderName = "Assets";
//        static readonly string RetromatHtmlPath = Path.Combine(AssetsFolderName, "RetromatWebsite.html");
        static readonly string RetromatHtmlPath = Path.Combine(AssetsFolderName, "RawActivities.php");
        static readonly string ActivityXsdPath = Path.Combine(AssetsFolderName, "Activities.xsd");
        const string XmlFilename = "Activities.xml";

        static void Main(string[] args)
        {
            var pathToHtml = Path.Combine(Environment.CurrentDirectory, RetromatHtmlPath);
            var htmlString = File.OpenText(pathToHtml).ReadToEnd();
            var parsedActivities = ActivityPhpParser.Parse(htmlString);

            string pathToSchema = Path.Combine(Environment.CurrentDirectory, ActivityXsdPath);
            XmlSchema schema = null;
            using (StreamReader reader = File.OpenText(pathToSchema))
            {
                schema = XmlSchema.Read(reader, null);
            }

            var formatter = new ActivityXmlFormatter(schema);
            var xml = formatter.FormatXmlFromActivities(parsedActivities);

            var pathToXml = Path.Combine(Environment.CurrentDirectory, XmlFilename);
            xml.Save(pathToXml);
        }
    }
}
