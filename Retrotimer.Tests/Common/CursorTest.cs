﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Retrotimer.Common;

namespace Common
{
	[TestFixture]
	public class CursorTest
	{
		[Test]
		public void Ctor_Should_ThrowArgumentNullException_When_NullPassed()
		{
			// Act
			TestDelegate test = () => new Cursor<object>(null);

			// Assert
			Assert.That(test, Throws.ArgumentNullException);
		}

		[Test]
		public void Current_Should_ReturnZero_When_ValueTypeCursorCreated()
		{
			// Arrange
			var testData = new List<int>();
			var sut = new Cursor<int>(testData);

			// Act
			var result = sut.Current;

			// Arrange
			Assert.That(result, Is.EqualTo(0));
		}

		[Test]
		public void MoveNext_Should_ReturnTrue_When_ReachedNextElement()
		{
			// Arrange
			var sut = InitSutWithObjects(5);

			// Act
			var result = sut.MoveNext();

			// Arrange
			Assert.That(result, Is.True);
		}

		[Test]
		public void MoveNext_Should_ReturnFalse_When_PassedEndOfCollection()
		{
			// Arrange
			var sut = InitSutWithObjects(1);
			sut.MoveNext();

			// Act
			var result = sut.MoveNext();

			// Arrange
			Assert.That(result, Is.False);
		}

		[Test]
		public void Current_Should_ReturnNull_When_ReferenceTypeCursorCreated()
		{
			// Arrange
			var sut = InitSutWithObjects(1);

			// Act
			var result = sut.Current;

			// Arrange
			Assert.That(result, Is.Null);
		}

		[Test]
		public void Current_Should_ReturnFirstElement_When_MoveNextExecuted()
		{
			// Arrange
			var sut = InitSutWithObjects(5);
			var first = testDataObjects.First();

			// Act
			sut.MoveNext();
			var result = sut.Current;

			// Arrange
			Assert.That(result, Is.EqualTo(first));
		}

		[Test]
		public void Current_Should_ReturnSameObject_When_StayingInPlace()
		{
			// Arrange
			var sut = InitSutWithObjects(5);

			// Act
			sut.MoveNext();
			var firstCall = sut.Current;
			var secondCall = sut.Current;

			// Arrange
			Assert.That(secondCall, Is.EqualTo(firstCall));
		}

		[Test]
		public void Current_Should_ReturnNull_When_PassedEndOfCollection()
		{
			// Arrange
			var sut = InitSutWithObjects(1);

			// Act
			sut.MoveNext();
			sut.MoveNext();
			var result = sut.Current;

			// Assert
			Assert.That(result, Is.Null);
		}

		[Test]
		public void MoveBack_Should_ReturnTrue_When_ReachedPreviousElement()
		{
			// Arrange
			var sut = InitSutWithObjects(5);

			// Act
			sut.MoveNext();
			sut.MoveNext();
			var result = sut.MovePrevious();

			// Arrange
			Assert.That(result, Is.True);
		}

		[Test]
		public void MoveBack_Should_ReturnFalse_When_PassedBeginningOfCollection()
		{
			// Arrange
			var sut = InitSutWithObjects(1);

			// Act
			sut.MoveNext();
			var result = sut.MovePrevious();

			// Arrange
			Assert.That(result, Is.False);
		}

		[Test]
		public void Current_Should_ReturnFirstElement_When_ExecutedFromSecondElement()
		{
			// Arrange
			var sut = InitSutWithObjects(5);
			var first = testDataObjects.First();

			// Act
			sut.MoveNext();
			sut.MoveNext();
			sut.MovePrevious();
			var result = sut.Current;

			// Arrange
			Assert.That(result, Is.EqualTo(first));
		}

		[Test]
		public void Current_Should_ReturnNull_When_PassedBeginningOfCollection()
		{
			// Arrange
			var sut = InitSutWithObjects(1);

			// Act
			sut.MoveNext();
			sut.MovePrevious();
			sut.MovePrevious();
			var result = sut.Current;

			// Assert
			Assert.That(result, Is.Null);
		}

		[Test]
		public void Current_ReturnsBeforeFirstElement_When_ResetExecuted()
		{
			// Arrange
			var sut = InitSutWithObjects(1);
			var first = testDataObjects.First();

			// Act
			sut.MoveNext();
			sut.Reset();
			sut.MoveNext();
			var result = sut.Current;

			// Assert
			Assert.That(result, Is.EqualTo(first));
		}

		[Test]
		public void Current_ReturnsNull_When_ResetExecuted()
		{
			// Arrange
			var sut = InitSutWithObjects(1);

			// Act
			sut.MoveNext();
			sut.Reset();
			var result = sut.Current;

			// Assert
			Assert.That(result, Is.Null);
		}

		List<object> testDataObjects;

		Cursor<object> InitSutWithObjects(int objCount)
		{
			testDataObjects = new List<object>();
			for (int i = 0; i < objCount; i++)
			{
				testDataObjects.Add(new object());
			}

			return new Cursor<object>(testDataObjects);
		}
	}
}
