﻿using System.Threading.Tasks;
using NUnit.Framework;
using Retrotimer.Services;

namespace Common
{
	[TestFixture]
    public class EmbeddedResourcesTest
    {
        [Test]
        public async Task OpenStreamToResource_Should_ReturnOpenedStream_When_ProvidedWithCorrectId()
        {
            // Arrange
            var resId = "Retrotimer.Resources.Activities.xml";
            var sut = new EmbeddedResources();

            // Act
            var result = await sut.OpenStreamToResource(resId);

            // Assert
            Assert.That(result, Is.Not.Null);
        }
    }
}
