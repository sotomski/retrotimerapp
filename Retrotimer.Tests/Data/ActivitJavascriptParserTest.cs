﻿using System.IO;
using NUnit.Framework;
using Retrotimer.Data;
using Retrotimer.Model;

namespace Data
{
	/// <summary>
	/// Simple activity source builder helpful during configuring tests.
	/// </summary>
	class ActivitySourceBuilder
    {
        public string Id = "0";

        public string PhaseFieldName = "phase";
        public string Phase = "0";

        public string NameFieldName = "name";
        public string Name = "ESVP";

		string GetActivityTemplate()
		{
			return @"all_activities[{0}] = {{ 
    {1}:     {2},
    {3}:      ""{4}"",
    }};";
		}

		public string GetConfiguredActivitySource()
        {
            return string.Format(GetActivityTemplate(), Id, 
                PhaseFieldName, Phase,
                NameFieldName, Name);
        }
    }

    [TestFixture]
    public class ActivitJavascriptParserTest
    {
		ActivitySourceBuilder activityBuilder;

		[SetUp]
        public void Initialize()
        {
            activityBuilder = new ActivitySourceBuilder();
        }

        [TearDown]
        public void Cleanup()
        {
            activityBuilder = null;
        }

        [Test]
        public void Parse_Should_ReturnEmptyList_When_NullPassed()
        {
            // Act
            var result = ActivityJavascriptParser.Parse(null);

            // Assert
            Assert.That(result, Is.Empty, "null was passed");
        }

        [Test]
        public void Parse_Should_ReturnEmptyList_When_EmptyStringPassed()
        {
            // Act
            var result = ActivityJavascriptParser.Parse(string.Empty);

            // Assert
            Assert.That(result, Is.Empty, "empty string was passed");
        }

        #region ID

        [Test]
        public void Parse_Should_SaveId_When_JsWithOneDigitIdPassed()
        {
            // Arrange
            activityBuilder.Id = "5";
            var activity = activityBuilder.GetConfiguredActivitySource();

            // Act
            var trivialList = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(trivialList, Has.Exactly(1).Matches<Activity>(x => x.ID == 6));
        }

        [Test]
        public void Parse_Should_SaveId_When_JsWithTwoDigitIdPassed()
        {
            // Arrange
            activityBuilder.Id = "10";
            var activity = activityBuilder.GetConfiguredActivitySource();

            // Act
            var trivialList = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(trivialList, Has.Exactly(1).Matches<Activity>(x => x.ID == 11));
        }

        [Test]
        public void Parse_Should_SaveId_When_JsWithThreeDigitIdPassed()
        {
            // Arrange
            activityBuilder.Id = "998";
            var activity = activityBuilder.GetConfiguredActivitySource();

            // Act
            var trivialList = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(trivialList, Has.Exactly(1).Matches<Activity>(x => x.ID == 999));
        }

        [Test]
        public void Parse_Should_Ignore_NegativeIds()
        {
            // Arrange
            activityBuilder.Id = "-14";
            var activity = activityBuilder.GetConfiguredActivitySource();

            // Act
            var activityList = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(activityList, Is.Empty, "negative Ids should be ignored");
        }

        [Test]
        public void Parse_Should_Ignore_InputWithoutAnyId()
        {
            // Arrange
            activityBuilder.Id = null;
            var activity = activityBuilder.GetConfiguredActivitySource();

            // Act
            var activityList = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(activityList, Is.Empty, "no ID has been passed at all");
        }

        [Test]
        public void Parse_Should_Ignore_InputWithMoreThanDigitsOnlyInIdField()

        {
            // Arrange
            activityBuilder.Id = "13f";
            var activity = activityBuilder.GetConfiguredActivitySource();

            // Act
            var activityList = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(activityList, Is.Empty, "non-digit character found in ID field");
        }

        #endregion

        #region Activity Phase

        // Source of mapping:
        // var phase_titles = ['Set the stage', 'Gather data', 'Generate insights', 'Decide what to do', 'Close the retrospective', 'Something completely different'];

        [Test]
        [TestCase("0", RetroPhase.SetTheStage)]
        [TestCase("1", RetroPhase.GatherData)]
        [TestCase("2", RetroPhase.GenerateInsights)]
        [TestCase("3", RetroPhase.DecideWhatToDo)]
        [TestCase("4", RetroPhase.CloseTheRetrospective)]
        public void Parse_Should_ProperlyRecognizePhases(string providedValue, RetroPhase expectedValue)
        {
            // Arrange
            activityBuilder.Phase = providedValue;
            var activity = activityBuilder.GetConfiguredActivitySource();

            // Act
            var result = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(result, Has.Exactly(1).Matches<Activity>(a => a.Phase == expectedValue));
        }

        //[TestMethod]
        //public void Parse_Should_SetPhaseToUndefined_When_FiveParsed()
        //{
        //    // Arrange
        //    activityBuilder.Phase = "5";
        //    var activity = activityBuilder.GetConfiguredActivitySource();
        //    // Act
        //    var result = ActivityJavascriptParser.Parse(activity);

        //    // Assert
        //    result.Should().ContainSingle(a => a.Phase == RetroPhase.Undefined, "five means something undefined but valid");
        //}

        [Test]
        public void Parse_Should_IgnoreSomethingCompletelyDifferentPhase()
        {
            // Arrange
            activityBuilder.Phase = "5";
            var activity = activityBuilder.GetConfiguredActivitySource();
            // Act
            var result = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(result, Is.Empty, "stage Something completely different should be ignored");
        }
            
        [Test]
        public void Parse_Should_ReturnNothing_When_NegativePhaseParsed()
        {
            // Arrange
            activityBuilder.Phase = "-3";
            var activity = activityBuilder.GetConfiguredActivitySource();
            // Act
            var result = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(result, Is.Empty, "negative phase index is invalid");
        }
            
        [Test]
        public void Parse_Should_ReturnNothing_When_IndexAboveFiveParsed()
        {
            // Arrange
            activityBuilder.Phase = "6";
            var activity = activityBuilder.GetConfiguredActivitySource();
            // Act
            var result = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(result, Is.Empty, "phase index gerater than 5 is invalid");
        }
            
        [Test]
        public void Parse_Should_ReturnNothing_When_NoPhaseIndexParsed()
        {
            // Arrange
            activityBuilder.Phase = null;
            var activity = activityBuilder.GetConfiguredActivitySource();
            // Act
            var result = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(result, Is.Empty, "phase index is obligatory");
        }
            
        [Test]
        public void Parse_Should_ReturnNothing_When_NonDigitParsed()
        {
            // Arrange
            activityBuilder.Phase = "tyf";
            var activity = activityBuilder.GetConfiguredActivitySource();
            // Act
            var result = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(result, Is.Empty, "phase index is obligatory");
        }

        #endregion

        #region Activity Name

        [Test]
        public void Parse_Should_SetName_When_NameProvided()
        {
            // Arrange
            var activity = activityBuilder.GetConfiguredActivitySource();
            // Act
            var result = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(result, Has.Exactly(1).Matches<Activity>(a => a.Name == "ESVP"), "correct name was provided and should be saved");
        }
            
        [Test]
        public void Parse_Should_IgnoreActivities_When_NoNameIsProvided()
        {
            // Arrange
            activityBuilder.NameFieldName = "nam";
            var activity = activityBuilder.GetConfiguredActivitySource();
            // Act
            var result = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(result, Is.Empty, "activities without name should be ignored");
        }
            
        [Test]
        public void Parse_Should_IgnoreActivities_When_EmptyNameIsProvided()
        {
            // Arrange
            activityBuilder.Name = "";
            var activity = activityBuilder.GetConfiguredActivitySource();

            // Act
            var result = ActivityJavascriptParser.Parse(activity);

            // Assert
            Assert.That(result, Is.Empty, "activities with empty name should be ignored");
        }

        #endregion

        #region Multiple activities (entire HTML)

        [Test]
        public void Parse_Should_BuildActivityCollection_When_MultipleActivitiesProvided()
        {
            // Arrange
            activityBuilder.Id = "1";
            var activities = activityBuilder.GetConfiguredActivitySource();
            activityBuilder.Id = "34";
            activities = string.Concat(activities, activityBuilder.GetConfiguredActivitySource());

            // Act
            var result = ActivityJavascriptParser.Parse(activities);

            // Assert
            Assert.That(result, Has.Count.EqualTo(2));
            Assert.That(result, Has.Exactly(1).Matches<Activity>(a => a.ID == 2));
            Assert.That(result, Has.Exactly(1).Matches<Activity>(a => a.ID == 35));
        }
            
        [Test]
        public void Parse_Should_FindAllActivities_When_ParsingRetromatHtmlFile()
        {
            // Arrange
            const int allExceptPhaseSomethingCompletelyDifferent = 124;
			var currentDirectoryAssets = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets");
            var path = Path.Combine(currentDirectoryAssets, "rawActivities.js");
			var htmlContent = File.ReadAllText(path);

            // Act
            var result = ActivityJavascriptParser.Parse(htmlContent);

            // Assert
            Assert.That(result, Has.Count.EqualTo(allExceptPhaseSomethingCompletelyDifferent), 
                        "all activities should be parsed");
        }

        #endregion
    }
}
