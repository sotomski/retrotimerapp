﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Retrotimer.Data;
using Retrotimer.Model;

namespace Data
{
    [TestFixture]
    public class JsonSerializerTest
    {
        JsonSerializer sut;

        [SetUp]
        public void SetUp()
        {
            sut = new JsonSerializer();
        }

        [Test]
        public void Serialize_Should_HandleSingleActivity()
        {
            // Arrange
            var input = new Activity(150, RetroPhase.CloseTheRetrospective, "Name of test activity");
            var expectedOutput = "{\"ID\":150,\"Phase\":4,\"Name\":\"Name of test activity\"}";

            // Act
            var actual = sut.Serialize(input);

            // Assert
            Assert.That(actual, Is.EqualTo(expectedOutput));
        }

        [Test]
        public void Deserialize_Should_HandleSingleActivity()
        {
			// Arrange
            var input = "{\"ID\":150,\"Phase\":4,\"Name\":\"Name of test activity\"}";
            var expectedOutput = new Activity(150, RetroPhase.CloseTheRetrospective, "Name of test activity");

            // Act
            var actual = sut.Deserialize<Activity>(input);

			// Assert
            Assert.That(actual, Is.EqualTo(expectedOutput));
        }

        [Test]
        public void Serialize_Should_HandleIEnumerableOfActivity()
        {
            // Arrange
            IEnumerable<Activity> input = new List<Activity>
            {
                new Activity(150, RetroPhase.SetTheStage, "Name of set the stage activity"),
                new Activity(1, RetroPhase.GatherData, "Name of set the gather data activity"),
                new Activity(150, RetroPhase.GenerateInsights, "Name of generate insights activity")
            };
            var expected = "[{\"ID\":150,\"Phase\":0,\"Name\":\"Name of set the stage activity\"},{\"ID\":1,\"Phase\":1,\"Name\":\"Name of set the gather data activity\"},{\"ID\":150,\"Phase\":2,\"Name\":\"Name of generate insights activity\"}]";

            // Act
            var actual = sut.Serialize(input);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

		[Test]
		public void Deserialize_Should_HandleIEnumerableOfActivity()
		{
            // Arrange
			var input = "[{\"ID\":150,\"Phase\":0,\"Name\":\"Name of set the stage activity\"},{\"ID\":1,\"Phase\":1,\"Name\":\"Name of set the gather data activity\"},{\"ID\":150,\"Phase\":2,\"Name\":\"Name of generate insights activity\"}]";
            IEnumerable<Activity> expected = new List<Activity>
            {
                new Activity(150, RetroPhase.SetTheStage, "Name of set the stage activity"),
                new Activity(1, RetroPhase.GatherData, "Name of set the gather data activity"),
                new Activity(150, RetroPhase.GenerateInsights, "Name of generate insights activity")
            };

			// Act
            var actual = sut.Deserialize<IEnumerable<Activity>>(input);

            // Assert
            Assert.That(actual, Is.EquivalentTo(expected));
		}
    }
}
