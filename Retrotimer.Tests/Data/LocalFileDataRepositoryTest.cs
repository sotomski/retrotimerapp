﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using PCLStorage;
using Retrotimer.Data;
using Retrotimer.Model;

namespace Data
{
    [TestFixture]
    public class LocalFileDataRepositoryTest
    {
        Mock<ISerializer> serializer;
        Mock<IFileSystem> fileSystem;
        Mock<IFolder> cacheFolder;
        Mock<IFile> file;

	    IDataRepository sut;

        [SetUp]
        public void SetUp()
        {
            serializer = new Mock<ISerializer>();

            file = new Mock<IFile>();
            file.Setup(f => f.OpenAsync(PCLStorage.FileAccess.ReadAndWrite, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new MemoryStream());

            cacheFolder = new Mock<IFolder>();
            cacheFolder.Setup(cf => cf.CreateFileAsync(It.IsAny<string>(), CreationCollisionOption.ReplaceExisting, It.IsAny<CancellationToken>()))
                       .ReturnsAsync(file.Object);
            cacheFolder.Setup(cf => cf.GetFilesAsync(It.IsAny<CancellationToken>()))
                       .ReturnsAsync(new List<IFile>());

            fileSystem = new Mock<IFileSystem>();
            fileSystem.Setup(fs => fs.LocalStorage.CreateFolderAsync(It.IsAny<string>(), CreationCollisionOption.OpenIfExists, It.IsAny<CancellationToken>()))
                      .ReturnsAsync(cacheFolder.Object);

            sut = new LocalFileDataRepository(fileSystem.Object, serializer.Object);
        }

        [Test]
        public void Ctor_Should_ThrowException_When_FileSystemIsNull()
        {
	        // Act
	        void Test() => new LocalFileDataRepository(null, serializer.Object);

	        // Assert
            Assert.That(Test, Throws.ArgumentNullException.With.Property("ParamName").EqualTo("fileSystem"));
        }

        [Test]
        public void Ctor_Should_ThrowException_When_SerializerIsNull()
        {
            // Act
            TestDelegate test = () => new LocalFileDataRepository(fileSystem.Object, null);

            // Assert
            Assert.That(test, Throws.ArgumentNullException.With.Property("ParamName").EqualTo("serializer"));
        }

        [Test]
        public async Task SaveAllAsync_Should_CreateCacheFolder()
        {
            // Arrange
            var testInput = new List<int>();
            const string dataFolderName = "data";

            // Act
            await sut.SaveAllAsync(testInput);

            // Assert
            fileSystem.Verify(fs => fs.LocalStorage.CreateFolderAsync(
                dataFolderName,
                CreationCollisionOption.OpenIfExists,
                It.IsAny<CancellationToken>()));
        }

        static Mock<IFile> GetFileMockWithName(string name)
        {
            var mock = new Mock<IFile>();
            mock.Setup(m => m.Name).Returns(name);
            return mock;
        }

        [Test]
        public async Task SaveAllAsync_Should_CreateFilenameWithSerializedTypeName()
        {
            // Arrange
            var testInput = new List<Activity>();
            const string expectedFilename = "Retrotimer.Model.Activity";

            // Act
            await sut.SaveAllAsync(testInput);

            // Assert
            cacheFolder.Verify(cf => cf.CreateFileAsync(
                expectedFilename,
                CreationCollisionOption.ReplaceExisting,
                It.IsAny<CancellationToken>()));
        }

        [Test]
        public async Task SaveAllAsync_Should_SaveSerializedDataToFile()
        {
            // Arrange
            var testInput = new List<int>() as IEnumerable<int>;

            var serializedInput = "Some dummy serialized input";
            var expectedFilestreamContent = Encoding.Default.GetBytes(serializedInput);
            serializer.Setup(m => m.Serialize(testInput)).Returns(serializedInput);

            var fileStream = new MemoryStream();
            file.Reset();
            file.Setup(f => f.OpenAsync(PCLStorage.FileAccess.ReadAndWrite, It.IsAny<CancellationToken>()))
                .ReturnsAsync(fileStream);

            // Act
            await sut.SaveAllAsync(testInput);

            // Assert
            serializer.Verify(m => m.Serialize(testInput));
            Assert.That(fileStream.ToArray(), Is.EquivalentTo(expectedFilestreamContent));
        }

        [Test]
        public async Task ReadAllAsync_Should_ReturnDeserializedContentOfCacheFile_When_OneWellFormedCacheFileExist()
        {
            // Arrange
            var expectedActivities = PrepareActivityDeserializationTestWithAdditionalCacheFiles(new List<IFile>());

            // Act
            var result = await sut.ReadAllAsync<Activity>();

            // Assert
            Assert.That(result, Is.EquivalentTo(expectedActivities));
        }

        [Test]
        public async Task ReadAllAsync_Should_DeserializeSpecifiedType_When_MultipleTypesCached()
		{
			// Arrange
			var expectedActivities = PrepareActivityDeserializationTestWithAdditionalCacheFiles(new List<IFile>
			{
				Mock.Of<IFile>(f => f.Name == "System.String"),
				Mock.Of<IFile>(f => f.Name == "Retrotimer.Model.RetroPhase"),
			});

			// Act
			var result = await sut.ReadAllAsync<Activity>();

			// Assert
			Assert.That(result, Is.EquivalentTo(expectedActivities));
		}

		[Test]
		public async Task ReadAllAsync_Should_ReturnEmptyCollection_When_CacheFolderDoesntExist()
		{
			// Arrange
			cacheFolder.Setup(f => f.GetFilesAsync(It.IsAny<CancellationToken>()))
					   .ReturnsAsync(new List<IFile>());

			// Act
			var result = await sut.ReadAllAsync<Activity>();

			// Assert
			Assert.That(result, Is.Empty);
		}


        List<Activity> PrepareActivityDeserializationTestWithAdditionalCacheFiles(IEnumerable<IFile> files)
		{
			var dummyContent = "Dummy bytes to be deserialized";
			var dummyBytes = Encoding.Default.GetBytes(dummyContent);
			var fileStream = new MemoryStream();
			fileStream.Write(dummyBytes, 0, dummyBytes.Length);
			fileStream.Position = 0;
			file.Setup(f => f.Name).Returns("Retrotimer.Model.Activity$" + DateTime.Now);
			file.Setup(f => f.OpenAsync(PCLStorage.FileAccess.Read, It.IsAny<CancellationToken>()))
				.ReturnsAsync(fileStream);

			var cachedFiles = new List<IFile>();
			cachedFiles.AddRange(files);
			cachedFiles.Add(file.Object);

			cacheFolder.Setup(f => f.GetFilesAsync(It.IsAny<CancellationToken>()))
					   .ReturnsAsync(cachedFiles);

			List<Activity> expectedActivities = TestData.CreateTestActivities();
			serializer.Setup(s => s.Deserialize<IEnumerable<Activity>>(dummyContent))
					  .Returns(expectedActivities);

			return expectedActivities;
		}
    }
}
