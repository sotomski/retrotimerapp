﻿using System;
using NUnit.Framework;
using Retrotimer.Model;
using Retrotimer.Data.State;

namespace Data.State
{
	[TestFixture]
	public class ActivityWithTimeboxStateTest
	{
		[Test]
		public void Ctor_Should_ConvertTimeboxToStringForEasierSerialization()
		{
			// Arrange
			var activity = new Activity();
			var timebox = TimeSpan.FromMinutes(24);
			var input = new ActivityWithTimebox(activity, timebox);

			// Act
			var sut = new ActivityWithTimeboxState(input);

			// Assert
			Assert.That(sut.Timebox, Is.EqualTo(timebox.ToString()));
		}

		[Test]
		public void ToActivityWithTimebox_Should_SimplyAssignActivity()
		{
			// Arrange
			var activity = new Activity();
			var timebox = TimeSpan.FromMinutes(24);
			var input = new ActivityWithTimebox(activity, timebox);
			var sut = new ActivityWithTimeboxState(input);

			// Act
			var result = sut.ToActivityWithTimebox();

			// Assert
			Assert.That(result.Activity, Is.EqualTo(input.Activity));
		}

		[Test]
		public void ToActivityWithTimebox_Should_ConvertTimeboxToTimeSpan()
		{
			// Arrange
			var activity = new Activity();
			var timebox = TimeSpan.FromMinutes(24);
			var input = new ActivityWithTimebox(activity, timebox);
			var sut = new ActivityWithTimeboxState(input);

			// Act
			var result = sut.ToActivityWithTimebox();

			// Assert
			Assert.That(result.Timebox, Is.EqualTo(input.Timebox));
		}

		[Test]
		public void ToActivityWithTimebox_Should_ReturnTimespanZero_When_ParsingTimeSpanNotSuccessful()
		{
			// Arrange
			var activity = new Activity();
			var timebox = TimeSpan.FromMinutes(24);
			var input = new ActivityWithTimebox(activity, timebox);
			var sut = new ActivityWithTimeboxState(input);
			sut.Timebox = "be sure to cause error";

			// Act
			var result = sut.ToActivityWithTimebox();

			// Assert
			Assert.That(result.Timebox, Is.EqualTo(TimeSpan.Zero));
		}
	}
}
