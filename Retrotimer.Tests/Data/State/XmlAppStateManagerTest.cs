﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using PCLStorage;
using Retrotimer.Data.State;

namespace Data.State
{
    [TestFixture]
	public class XmlAppStateManagerTest
	{
		XmlAppStateManager sut;
		const string testPath = "My/Mocked/Test/Path/RetrospectivePlan.xml";
		Mock<IFile> fileMock;
		Mock<IFolder> folderMock;
		Mock<IFileSystem> fileSystemMock;

		[SetUp]
		public void Setup()
		{
			fileMock = new Mock<IFile>();
			folderMock = new Mock<IFolder>();
			fileSystemMock = new Mock<IFileSystem>();

			folderMock.Setup(fm => fm.CreateFileAsync(It.IsAny<string>(), It.IsAny<CreationCollisionOption>(), default(CancellationToken)))
				  .ReturnsAsync(fileMock.Object);
			fileSystemMock.Setup(sm => sm.GetFolderFromPathAsync(It.IsAny<string>(), default(CancellationToken)))
					  .ReturnsAsync(folderMock.Object);

			sut = new XmlAppStateManager(testPath, fileSystemMock.Object);
		}

		[Test]
		public void Ctor_Should_ThrowArgumentNullException_When_FileSystemIsNull()
		{
			// Act
			TestDelegate test = () => new XmlAppStateManager(null, null);

			// Assert
			Assert.That(test, Throws.ArgumentNullException.With.Property(nameof(ArgumentNullException.ParamName)).EqualTo("fileSystem"));
		}

		[Test]
		public async Task SaveAsync_Should_CreateNewFile()
		{
			// Arrange
			var testState = XmlTestDataGenerator.GetTestRetroPlanStateAndExpectedResult().Item1;
			var expectedFilename = Path.GetFileName(testPath);

			fileMock.Setup(ffm => ffm.OpenAsync(It.IsAny<PCLStorage.FileAccess>(), default(CancellationToken)))
				.ReturnsAsync(new MemoryStream());

			// Act
			await sut.SaveAsync(testState);

			// Assert
			folderMock.Verify(fm => fm.CreateFileAsync(expectedFilename, CreationCollisionOption.ReplaceExisting, It.IsAny<CancellationToken>()));
		}

		[Test]
		public async Task SaveAsync_Should_SaveXmlToTheFile()
		{
			// Arrange
			var testData = XmlTestDataGenerator.GetTestRetroPlanStateAndExpectedResult();
			var buff = new byte[testData.Item2.Length];
			var stream = new MemoryStream(buff);

			fileMock.Setup(ffm => ffm.OpenAsync(It.IsAny<PCLStorage.FileAccess>(), default(CancellationToken)))
					.ReturnsAsync(stream);

			// Act
			await sut.SaveAsync(testData.Item1);
			var result = Encoding.Default.GetString(buff);

			// Assert
			Assert.That(result, Is.EqualTo(testData.Item2));
		}

		[Test]
		public void SaveAync_Should_NotThrow_When_SerializerThrowsExceptions()
		{
			// Arrange
			var testData = XmlTestDataGenerator.GetTestRetroPlanStateAndExpectedResult();
			var tooShortStream = new MemoryStream(new byte[1]);
			fileMock.Setup(ffm => ffm.OpenAsync(It.IsAny<PCLStorage.FileAccess>(), default(CancellationToken)))
					.ReturnsAsync(tooShortStream);

			// Act
			AsyncTestDelegate test = async () => await sut.SaveAsync(testData.Item1);

			// Assert
			Assert.DoesNotThrowAsync(test);
		}

		[Test]
		public async Task ReadAsync_Should_ReadRetroStateFromFile_When_FileExist()
		{
			// Arrange
			var testData = XmlTestDataGenerator.GetTestRetroPlanStateAndExpectedResult();
			var expectedPlan = testData.Item1;
			var buff = Encoding.Default.GetBytes(testData.Item2);
			var stream = new MemoryStream(buff);
			fileMock.Setup(fm => fm.OpenAsync(PCLStorage.FileAccess.Read, default(CancellationToken)))
					.ReturnsAsync(stream);
			fileSystemMock.Setup(fsm => fsm.GetFileFromPathAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
						  .ReturnsAsync(fileMock.Object);

			// Act
			var result = await sut.ReadAsync();

			// Assert
			Assert.That(result.ToRetrospectivePlan(), Is.EqualTo(expectedPlan.ToRetrospectivePlan()));
			Assert.That(result.ToVisualStyles(), Is.EquivalentTo(expectedPlan.ToVisualStyles()));
		}

		[Test]
		public void ReadAsync_Should_ThrowStateStorageException_When_FileDoesNotExist()
		{
			// Arrange

			//http://www.nudoq.org/#!/Packages/PCLStorage/PCLStorage.Abstractions/IFileSystem/M/GetFileFromPathAsync
			fileSystemMock.Setup(fsm => fsm.GetFileFromPathAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
						  .ReturnsAsync(() => null);

			// Act
			AsyncTestDelegate test = async () => await sut.ReadAsync();

			// Assert
			Assert.ThrowsAsync(typeof(StateStorageException), test);
		}

		[Test]
		public void ReadAsync_Should_ThrowStateStorageException_When_XmlMalformed()
		{
			// Arrange
			var trashBytes = Encoding.Default.GetBytes("not XML at all");
			var stream = new MemoryStream(trashBytes);
			fileMock.Setup(fm => fm.OpenAsync(PCLStorage.FileAccess.Read, default(CancellationToken)))
					.ReturnsAsync(stream);
			fileSystemMock.Setup(fsm => fsm.GetFileFromPathAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
						  .ReturnsAsync(fileMock.Object);

			// Act
			AsyncTestDelegate test = async () => await sut.ReadAsync();

			// Assert
			Assert.ThrowsAsync(typeof(StateStorageException), test);
		}
	}
}
