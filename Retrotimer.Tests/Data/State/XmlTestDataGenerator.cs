﻿using System;
using System.Collections.Generic;
using Retrotimer.Data.State;
using Retrotimer.Model;
using Retrotimer.ViewModel;

namespace Data.State
{
    static class XmlTestDataGenerator
	{
		internal static Tuple<RetroPlanState, string> GetTestRetroPlanStateAndExpectedResult()
		{
			var plan = new RetrospectivePlan
			{
				SetTheStage = new ActivityWithTimebox(new Activity(1, RetroPhase.SetTheStage, "TestName STS"), TimeSpan.FromMinutes(11)),
				GatherData = new ActivityWithTimebox(new Activity(2, RetroPhase.GatherData, "TestName GD"), TimeSpan.FromMinutes(22)),
				GenerateInsights = new ActivityWithTimebox(new Activity(3, RetroPhase.GenerateInsights, "TestName GI"), TimeSpan.FromMinutes(33)),
				DecideWhatToDo = new ActivityWithTimebox(new Activity(4, RetroPhase.DecideWhatToDo, "TestName D"), TimeSpan.FromMinutes(44)),
				CloseTheRetrospective = new ActivityWithTimebox(new Activity(5, RetroPhase.CloseTheRetrospective, "TestName C"), TimeSpan.FromMinutes(55))
			};

			var styles = new List<VisualStyle>
			{
				VisualStyle.LightOrange,
				VisualStyle.LightBlue,
				VisualStyle.DarkBlue,
				VisualStyle.DarkOrange,
				VisualStyle.Blue
			};

			var state = new RetroPlanState(plan, styles);

			var expected = @"<?xml version=""1.0""?>
<RetroPlanState xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
  <SetTheStage>
    <Activity>
      <ID>1</ID>
      <Phase>SetTheStage</Phase>
      <Name>TestName STS</Name>
    </Activity>
    <Timebox>00:11:00</Timebox>
  </SetTheStage>
  <GatherData>
    <Activity>
      <ID>2</ID>
      <Phase>GatherData</Phase>
      <Name>TestName GD</Name>
    </Activity>
    <Timebox>00:22:00</Timebox>
  </GatherData>
  <GenerateInsights>
    <Activity>
      <ID>3</ID>
      <Phase>GenerateInsights</Phase>
      <Name>TestName GI</Name>
    </Activity>
    <Timebox>00:33:00</Timebox>
  </GenerateInsights>
  <DecideWhatToDo>
    <Activity>
      <ID>4</ID>
      <Phase>DecideWhatToDo</Phase>
      <Name>TestName D</Name>
    </Activity>
    <Timebox>00:44:00</Timebox>
  </DecideWhatToDo>
  <CloseTheRetrospective>
    <Activity>
      <ID>5</ID>
      <Phase>CloseTheRetrospective</Phase>
      <Name>TestName C</Name>
    </Activity>
    <Timebox>00:55:00</Timebox>
  </CloseTheRetrospective>
  <VisualStyles>
    <StyleCollection>
      <VisualStyle>LightOrange</VisualStyle>
      <VisualStyle>LightBlue</VisualStyle>
      <VisualStyle>DarkBlue</VisualStyle>
      <VisualStyle>DarkOrange</VisualStyle>
      <VisualStyle>Blue</VisualStyle>
    </StyleCollection>
  </VisualStyles>
</RetroPlanState>";

			return new Tuple<RetroPlanState, string>(state, expected);
		}
	}
}
