﻿using System;
using System.Collections.Generic;
using Retrotimer.Data.State;
using Retrotimer.Model;
using Retrotimer.ViewModel;

namespace Data
{
    public static class TestData
    {
	    public static readonly Activity FirstSetTheStageTestData =
		    new Activity(14, RetroPhase.SetTheStage, "Name first set stage");

	    public static readonly Activity FirstGatherDataTestData = 
		    new Activity(1, RetroPhase.GatherData, "Name gather data");

	    public static readonly Activity FirstGenerateInsightsTestData = 
		    new Activity(2, RetroPhase.GenerateInsights, "Name generate insights");

	    public static readonly Activity FirstDecideWhatToDoTestData = 
		    new Activity(3, RetroPhase.DecideWhatToDo, "Name decide what do to");

	    public static readonly Activity FirstCloseTheRetrospectiveTestData = 
		    new Activity(4, RetroPhase.CloseTheRetrospective, "Name close the retrospective");

	    public static readonly Activity SecondSetTheStageTestData = 
		    new Activity(140, RetroPhase.SetTheStage, "Name set stage");

	    public static readonly Activity SecondGatherDataTestData = 
		    new Activity(10, RetroPhase.GatherData, "Name gather data");

	    public static readonly Activity SecondGenerateInsightsTestData = 
		    new Activity(20, RetroPhase.GenerateInsights, "Name generate insights");

	    public static readonly Activity SecondDecideWhatToDoTestData = 
		    new Activity(30, RetroPhase.DecideWhatToDo, "Name decide what do to");

	    public static readonly Activity SecondCloseTheRetrospectiveTestData = 
		    new Activity(40, RetroPhase.CloseTheRetrospective, "Name close the retrospective");

	    public static readonly ActivityWithTimebox FirstSetTheStageActivityWithTimebox = 
		    new ActivityWithTimebox(FirstSetTheStageTestData, TimeSpan.FromMinutes(5));

	    public static readonly ActivityWithTimebox FirstGatherDataActivityWithTimebox = 
		    new ActivityWithTimebox(FirstGatherDataTestData, TimeSpan.FromMinutes(20));

	    public static readonly ActivityWithTimebox FirstGenerateInsightsActivityWithTimebox = 
		    new ActivityWithTimebox(FirstGenerateInsightsTestData, TimeSpan.FromMinutes(40));

	    public static readonly ActivityWithTimebox FirstDecideWhatToDoActivityWithTimebox = 
		    new ActivityWithTimebox(FirstDecideWhatToDoTestData, TimeSpan.FromMinutes(15));

	    public static readonly ActivityWithTimebox FirstCloseTheRetrospectiveActivityWithTimebox = 
		    new ActivityWithTimebox(FirstCloseTheRetrospectiveTestData, TimeSpan.FromMinutes(5));

	    public static readonly ActivityWithTimebox SecondSetTheStageActivityWithTimebox = 
		    new ActivityWithTimebox(SecondSetTheStageTestData, TimeSpan.FromMinutes(55));

	    public static readonly ActivityWithTimebox SecondGatherDataActivityWithTimebox = 
		    new ActivityWithTimebox(SecondGatherDataTestData, TimeSpan.FromMinutes(202));

	    public static readonly ActivityWithTimebox SecondGenerateInsightsActivityWithTimebox = 
		    new ActivityWithTimebox(SecondGenerateInsightsTestData, TimeSpan.FromMinutes(405));

	    public static readonly ActivityWithTimebox SecondDecideWhatToDoActivityWithTimebox = 
		    new ActivityWithTimebox(SecondDecideWhatToDoTestData, TimeSpan.FromMinutes(152));

	    public static readonly ActivityWithTimebox SecondCloseTheRetrospectiveActivityWithTimebox = 
		    new ActivityWithTimebox(SecondCloseTheRetrospectiveTestData, TimeSpan.FromMinutes(555));
	    
	    public static List<Activity> CreateTestActivities()
	    {
		    return new List<Activity>
		    {
			    FirstSetTheStageTestData,
			    FirstGatherDataTestData,
			    FirstGenerateInsightsTestData,
			    FirstDecideWhatToDoTestData,
			    FirstCloseTheRetrospectiveTestData,
			    SecondSetTheStageTestData,
			    SecondGatherDataTestData,
			    SecondGenerateInsightsTestData,
			    SecondDecideWhatToDoTestData,
			    SecondCloseTheRetrospectiveTestData
		    };
	    }

	    public static List<ActivityWithTimebox> CreateTestActivitiesWithTimebox()
	    {
		    return new List<ActivityWithTimebox>
		    {
			    FirstSetTheStageActivityWithTimebox,
			    FirstGatherDataActivityWithTimebox,
			    FirstGenerateInsightsActivityWithTimebox,
			    FirstDecideWhatToDoActivityWithTimebox,
			    FirstCloseTheRetrospectiveActivityWithTimebox,
			    SecondSetTheStageActivityWithTimebox,
			    SecondGatherDataActivityWithTimebox,
			    SecondGenerateInsightsActivityWithTimebox,
			    SecondDecideWhatToDoActivityWithTimebox,
			    SecondCloseTheRetrospectiveActivityWithTimebox
		    };
	    }

	    public static RetroPlanState CreateTestRetroPlanState()
	    {
		    return new RetroPlanState
		    {
			    SetTheStage = new ActivityWithTimeboxState(FirstSetTheStageActivityWithTimebox),
			    GatherData = new ActivityWithTimeboxState(FirstGatherDataActivityWithTimebox),
			    GenerateInsights = new ActivityWithTimeboxState(FirstGenerateInsightsActivityWithTimebox),
			    DecideWhatToDo = new ActivityWithTimeboxState(FirstDecideWhatToDoActivityWithTimebox),
			    CloseTheRetrospective = new ActivityWithTimeboxState(FirstCloseTheRetrospectiveActivityWithTimebox),
			    VisualStyles = new VisualStyleCollectionState(new List<VisualStyle>
			    {
				    VisualStyle.Blue,
				    VisualStyle.DarkOrange,
				    VisualStyle.DarkBlue,
				    VisualStyle.LightOrange,
				    VisualStyle.LightBlue
			    })
		    };
	    }
    }
}
