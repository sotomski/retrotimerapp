﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Retrotimer.Data;
using Retrotimer.Model;
using Retrotimer.Services;

namespace Data
{
    [Ignore("Deprecation of the XML format in the app")]
	[TestFixture]
    public class XmlActivityReaderTest
    {
        Mock<IStorage> storageMock;
        Mock<IEmbeddedResources> embeddedResMock;
        XmlActivityReader sut;

        [SetUp]
        public void SetUp()
        {
            embeddedResMock = new Mock<IEmbeddedResources>();
            embeddedResMock.Setup(e => e.OpenStreamToResource(It.IsAny<string>(), default(CancellationToken))).ReturnsAsync(new System.IO.MemoryStream());

            storageMock = new Mock<IStorage>();
            storageMock.Setup(s => s.EmbeddedResources).Returns(embeddedResMock.Object);

            sut = new XmlActivityReader(string.Empty, storageMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            sut = null;
            storageMock = null;
            embeddedResMock = null;
        }

        void ConfigureMocksWithXml(string xml)
        {
            var xmlStream = new System.IO.MemoryStream(Encoding.UTF8.GetBytes(xml));
            embeddedResMock.Setup(e => e.OpenStreamToResource(It.IsAny<string>(), default(CancellationToken))).ReturnsAsync(xmlStream);
        }

        class ActivityXmlBuilder
        {
            public string Id = "0";
            public string Phase = "SetTheStage";
            public string Name = "ESVP";

            private string GetActivityTemplate()
            {
                return @"<?xml version=""1.0"" encoding=""utf-8"" standalone=""yes""?>
                        <activities xmlns=""http://tempuri.org/Activities.xsd"">
                            <activity activityId=""{0}"">
                                <phase>{1}</phase>
                                <name>{2}</name>
                            </activity>
                        </activities>";
            }

            public string GetConfiguredActivityXml()
            {
                return string.Format(GetActivityTemplate(), Id, Phase, Name);
            }
        }
        
        [Test]
        public void PathToXml_Should_ExposePathToXml_When_PassedToCtor()
        {
            // Arrange
            const string testPath = "Test path to Xml";
            var sut = new XmlActivityReader(testPath, null);

            // Act
            var result = sut.XmlResourceId;

            // Assert
            Assert.That(result, Is.EqualTo(testPath));
        }

        [Test]
        public void ReadAllActivitiesAsync_Should_ThrowInvalidOperationException_When_StorageIsNull()
        {
            // Arrange
            var sut = new XmlActivityReader(string.Empty, null);

            // Act
            AsyncTestDelegate test = async () => await sut.ReadAllActivitiesAsync();

            // Assert
            Assert.ThrowsAsync<InvalidOperationException>(test);
        }

        [Test]
		public void ReadAllActivitiesAsync_Should_ThrowInvalidOperationException_When_OpenStreamToResource()
        {
            // Arrange
            storageMock.Setup(s => s.EmbeddedResources.OpenStreamToResource(It.IsAny<string>(), default(CancellationToken)))
                .Throws<Exception>()
                .Verifiable();
            var sut = new XmlActivityReader(string.Empty, storageMock.Object);

            // Act
            AsyncTestDelegate test = async () => await sut.ReadAllActivitiesAsync();

			// Assert
			//Assert.That(test, Throws.InvalidOperationException);
			Assert.ThrowsAsync<InvalidOperationException>(test);
            storageMock.Verify();
        }

        [Test]
        public async Task ReadAllActivitiesAsync_Should_ReturnEmptyList_When_XmlContainesNoActivitiesNode()
        {
            // Arrange
            const string xml = @"<?xml version=""1.0"" encoding=""utf-8"" standalone=""yes""?><nothing />";
            ConfigureMocksWithXml(xml);

            // Act
            IList<Activity> result = await sut.ReadAllActivitiesAsync();

            // Assert
            Assert.That(result, Is.Empty);
        }

        [Test]
        public async Task ReadAllActivitiesAsync_Should_ReturnEmptyList_When_XmlContainsEmptyActivityNode()
        {
            // Arrange
            const string xml = @"<?xml version=""1.0"" encoding=""utf-8"" standalone=""yes""?><activities />";
            ConfigureMocksWithXml(xml);

            // Act
            IList<Activity> result = await sut.ReadAllActivitiesAsync();

            // Assert
            Assert.That(result, Is.Empty);
        }

        [Test]
        [TestCase("0")]
        [TestCase("4")]
        [TestCase("87")]
        [TestCase("999")]
        public async Task ReadAllActivitiesAsync_Should_ReturnActivitiesWithIds_When_XmlContainsIdNodes(string id)
        {
            // Arrange
            var builder = new ActivityXmlBuilder()
            { 
                Id = id 
            };

            ConfigureMocksWithXml(builder.GetConfiguredActivityXml());

            // Act
            IList<Activity> result = await sut.ReadAllActivitiesAsync();

            // Assert
            Assert.That(result[0].ID, Is.EqualTo(int.Parse(id)));
        }

        [Test]
        [TestCase("-1")]
        [TestCase("blablabla")]
        [TestCase("99d9")]
        [TestCase("1.5")]
        public async Task ReadAllActivitiesAsync_Should_IgnoreActivitiesWithIncorrectIds(string id)
        {
            // Arrange
            var builder = new ActivityXmlBuilder()
            { 
                Id = id 
            };
            ConfigureMocksWithXml(builder.GetConfiguredActivityXml());

            // Act
            IList<Activity> result = await sut.ReadAllActivitiesAsync();

            // Assert
            Assert.That(result, Is.Empty);
        }

        [Test]
        [TestCase(RetroPhase.SetTheStage)]
        [TestCase(RetroPhase.GenerateInsights)]
        [TestCase(RetroPhase.GatherData)]
        [TestCase(RetroPhase.DecideWhatToDo)]
        [TestCase(RetroPhase.CloseTheRetrospective)]
        public async Task ReadAllActivitiesAsync_Should_ReturnActivitiesWithPhase_When_XmlContainesPhaseNodes(RetroPhase phase)
        {
            // Arrange
            var builder = new ActivityXmlBuilder()
            {
                Phase = phase.ToString()
            };
            ConfigureMocksWithXml(builder.GetConfiguredActivityXml());

            // Act
            var result = await sut.ReadAllActivitiesAsync();

            // Assert
            Assert.That(result[0].Phase, Is.EqualTo(phase));
        }

        [Test]
        public async Task ReadAllActivitiesAsync_Should_ReturnActivitiesWithName_When_XmlContainesNameNodes()
        {
            // Arrange
            const string testName = "Test name that I want to see returned";
            var builder = new ActivityXmlBuilder()
            {
                Name = testName
            };
            ConfigureMocksWithXml(builder.GetConfiguredActivityXml());

            // Act
            var result = await sut.ReadAllActivitiesAsync();

            // Assert
            Assert.That(result[0].Name, Is.EqualTo(testName));
        }

        [Test]
        public async Task ReadAllActivitiesAsync_Should_IgnoreActivityNodes_When_NameSubnodeIsEmpty()
        {
            // Arrange
            var builder = new ActivityXmlBuilder()
            {
                Phase = string.Empty
            };
            ConfigureMocksWithXml(builder.GetConfiguredActivityXml());

            // Act
            var result = await sut.ReadAllActivitiesAsync();

            // Assert
            Assert.That(result, Is.Empty);
        }

        [Test]
		[Ignore("Can't figure out the correct path to test data.")]
        public async Task ReadAllActivitiesAsync_Should_ReadAllEmbeddedActivities()
        {
            // Arrange
            var fs = new FileStream(@"TestData/Activities.xml", FileMode.Open);

            embeddedResMock.Reset();
            embeddedResMock.Setup(e => e.OpenStreamToResource(It.IsAny<string>(), default(CancellationToken)))
                .ReturnsAsync(fs)
                .Verifiable();

            // Act
            var result = await sut.ReadAllActivitiesAsync();

            // Assert
            embeddedResMock.Verify();
            Assert.That(result.Count(), Is.GreaterThan(0));
        }
    }
}
