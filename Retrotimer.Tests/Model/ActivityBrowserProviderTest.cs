﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Retrotimer.Data;
using Retrotimer.Model;

namespace Model
{
    [Ignore("Preparation for class deletion")]
	[TestFixture]
	public class ActivityBrowserProviderTest
	{
		static readonly Activity FirstSetTheStageTestData = new Activity(14, RetroPhase.SetTheStage, "Name set stage");
		static readonly Activity FirstGatherDataTestData = new Activity(1, RetroPhase.GatherData, "Name gather data");
		static readonly Activity FirstGenerateInsightsTestData = new Activity(2, RetroPhase.GenerateInsights, "Name generate insights");
		static readonly Activity FirstDecideWhatToDoTestData = new Activity(3, RetroPhase.DecideWhatToDo, "Name decide what do to");
		static readonly Activity FirstCloseTheRetrospectiveTestData = new Activity(4, RetroPhase.CloseTheRetrospective, "Name close the retrospective");
		static readonly Activity SecondSetTheStageTestData = new Activity(140, RetroPhase.SetTheStage, "Name set stage");
		static readonly Activity SecondGatherDataTestData = new Activity(10, RetroPhase.GatherData, "Name gather data");
		static readonly Activity SecondGenerateInsightsTestData = new Activity(20, RetroPhase.GenerateInsights, "Name generate insights");
		static readonly Activity SecondDecideWhatToDoTestData = new Activity(30, RetroPhase.DecideWhatToDo, "Name decide what do to");
		static readonly Activity SecondCloseTheRetrospectiveTestData = new Activity(40, RetroPhase.CloseTheRetrospective, "Name close the retrospective");

		static List<Activity> CreateTestData()
		{
			return new List<Activity>
			{
				FirstSetTheStageTestData,
				FirstGatherDataTestData,
				FirstGenerateInsightsTestData,
				FirstDecideWhatToDoTestData,
				FirstCloseTheRetrospectiveTestData,
				SecondSetTheStageTestData,
				SecondGatherDataTestData,
				SecondGenerateInsightsTestData,
				SecondDecideWhatToDoTestData,
				SecondCloseTheRetrospectiveTestData
			};
		}

		Mock<IActivityReader> readerMock;
		ActivityBrowserProvider sut;

		[SetUp]
		public void Setup()
		{
			readerMock = new Mock<IActivityReader>();
			sut = new ActivityBrowserProvider(readerMock.Object);
		}

		[TearDown]
		public void Teardown()
		{
			sut = null;
			readerMock = null;
		}

		[Test]
		public void Ctor_Should_ThrowArgumentNullException_When_ReaderIsNull()
		{
			// Act
			TestDelegate test = () => new ActivityBrowserProvider(null);

			// Assert
			Assert.That(test, Throws.TypeOf<ArgumentNullException>()
						.With.Property(nameof(ArgumentNullException.ParamName)).EqualTo("reader"));
		}

		[Test]
		public void Ctor_Should_CreateInstance_When_ReaderProvided()
		{
			// Arrange
			var reader = Mock.Of<IActivityReader>();

			// Act
			var lsut = new ActivityBrowserProvider(reader);

			// Assert
			Assert.That(lsut, Is.Not.Null);
		}

		[Test]
		public async Task CreateActivityBrowserAsync_Should_ReturnNull_When_ActivityProviderReturnsNoActivities()
		{
			// Arrange
			readerMock.Setup(m => m.ReadAllActivitiesAsync()).ReturnsAsync(new List<Activity>());

			// Act
			var result = await sut.CreateActivityBrowserAsync();

			// Assert
			Assert.That(result, Is.Null);
		}

		[Test]
		public async Task CreateActivityBrowserAsync_Should_CreateBrowserWithActivitiesFromActivityReader()
		{
			// Arrange
			var inputList = CreateTestData();
			readerMock.Setup(m => m.ReadAllActivitiesAsync()).ReturnsAsync(inputList);

			// Act
			var result = await sut.CreateActivityBrowserAsync();

			// Assert			
			Assert.That(result.GetAllActivities(), Is.EquivalentTo(inputList));
		}
	}
}

