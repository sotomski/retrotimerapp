﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Retrotimer.Model;

namespace Model
{
    [Ignore("Preparation for deletion of the class")]
	[TestFixture]
	public class ActivityBrowserTest
	{
		static readonly Activity FirstSetTheStageTestData = new Activity(14, RetroPhase.SetTheStage, "Name set stage");
		static readonly Activity FirstGatherDataTestData = new Activity(1, RetroPhase.GatherData, "Name gather data");
		static readonly Activity FirstGenerateInsightsTestData = new Activity(2, RetroPhase.GenerateInsights, "Name generate insights");
		static readonly Activity FirstDecideWhatToDoTestData = new Activity(3, RetroPhase.DecideWhatToDo, "Name decide what do to");
		static readonly Activity FirstCloseTheRetrospectiveTestData = new Activity(4, RetroPhase.CloseTheRetrospective, "Name close the retrospective");
		static readonly Activity SecondSetTheStageTestData = new Activity(140, RetroPhase.SetTheStage, "Name set stage");
		static readonly Activity SecondGatherDataTestData = new Activity(10, RetroPhase.GatherData, "Name gather data");
		static readonly Activity SecondGenerateInsightsTestData = new Activity(20, RetroPhase.GenerateInsights, "Name generate insights");
		static readonly Activity SecondDecideWhatToDoTestData = new Activity(30, RetroPhase.DecideWhatToDo, "Name decide what do to");
		static readonly Activity SecondCloseTheRetrospectiveTestData = new Activity(40, RetroPhase.CloseTheRetrospective, "Name close the retrospective");

		static List<Activity> CreateTestData()
		{
			return new List<Activity>
			{
				FirstSetTheStageTestData,
				FirstGatherDataTestData,
				FirstGenerateInsightsTestData,
				FirstDecideWhatToDoTestData,
				FirstCloseTheRetrospectiveTestData,
				SecondSetTheStageTestData,
				SecondGatherDataTestData,
				SecondGenerateInsightsTestData,
				SecondDecideWhatToDoTestData,
				SecondCloseTheRetrospectiveTestData
			};
		}
		
		[Test]
		public void Ctor_Should_ThrowArgumentNullException_When_ActivitiesIsNull()
		{
			// Act
			TestDelegate test = () => new ActivityBrowser(null);

			// Assert
			Assert.That(test, Throws.ArgumentNullException
						.With.Property(nameof(ArgumentNullException.ParamName))
						.EqualTo("activities"));
		}

		[Test]
		public void Ctor_Should_CreateInstance_When_ActivitiesIsNotNull()
		{
			// Arrange
			var input = new List<Activity>();

			// Act
			var sut = new ActivityBrowser(input);

			// Assert
			Assert.That(sut, Is.Not.Null);
		}

		[Test]
		public void GetAllActivities_Should_ReturnEmptyList_When_InputActivitiesListWasEmpty()
		{
			// Arrange
			var emptyInput = new List<Activity>();
			var sut = new ActivityBrowser(emptyInput);

			// Act
			var actual = sut.GetAllActivities();

			// Assert
			Assert.That(actual, Is.Empty);
		}

		[Test]
		public void GetAllActivities_Should_ReturnActivitiesPassedViaConstructor()
		{
			// Arrange
			var input = CreateTestData();
			var sut = new ActivityBrowser(input);

			// Act
			var actual = sut.GetAllActivities();

			// Assert
			Assert.That(actual, Is.EquivalentTo(input));
		}

		[Test]
		[TestCase(RetroPhase.SetTheStage)]
		[TestCase(RetroPhase.GatherData)]
		[TestCase(RetroPhase.GenerateInsights)]
		[TestCase(RetroPhase.DecideWhatToDo)]
		[TestCase(RetroPhase.CloseTheRetrospective)]
		public void GetActivitiesForPhase_Should_ReturnEmptyList_When_InputActivitiesListWasEmpty(RetroPhase phase)
		{
			// Arrange
			var emptyInput = new List<Activity>();
			var sut = new ActivityBrowser(emptyInput);

			// Act
			var actual = sut.GetActivitiesForPhase(phase);

			// Assert
			Assert.That(actual, Is.Empty);
		}

		[Test]
		[TestCase(RetroPhase.SetTheStage)]
		[TestCase(RetroPhase.GatherData)]
		[TestCase(RetroPhase.GenerateInsights)]
		[TestCase(RetroPhase.DecideWhatToDo)]
		[TestCase(RetroPhase.CloseTheRetrospective)]
		public void GetActivitiesForPhase_Should_ReturnPhaseActivitiesPassedViaConstructor(RetroPhase phase)
		{
			// Assert
			var input = CreateTestData();
			var inputForPhase = input.Where(a => a.Phase == phase);
			var sut = new ActivityBrowser(input);

			// Act
			var actual = sut.GetActivitiesForPhase(phase);

			// Assert
			Assert.That(actual, Is.EquivalentTo(inputForPhase));
		}
	}
}

