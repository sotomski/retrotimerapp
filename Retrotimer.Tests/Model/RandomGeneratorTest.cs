﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Retrotimer.Services;

namespace Model
{
	[TestFixture]
    public class RandomGeneratorTest
    {
        [Test]
        public void GetNext_Should_CreateAtLeastTwoDifferentNumbersFromRange_When_Run10Times()
        {
            /// It is a very lousy test, but it's the best I could come up with to test
            /// the randomness of the generator

            // Arrange
            var repeats = 10;
            const int min = 0;
            const int max = 14;
            var sut = new RandomGenerator();
            var results = new List<int>();

            // Act
            while(repeats > 0)
            {
                results.Add(sut.GetInt(min, max));
                --repeats;
            }

            // Assert
            Assert.That(results.Distinct().Count(), Is.GreaterThan(1));
            Assert.That(results.TrueForAll(i => min <= i && i < max), Is.True);
        }
    }
}

