﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Moq;
using NUnit.Framework;
using Retrotimer;
using Retrotimer.Data.State;
using Retrotimer.Model;
using Retrotimer.Services;
using Retrotimer.Services.Data;

namespace Model
{
	[TestFixture]
	public class RetrospectiveInteractorTest
	{
		List<Activity> testData;
        Mock<IDataService> dataSvcMock;
		Mock<IRandomGenerator> randomizerMock;
		RetrospectiveInteractor sut;
		private Mock<IAppStateManager> appStateManager;

		[SetUp]
		public void SetUp()
		{
			testData = TestData.CreateTestActivities();

            dataSvcMock = new Mock<IDataService>();
            dataSvcMock.Setup(m => m.ReadActivitiesAsync()).ReturnsAsync(testData);
			appStateManager = new Mock<IAppStateManager>();
			randomizerMock = new Mock<IRandomGenerator>();

            sut = new RetrospectiveInteractor(dataSvcMock.Object, appStateManager.Object, randomizerMock.Object);
		}

		[TearDown]
		public void TearDown()
		{
			sut = null;
			randomizerMock = null;
		}

		[Test]
		public void Ctor_Should_Throw_ArgumentNullException_When_DataServiceIsNull()
		{
			// Act
			TestDelegate test = () => new RetrospectiveInteractor(null, appStateManager.Object, randomizerMock.Object);

			// Assert
			Assert.That(test, Throws.ArgumentNullException
						.With.Property(nameof(ArgumentNullException.ParamName)).EqualTo("dataService"));
		}
		
		[Test]
		public void Ctor_Should_Throw_ArgumentNullException_When_RandomizerIsNull()
		{
			// Act
            TestDelegate test = () => new RetrospectiveInteractor(dataSvcMock.Object, appStateManager.Object, null);

			// Assert
			Assert.That(test, Throws.ArgumentNullException
			            .With.Property(nameof(ArgumentNullException.ParamName)).EqualTo("randomizer"));
		}

		[Test]
		public void Ctor_Should_ThrowArgumentNullException_When_AppStateManagerIsNull()
		{
			// Act
			TestDelegate test = () => new RetrospectiveInteractor(dataSvcMock.Object, null, randomizerMock.Object);

			// Assert
			Assert.That(
				test, 
				Throws
					.ArgumentNullException
					.With
					.Property(nameof(ArgumentNullException.ParamName))
					.EqualTo("appStateManager"));
		}

		[Test]
		public void GenerateRetrospectivePlan_Should_ThrowInvalidOperationException_When_NoSetTheStageActivityPresent()
		{
            // Arrange
            testData.RemoveAll(a => a.Phase == RetroPhase.SetTheStage);

			var expectedMsg = string.Format("Required activity for {0} phase not found.", RetroPhase.SetTheStage);

			// Act
			AsyncTestDelegate test = async () => await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(test, Throws.InvalidOperationException.With.Message.EqualTo(expectedMsg));
		}

		[Test]
		public void GenerateRetrospectivePlan_Should_ThrowInvalidOperationException_When_NoGatherDataActivityPresent()
		{
            // Arrange
            testData.RemoveAll(a => a.Phase == RetroPhase.GatherData);
			
			var expectedMsg = string.Format("Required activity for {0} phase not found.", RetroPhase.GatherData);

			// Act
			AsyncTestDelegate test = async () => await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(test, Throws.InvalidOperationException.With.Message.EqualTo(expectedMsg));
		}

		[Test]
		public void GenerateRetrospectivePlan_Should_ThrowInvalidOperationException_When_NoGenerateInsightsActivityPresent()
		{
            // Arrange
            testData.RemoveAll(a => a.Phase == RetroPhase.GenerateInsights);
			
			var expectedMsg = string.Format("Required activity for {0} phase not found.", RetroPhase.GenerateInsights);

			// Act
			AsyncTestDelegate test = async () => await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(test, Throws.InvalidOperationException.With.Message.EqualTo(expectedMsg));
		}

		[Test]
		public void GenerateRetrospectivePlan_Should_ThrowInvalidOperationException_When_NoDecideWhatToDoActivityPresent()
		{
            // Arrange
            testData.RemoveAll(a => a.Phase == RetroPhase.DecideWhatToDo);
			
			var expectedMsg = string.Format("Required activity for {0} phase not found.", RetroPhase.DecideWhatToDo);

			// Act
			AsyncTestDelegate test = async () => await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(test, Throws.InvalidOperationException.With.Message.EqualTo(expectedMsg));
		}

		[Test]
		public void GenerateRetrospectivePlan_Should_ThrowInvalidOperationException_When_NoCloseTheRetroActivityPresent()
		{
			// Arrange
            testData.RemoveAll(a => a.Phase == RetroPhase.CloseTheRetrospective);

			var expectedMsg = string.Format("Required activity for {0} phase not found.", RetroPhase.CloseTheRetrospective);

			// Act
			AsyncTestDelegate test = async () => await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(test, Throws.InvalidOperationException.With.Message.EqualTo(expectedMsg));
		}

		[Test]
        public async Task GenerateRetrospectivePlan_Should_ReturnFirstActivityForEachPhase_When_RandomizerReturnsAlwaysZeroes()
		{
			// Assert
			randomizerMock.Setup(r => r.GetInt(It.IsAny<int>(), It.IsAny<int>())).Returns(0);
			
			// Act
			var retroPlan = await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(retroPlan.SetTheStage.Activity, Is.EqualTo(TestData.FirstSetTheStageTestData));
			Assert.That(retroPlan.GatherData.Activity, Is.EqualTo(TestData.FirstGatherDataTestData));
			Assert.That(retroPlan.GenerateInsights.Activity, Is.EqualTo(TestData.FirstGenerateInsightsTestData));
			Assert.That(retroPlan.DecideWhatToDo.Activity, Is.EqualTo(TestData.FirstDecideWhatToDoTestData));
			Assert.That(retroPlan.CloseTheRetrospective.Activity, Is.EqualTo(TestData.FirstCloseTheRetrospectiveTestData));
		}

		[Test]
		public async Task GenerateRetrospectivePlan_Should_ReturnSecondActivityForEachPhase_When_RandomizerReturnsAlwaysOnes()
		{
			// Assert
			randomizerMock.Setup(r => r.GetInt(It.IsAny<int>(), It.IsAny<int>())).Returns(1);
			

			// Act
			var retroPlan = await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(retroPlan.SetTheStage.Activity, Is.EqualTo(TestData.SecondSetTheStageTestData));
			Assert.That(retroPlan.GatherData.Activity, Is.EqualTo(TestData.SecondGatherDataTestData));
			Assert.That(retroPlan.GenerateInsights.Activity, Is.EqualTo(TestData.SecondGenerateInsightsTestData));
			Assert.That(retroPlan.DecideWhatToDo.Activity, Is.EqualTo(TestData.SecondDecideWhatToDoTestData));
			Assert.That(retroPlan.CloseTheRetrospective.Activity, Is.EqualTo(TestData.SecondCloseTheRetrospectiveTestData));
		}

		[Test]
		public async Task GenerateRetrospectivePlan_Should_AssignFiveMinuteTimeboxeToSetTheStageActivity_When_CreatingNewRetroPlan()
		{
			// Arrange
			randomizerMock.Setup(r => r.GetInt(It.IsAny<int>(), It.IsAny<int>())).Returns(1);
			

			// Act
			var retroPlan = await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(retroPlan.SetTheStage.Timebox, Is.EqualTo(TimeSpan.FromMinutes(5)));
		}

		[Test]
		public async Task GenerateRetrospectivePlan_Should_AssignTwentyMinuteTimeboxeToGatherDataActivity_When_CreatingNewRetroPlan()
		{
			// Arrange
			randomizerMock.Setup(r => r.GetInt(It.IsAny<int>(), It.IsAny<int>())).Returns(1);
			

			// Act
			var retroPlan = await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(retroPlan.GatherData.Timebox, Is.EqualTo(TimeSpan.FromMinutes(20)));
		}

		[Test]
		public async Task GenerateRetrospectivePlan_Should_AssignThirtyMinuteTimeboxeToGenerateInsightsActivity_When_CreatingNewRetroPlan()
		{
			// Arrange
			randomizerMock.Setup(r => r.GetInt(It.IsAny<int>(), It.IsAny<int>())).Returns(1);
			

			// Act
			var retroPlan = await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(retroPlan.GenerateInsights.Timebox, Is.EqualTo(TimeSpan.FromMinutes(30)));
		}

		[Test]
		public async Task GenerateRetrospectivePlan_Should_AssignThirtyMinuteTimeboxeToDecideWhatToDoActivity_When_CreatingNewRetroPlan()
		{
			// Arrange
			randomizerMock.Setup(r => r.GetInt(It.IsAny<int>(), It.IsAny<int>())).Returns(1);
			

			// Act
			var retroPlan = await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(retroPlan.DecideWhatToDo.Timebox, Is.EqualTo(TimeSpan.FromMinutes(30)));
		}

		[Test]
		public async Task GenerateRetrospectivePlan_Should_AssignFiveMinuteTimeboxeToCloseTheRetrospectiveActivity_When_CreatingNewRetroPlan()
		{
			// Arrange
			randomizerMock.Setup(r => r.GetInt(It.IsAny<int>(), It.IsAny<int>())).Returns(1);
			

			// Act
			var retroPlan = await sut.GenerateRetrospectivePlan();

			// Assert
			Assert.That(retroPlan.CloseTheRetrospective.Timebox, Is.EqualTo(TimeSpan.FromMinutes(5)));
		}

		[Test]
		public async Task SaveAsync_Should_PersistTheRetro()
		{
			// Arrange
			var testPlan = TestData.CreateTestRetroPlanState();

			// Act
			await sut.SaveAsync(testPlan);

			// Assert
			appStateManager.Verify(asm => asm.SaveAsync(testPlan));
		}

		[Test]
		public async Task ReadAsync_Should_LoadRetroFromPersistentStorage()
		{
			// Arrange
			var testPlan = TestData.CreateTestRetroPlanState();
			appStateManager.Setup(asm => asm.ReadAsync()).ReturnsAsync(testPlan);

			// Act
			var result = await sut.ReadAsync();

			// Assert
			AssertAreRetroStatesEqual(result, testPlan);
		}

		[Test]
		public async Task ReadAsync_Should_EnsureThatLoadedStateExistsInCurrentDataset()
		{
			// Arrange
			var expectedPlan = TestData.CreateTestRetroPlanState(); // TestData guarantees data integrity.
			var outdatedPlan = TestData.CreateTestRetroPlanState();
			outdatedPlan.SetTheStage.Activity.ID = int.MaxValue;
			outdatedPlan.GatherData.Activity.Name = "Test name that should be overriden";
			outdatedPlan.GenerateInsights.Activity.ID = int.MinValue;
			outdatedPlan.DecideWhatToDo.Activity.Name = "Test name that should be overriden";
			outdatedPlan.CloseTheRetrospective.Activity.ID = int.MaxValue;
			
			appStateManager.Setup(asm => asm.ReadAsync()).ReturnsAsync(outdatedPlan);
			
			// Act
			var result = await sut.ReadAsync();
			
			// Assert
			AssertAreRetroStatesEqual(result, expectedPlan);
		}

		[Test]
		public async Task RetrospectivePlanOutdated_Should_PublishCurrentMerger()
		{
			// Arrange
			var newDataset = TestData.CreateTestActivities();
			var testPlanState = TestData.CreateTestRetroPlanState();
			var expectedPlan = testPlanState.ToRetrospectivePlan();
			var oldPlan = PrepareOldRetrospectivePlan(newDataset, expectedPlan);
			var oldDataset = PrepareOldDataSet(oldPlan);
			var oldPlanState = new RetroPlanState(oldPlan, testPlanState.ToVisualStyles());
			appStateManager.Setup(asm => asm.ReadAsync()).ReturnsAsync(oldPlanState);
			dataSvcMock.SetupSequence(ds => ds.ReadActivitiesAsync())
				.ReturnsAsync(oldDataset)
				.ReturnsAsync(newDataset);
			
			var eventArgs = new EventArgs<IEnumerable<Activity>>(newDataset);
			var actual = default(RetrospectivePlan);

			// Act
			var actualOldPlanState = await sut.ReadAsync();
			sut.RetrospectivePlanOutdated += (sender, args) =>
			{
				actual = args.Merger(oldPlan);
			};
			dataSvcMock.Raise(ds => ds.NewActivitiesAvailable += null, eventArgs);
			
			// Assert
			AssertAreRetroStatesEqual(actualOldPlanState, oldPlanState);
			Assert.That(actual, Is.EqualTo(expectedPlan));
		}

		private static void AssertAreRetroStatesEqual(RetroPlanState actual, RetroPlanState expectedPlan)
		{
			Assert.That(actual.ToRetrospectivePlan(), Is.EqualTo(expectedPlan.ToRetrospectivePlan()));
			Assert.That(actual.ToVisualStyles(), Is.EqualTo(expectedPlan.ToVisualStyles()));
		}
		
		private static List<Activity> PrepareOldDataSet(RetrospectivePlan outdatedPlan)
		{
			var oldDataset = new List<Activity>
			{
				outdatedPlan.SetTheStage.Activity,
				outdatedPlan.GatherData.Activity,
				outdatedPlan.GenerateInsights.Activity,
				outdatedPlan.DecideWhatToDo.Activity,
				outdatedPlan.CloseTheRetrospective.Activity,
			};
			return oldDataset;
		}

		private static RetrospectivePlan PrepareOldRetrospectivePlan(List<Activity> newDataset, RetrospectivePlan expectedPlan)
		{
			var oldSetTheStage = newDataset.First(a => a.Phase == RetroPhase.SetTheStage);
			oldSetTheStage.ID = int.MaxValue;
			var oldGatherData = newDataset.First(a => a.Phase == RetroPhase.GatherData);
			oldGatherData.Name = "Test name that should be overriden";
			var oldGenerateInsights = newDataset.First(a => a.Phase == RetroPhase.GenerateInsights);
			oldGenerateInsights.ID = int.MinValue;
			var oldDecideWhatToDo = newDataset.First(a => a.Phase == RetroPhase.DecideWhatToDo);
			oldDecideWhatToDo.Name = "Test name that should be overriden";
			var oldCloseTheRetrospective = newDataset.First(a => a.Phase == RetroPhase.CloseTheRetrospective);
			oldCloseTheRetrospective.ID = int.MaxValue - 1;

			var outdatedPlan = new RetrospectivePlan
			{
				SetTheStage = new ActivityWithTimebox(oldSetTheStage, expectedPlan.SetTheStage.Timebox),
				GatherData = new ActivityWithTimebox(oldGatherData, expectedPlan.GatherData.Timebox),
				GenerateInsights = new ActivityWithTimebox(oldGenerateInsights, expectedPlan.GenerateInsights.Timebox),
				DecideWhatToDo = new ActivityWithTimebox(oldDecideWhatToDo, expectedPlan.DecideWhatToDo.Timebox),
				CloseTheRetrospective =
					new ActivityWithTimebox(oldCloseTheRetrospective, expectedPlan.CloseTheRetrospective.Timebox)
			};
			return outdatedPlan;
		}

	}
}
