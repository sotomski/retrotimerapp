﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Retrotimer.Model;

namespace Model
{
	[TestFixture]
	public class RetrospectivePlanTest
	{
		[Test]
		public void ChangePlanWithActivity_Should_ReturnPlanWithNewSetTheStage_When_SetTheStageActivityPassed()
		{
			// Arrange
			var activityToInsert = CreateTestActivityWithPhase(RetroPhase.SetTheStage);
			var sut = new RetrospectivePlan();

			// Act
			var result = sut.ChangePlanWithActivity(activityToInsert);

			// Assert
			Assert.That(result.SetTheStage, Is.EqualTo(activityToInsert));
			Assert.That(result.GatherData, Is.EqualTo(sut.GatherData));
			Assert.That(result.GenerateInsights, Is.EqualTo(sut.GenerateInsights));
			Assert.That(result.DecideWhatToDo, Is.EqualTo(sut.DecideWhatToDo));
			Assert.That(result.CloseTheRetrospective, Is.EqualTo(sut.CloseTheRetrospective));
		}

		[Test]
		public void ChangePlanWithActivity_Should_ReturnPlanWithNewGatherData_When_GatherDataPassed()
		{
			// Arrange
			var activityToInsert = CreateTestActivityWithPhase(RetroPhase.GatherData);
			var sut = new RetrospectivePlan();

			// Act
			var result = sut.ChangePlanWithActivity(activityToInsert);

			// Assert
			Assert.That(result.SetTheStage, Is.EqualTo(sut.SetTheStage));
			Assert.That(result.GatherData, Is.EqualTo(activityToInsert));
			Assert.That(result.GenerateInsights, Is.EqualTo(sut.GenerateInsights));
			Assert.That(result.DecideWhatToDo, Is.EqualTo(sut.DecideWhatToDo));
			Assert.That(result.CloseTheRetrospective, Is.EqualTo(sut.CloseTheRetrospective));
		}

		[Test]
		public void ChangePlanWithActivity_Should_ReturnPlanWithNewGenerateInsights_When_GenerateInsightsPassed()
		{
			// Arrange
			var activityToInsert = CreateTestActivityWithPhase(RetroPhase.GenerateInsights);
			var sut = new RetrospectivePlan();

			// Act
			var result = sut.ChangePlanWithActivity(activityToInsert);

			// Assert
			Assert.That(result.SetTheStage, Is.EqualTo(sut.SetTheStage));
			Assert.That(result.GatherData, Is.EqualTo(sut.GatherData));
			Assert.That(result.GenerateInsights, Is.EqualTo(activityToInsert));
			Assert.That(result.DecideWhatToDo, Is.EqualTo(sut.DecideWhatToDo));
			Assert.That(result.CloseTheRetrospective, Is.EqualTo(sut.CloseTheRetrospective));
		}

		[Test]
		public void ChangePlanWithActivity_Should_ReturnPlanWithNewDecideWhatToDo_When_DecideWhatToDoPassed()
		{
			// Arrange
			var activityToInsert = CreateTestActivityWithPhase(RetroPhase.DecideWhatToDo);
			var sut = new RetrospectivePlan();

			// Act
			var result = sut.ChangePlanWithActivity(activityToInsert);

			// Assert
			Assert.That(result.SetTheStage, Is.EqualTo(sut.SetTheStage));
			Assert.That(result.GatherData, Is.EqualTo(sut.GatherData));
			Assert.That(result.GenerateInsights, Is.EqualTo(sut.GenerateInsights));
			Assert.That(result.DecideWhatToDo, Is.EqualTo(activityToInsert));
			Assert.That(result.CloseTheRetrospective, Is.EqualTo(sut.CloseTheRetrospective));
		}

		[Test]
		public void ChangePlanWithActivity_Should_ReturnPlanWithNewCloseTheRetro_When_CloseTheRetroPassed()
		{
			// Arrange
			var activityToInsert = CreateTestActivityWithPhase(RetroPhase.CloseTheRetrospective);
			var sut = new RetrospectivePlan();

			// Act
			var result = sut.ChangePlanWithActivity(activityToInsert);

			// Assert
			Assert.That(result.SetTheStage, Is.EqualTo(sut.SetTheStage));
			Assert.That(result.GatherData, Is.EqualTo(sut.GatherData));
			Assert.That(result.GenerateInsights, Is.EqualTo(sut.GenerateInsights));
			Assert.That(result.DecideWhatToDo, Is.EqualTo(sut.DecideWhatToDo));
			Assert.That(result.CloseTheRetrospective, Is.EqualTo(activityToInsert));
		}

		[Test]
		[TestCase(RetroPhase.SetTheStage)]
		[TestCase(RetroPhase.GatherData)]
		[TestCase(RetroPhase.GenerateInsights)]
		[TestCase(RetroPhase.DecideWhatToDo)]
		[TestCase(RetroPhase.CloseTheRetrospective)]
		public void GetActivityForPhase_Should_ReturnCorrectActivityForPhase(RetroPhase testPhase)
		{
			// Arrange
			var sut = CreateTestRetroPlan();
			var allActivities = new List<ActivityWithTimebox>
			{
				sut.SetTheStage,
				sut.GatherData,
				sut.GenerateInsights,
				sut.DecideWhatToDo,
				sut.CloseTheRetrospective
			};
			var expectedActivity = allActivities.First(a => a.Activity.Phase == testPhase);

			// Act
			var result = sut.GetActivityForPhase(testPhase);

			// Assert
			Assert.That(result, Is.EqualTo(expectedActivity));
		}

		static RetrospectivePlan CreateTestRetroPlan()
		{
			return new RetrospectivePlan
			{
				SetTheStage = CreateTestActivityWithPhase(RetroPhase.SetTheStage),
				GatherData = CreateTestActivityWithPhase(RetroPhase.GatherData),
				GenerateInsights = CreateTestActivityWithPhase(RetroPhase.GenerateInsights),
				DecideWhatToDo = CreateTestActivityWithPhase(RetroPhase.DecideWhatToDo),
				CloseTheRetrospective = CreateTestActivityWithPhase(RetroPhase.CloseTheRetrospective),
			};
		}

		static ActivityWithTimebox CreateTestActivityWithPhase(RetroPhase phase)
		{
			var phaseName = phase.ToString();
			var a = new Activity(42, phase, phaseName + " name");

			return new ActivityWithTimebox(a, TimeSpan.FromMinutes(42));
		}
	}
}
