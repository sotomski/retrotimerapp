﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using Moq;
using NUnit.Framework;
using Retrotimer;
using Retrotimer.Common;
using Retrotimer.Model;
using Retrotimer.Services;
using Retrotimer.Services.Timer;

namespace Model
{
	// Since instances of this class communicate with the outside world 
	// using encapsulted store (playlist), this part will also
	// be treated as public/testable part and not as implementation detail.
	[TestFixture]
	public class RetrospectivePlayerTest
	{
		Mock<IMessenger> messengerMock;
 		Mock<ICountdownTimer> timerMock;
		Mock<ICountdownTimerProvider> timerProvMock;

		List<IRetroPlaylistItem> playlist;
		bool wasRetroFinishedRaised;
		Action<TimeSpan> timerTickedCallback;
		Action timerFinishedCallback;
		TimeSpan timerRemainingTime;

		RetrospectivePlayerForTests sut;

		[SetUp]
		public void Setup()
		{
			timerMock = SetupTimerMock();

			timerProvMock = new Mock<ICountdownTimerProvider>();
			timerProvMock.Setup(ctp => ctp.CreateTimer())
						 .Returns(timerMock.Object);

			messengerMock = new Mock<IMessenger>();
			Messenger.OverrideDefault(messengerMock.Object);
		}

		Mock<ICountdownTimer> SetupTimerMock()
		{
			var mock = new Mock<ICountdownTimer>();
			mock.Setup(tm => tm.StartCountdown(It.IsAny<CountdownInfo>()))
					 .Callback<CountdownInfo>(ci =>
			{
				timerTickedCallback = ci.UpdateRemainingTimeCallback;
				timerFinishedCallback = ci.FinishCallback;
			});

			return mock;
		}

		[TearDown]
		public void Teardown()
		{
			timerProvMock = null;
			timerMock = null;

			playlist = null;
			wasRetroFinishedRaised = false;
			timerTickedCallback = null;
			timerRemainingTime = TimeSpan.Zero;

			Messenger.Reset();
		}

		[Test]
		public void Ctor_Should_ThrowArgumentNullException_When_InitialPlaylistIsNull()
		{
			// Act
			TestDelegate test = () => new RetrospectivePlayer(null, timerProvMock.Object);

			// Assert
			Assert.That(test, Throws.ArgumentNullException.With.Property("ParamName").EqualTo("initialPlaylist"));
		}

		[Test]
		public void Ctor_Should_ThrowArgumentNullException_When_CountdownProviderIsNull()
		{
			GivenEmptyPlaylist();

			// Act
			TestDelegate test = () => new RetrospectivePlayer(playlist, null);

			// Assert
			Assert.That(test, Throws.ArgumentNullException.With.Property("ParamName").EqualTo("timerProvider"));
		}

		[Test]
		public void Start_Should_ImmediatelyRaiseRetrospectiveFinishedEvent_When_PlaylistEmpty()
		{
			GivenEmptyPlaylist();

			WhenPlayerIsStarted();

			ThenRetrospectiveFinishedEventWasRaised();
		}

		[Test]
		public void Start_Should_StartTimerWithOneSecInterval_When_PlaylistHasAtLeastOneElement()
		{
			GivenPlaylistWithOneItem();

			WhenPlayerIsStarted();

			ThenTimerIntervalIsOneSecond();
		}

		[Test]
		public void Start_Should_StartTimerWithPlaylistElementOriginalTimebox_When_PlaylistHasOneElement()
		{
			GivenPlaylistWithOneItem();

			WhenPlayerIsStarted();

			ThenTimerStartedCountdownOf(playlist.First());
		}

		[Test]
		public void TimerTick_Should_UpdateTheOnlyItemAndContinueCountdown_When_RemainingTimeGreaterThanZero()
		{
			GivenPlaylistWithOneItem();
			GivenRemainingTimeIsGreaterThanZero();

			WhenPlayerIsStarted();
			WhenTimerTicks();

			ThenFirstItemRemainingTimeboxIsEqualTimerTick();
			ThenRetrospectiveFinishedEventWasNotRaised();
		}

		[Test]
		public void Start_Should_StartCountdownWithFirstTimebox_When_NewlyCreated()
		{
			GivenPlaylistWithMultipleItems();

			WhenPlayerIsStarted();

			ThenTimerStartedCountdownOf(playlist.First());
		}

		[Test]
		public void Start_Should_SetPlaylistItemRemainingTimeboxToZero_When_PlaylistHasOneElementAndCountdownReachesZero()
		{
			GivenPlaylistWithOneItem();

			WhenPlayerIsStarted();
			WhenTimerFinishesCountdown();

			ThenFirstItemRemainingTimeboxIsZero();
		}

		[Test]
		public void Start_Should_UpdateOnlyFirstItem_When_FirstItemCountdownInProgress()
		{
			GivenPlaylistWithMultipleItems();
			var first = playlist.First();
			timerRemainingTime = TimeSpan.FromSeconds(
				first.OriginalTimebox.TotalSeconds / 2);

			WhenPlayerIsStarted();
			WhenTimerTicks();

			ThenOnlyHeadOfListIsUpdated(playlist);
		}

		[Test]
		public void Start_Should_PublishCountdownForStageCompleted_When_ItemCountdownFinished()
		{
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfFirstItem();

			WhenPlayerIsStarted();
			WhenTimerFinishesCountdown();

			ThenNotificationIsBroadcasted(MessageCommunicationProtocol.CountdownStageChanged);
		}

		[Test]
		public void Start_Should_NotPublishCountdownForStageCompleted_When_LastItemCountdownFinished()
		{
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfSecondItem();

			WhenPlayerIsStarted();
			messengerMock.ResetCalls();
			WhenTimerFinishesCountdown();

			ThenNotificationNotBroadcasted(MessageCommunicationProtocol.CountdownStageChanged);
		}

		[Test]
		public void Start_Should_RaisePlayedItemChanged_When_ItemCountdownFinished()
		{
			GivenPlaylistWithMultipleItems();
			var second = playlist.ElementAt(1);
			EventArgs<IRetroPlaylistItem> args = null;
			sut.PlayedItemChanged += (sender, e) => args = e;

			WhenPlayerIsStarted();
			WhenTimerFinishesCountdown();

			Assert.That(args.Value, Is.EqualTo(second));
		}

		[Test]
		public void Start_Should_RaisePlayedItemChanged_When_CountdownChangedToLastItem()
		{
			GivenPlaylistWithMultipleItems();
			var last = playlist.Last();
			EventArgs<IRetroPlaylistItem> args = null;
			sut.PlayedItemChanged += (sender, e) => args = e;
			WhenPlayerIsStarted();
			GivenCountdownOfSecondToLastItem();

			WhenTimerFinishesCountdown();

			Assert.That(args.Value, Is.EqualTo(last));
		}

		[Test]
		public void Start_Should_ImmediatelyRaisePlayedItemChangedWithFirstItem()
		{
			// Arrange
			GivenPlaylistWithMultipleItems();
			var first = playlist.First();
			EventArgs<IRetroPlaylistItem> args = null;
			sut.PlayedItemChanged += (sender, e) => args = e;

			// Act
			WhenPlayerIsStarted();

			// Assert
			Assert.That(args.Value, Is.EqualTo(first));
		}

		[Test]
		public void Reset_Should_RaisePlayedItemChangedWithNull()
		{
			// Arrange
			GivenPlaylistWithMultipleItems();
			EventArgs<IRetroPlaylistItem> args = null;
			sut.PlayedItemChanged += (sender, e) => args = e;

			// Act
			WhenPlayerIsStarted();
			WhenPlayerIsReset();

			// Assert
			Assert.That(args.Value, Is.Null);
		}

		[Test]
		public void PlayedItemChanged_Should_BeRaisedWithNull_When_LastCountdownFinished()
		{
			// Arrange
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfSecondItem();
			EventArgs<IRetroPlaylistItem> args = null;
			sut.PlayedItemChanged += (sender, e) => args = e;

			// Act
			WhenPlayerIsStarted();
			timerFinishedCallback();

			// Assert
			Assert.That(args.Value, Is.Null);
		}

		[Test]
		public void Start_Should_ZeroAllTimeboxes_When_LastCountdownFinished()
		{
			// Arrange
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfSecondToLastItem();

			// Act
			WhenPlayerIsStarted();
			timerFinishedCallback();

			ThenAllRemainingTimeboxesAreZero();
		}

		[Test]
		public void Start_Should_SendCountdownFinishedNotification_When_CountdownFinishesForMultipleItems()
		{
			GivenPlaylistWithOneItem();

			WhenPlayerIsStarted();
			WhenTimerFinishesCountdown();

			ThenNotificationIsBroadcasted(MessageCommunicationProtocol.CountdownCompleted);
		}

		[Test]
		public void Reset_Should_StopTheCountdown()
		{
			GivenPlaylistWithMultipleItems();

			WhenPlayerIsStarted();
			WhenPlayerIsReset();

			ThenTimerIsStopped();
		}

		[Test]
		public void Reset_Should_AssignOriginalValuesToAllPlaylistItems()
		{
			GivenPlaylistWithMultipleItems();
			GivenAllItemsRemainingTimeboxesWereUpdated();

			WhenPlayerIsStarted();
			WhenPlayerIsReset();

			ThenAllRemainingTimeboxesAreReset();
		}

		[Test]
		public void Pause_Should_PauseTheCountdown_When_TimerIsRunning()
		{
			GivenPlaylistWithMultipleItems();

			WhenPlayerIsStarted();
			WhenPlayerIsPaused();

			ThenTimerIsPaused();
		}

		[Test]
		public void Pause_Should_NotCauseProblems_When_CalledAndCountdownIsNotActive()
		{
			GivenPlaylistWithMultipleItems();

			WhenPlayerIsPaused();

			ThenTimerWasNeverCreated();
		}

		[Test]
		public void Start_ShouldResumeCountdown_When_TimerIsPaused()
		{
			GivenPlaylistWithMultipleItems();

			WhenPlayerIsStarted();
			WhenPlayerIsPaused();
			WhenPlayerIsStarted();

			ThenTimerIsResumedAndNoNewTimersAreCreated();
		}

		[Test]
		public void CheckIfPlayingFirstItem_Should_ReturnFalse_When_CountdownNotActive()
		{
			GivenPlaylistWithMultipleItems();

			ThenCheckIfPlayingFirstItemIs(false);
		}

		[Test]
		public void CheckIfPlayingFirstItem_Should_ReturnFalse_When_PlaylistEmpty()
		{
			GivenEmptyPlaylist();

			WhenPlayerIsStarted();

			ThenCheckIfPlayingFirstItemIs(false);
		}

		[Test]
		public void CheckIfPlayingFirstItem_Should_ReturnTrue_When_ActiveCountdownOfFirstItem() 
		{
			GivenPlaylistWithMultipleItems();

			WhenPlayerIsStarted();
			WhenTimerTicks();

			ThenCheckIfPlayingFirstItemIs(true);
		}

		[Test]
		public void CheckIfPlayingFirstItem_Should_ReturnTrue_When_ActiveCountdownOfFirstItemBeforeFirstTick()
		{
			GivenPlaylistWithMultipleItems();

			WhenPlayerIsStarted();

			ThenCheckIfPlayingFirstItemIs(true);
		}

		[Test]
		public void CheckIfPlayingFirstItem_Should_ReturnTrue_When_PausedCountdownOfFirstItem()
		{
			GivenPlaylistWithMultipleItems();

			WhenPlayerIsStarted();
			WhenTimerTicks();
			WhenPlayerIsPaused();

			ThenCheckIfPlayingFirstItemIs(true);
		}

		[Test]
		public void CheckIfPlayingFirstItem_Shoud_ReturnFalse_When_ActiveCountdownOfNonFirstItem()
		{
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfSecondItem();

			WhenPlayerIsStarted();
			WhenTimerTicks();

			ThenCheckIfPlayingFirstItemIs(false);
		}

		[Test]
		public void CheckIfPlayingLastItem_Should_ReturnFalse_When_CountdownNotActive()
		{
			GivenPlaylistWithMultipleItems();

			ThenCheckIfPlayingLastItemIs(false);
		}

		[Test]
		public void CheckIfPlayingLastItem_Should_ReturnFalse_When_PlaylistEmpty()
		{
			GivenEmptyPlaylist();

			WhenPlayerIsStarted();

			ThenCheckIfPlayingLastItemIs(false);
		}

		[Test]
		public void CheckIfPlayingLastItem_Should_ReturnTrue_When_ActiveCountdownOfLastItem()
		{
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfSecondToLastItem();

			WhenPlayerIsStarted();

			ThenCheckIfPlayingLastItemIs(true);
		}

		[Test]
		public void CheckIfPlayingLastItem_Should_ReturnTrue_When_PausedCountdownOfLastItem()
		{
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfSecondToLastItem();

			WhenPlayerIsStarted();
			WhenTimerTicks();
			WhenPlayerIsPaused();

			ThenCheckIfPlayingLastItemIs(true);
		}

		[Test]
		public void CheckIfPlayingLastItem_Shoud_ReturnFalse_When_ActiveCountdownOfNonLastItem()
		{
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfFirstItem();

			WhenPlayerIsStarted();
			WhenTimerTicks();

			ThenCheckIfPlayingLastItemIs(false);
		}

		[Test]
		public void MoveForward_Should_ThrowInvalidOperationException_When_CountdownInactive()
		{
			GivenPlaylistWithMultipleItems();

			ThenThrowsInvalidOperationException(sut.MoveForwards);
		}

		[Test]
		public void MoveForward_Should_ThrowInvalidOperationException_When_PlaylistEmpty()
		{
			GivenEmptyPlaylist();

			WhenPlayerIsStarted();

			ThenThrowsInvalidOperationException(sut.MoveForwards);
		}

		[Test]
		public void MoveForward_Should_SkipPlaybackToSecondItem_When_ActiveCountdownOfFirstItem()
		{
			GivenPlaylistWithMultipleItems();

			WhenPlayerIsStarted();
			sut.MoveForwards();

			ThenCountdownIsSetOnItem(playlist.ElementAt(1));
		}

		[Test]
		public void MoveForward_Should_SkipPlaybackToLastItem_When_ActiveCountdownOfSecondToLastItem()
		{
			GivenPlaylistWithMultipleItems();
			var thirdToLast = playlist.ElementAt(playlist.Count - 3);
			GivenCountdownOfItem(thirdToLast);

			WhenPlayerIsStarted();
			sut.MoveForwards();

			ThenCountdownIsSetOnItem(playlist.Last());
		}

		[Test]
		public void MoveForward_Should_EndCountdown_When_ActiveCountdownOfLastItem()
		{
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfSecondToLastItem();

			WhenPlayerIsStarted();
			sut.MoveForwards();

			ThenAllRemainingTimeboxesAreZero();
			Assert.That(sut.Cursor.Current, Is.Null);
		}

		[Test]
		public void MoveForward_Should_KeepTimerPaused_When_TimerWasPausedBefore()
		{
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfFirstItem();

			WhenPlayerIsStarted();
			WhenTimerTicks();
			WhenPlayerIsPaused();
			sut.MoveForwards();

			ThenPlayerIsPaused();
		}

		[Test]
		public void MoveForward_Should_RaisePlayedItemChanged()
		{
			GivenPlaylistWithMultipleItems();
			var second = playlist.Skip(1).First();
			var wasEventRaised = false;
			sut.PlayedItemChanged += (sender, e) => wasEventRaised |= (e.Value == second);

			WhenPlayerIsStarted();
			sut.MoveForwards();

			Assert.That(wasEventRaised, Is.True);
		}

		[Test]
		public void MoveForward_Should_PublishCountdownForStageCompleted()
		{
			GivenPlaylistWithMultipleItems();

			WhenPlayerIsStarted();
			sut.MoveForwards();

			ThenNotificationIsBroadcasted(MessageCommunicationProtocol.CountdownStageChanged);
		}

		[Test]
		public void MoveBackwards_Should_ThrowInvalidOperationException_When_CountdownInactive()
		{
			GivenPlaylistWithMultipleItems();

			ThenThrowsInvalidOperationException(sut.MoveBackwards);
		}

		[Test]
		public void MoveBackwards_Should_ThrowInvalidOperationException_When_PlaylistEmpty()
		{
			GivenEmptyPlaylist();

			WhenPlayerIsStarted();

			ThenThrowsInvalidOperationException(sut.MoveBackwards);
		}

		[Test]
		public void MoveBackwards_Should_MovePlaybackToSecondToLastItem_When_ActiveCountdownOfLastItem()
		{
			GivenPlaylistWithMultipleItems();
			var second = GivenCountdownOfSecondItem();

			WhenPlayerIsStarted();
			sut.MoveBackwards();

			ThenCountdownIsSetOnItem(second);
		}

		[Test]
		public void MoveBackwards_Should_MovePlaybackToFirstItem_When_ActiveCountdownOfSecondItem()
		{
			GivenPlaylistWithMultipleItems();
			var first = GivenCountdownOfFirstItem();

			WhenPlayerIsStarted();
			sut.MoveBackwards();

			ThenCountdownIsSetOnItem(first);
		}

		[Test]
		public void MoveBackwards_Should_ResetFirstItemTimebox_When_ActiveCountdownOfFirstItem()
		{
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfFirstItem();

			WhenPlayerIsStarted();
			sut.MoveBackwards();

			// Assert
			var second = playlist.ElementAt(1);
			Assert.That(second.RemainingTimebox, Is.EqualTo(second.OriginalTimebox));
		}

		[Test]
		public void MoveBackwards_Should_ResetPreviousTimebox()
		{
			GivenPlaylistWithMultipleItems();
			var first = GivenCountdownOfFirstItem();

			WhenPlayerIsStarted();
			sut.MoveBackwards();

			ThenCountdownIsSetOnItem(first);
			Assert.That(first.RemainingTimebox, Is.EqualTo(first.OriginalTimebox));
		}

		[Test]
		public void MoveBackwards_Should_KeepTimerPaused_When_TimerWasPausedBefore()
		{
			GivenPlaylistWithMultipleItems();
			var second = GivenCountdownOfSecondItem();

			WhenPlayerIsStarted();
			WhenPlayerIsPaused();
			sut.MoveBackwards();

			ThenPlayerIsPaused();
			ThenCountdownIsSetOnItem(second);
		}

		[Test]
		public void MoveBackwards_Should_RaisePlayedItemChanged()
		{
			GivenPlaylistWithMultipleItems();
			var second = GivenCountdownOfSecondItem();
			var wasEventRaised = false;
			sut.PlayedItemChanged += (sender, e) => wasEventRaised |= (e.Value == second);

			WhenPlayerIsStarted();
			sut.MoveBackwards();

			Assert.That(wasEventRaised, Is.True);
		}

		[Test]
		public void MoveBackwards_Should_PublishCountdownForStageCompleted()
		{
			GivenPlaylistWithMultipleItems();
			GivenCountdownOfSecondItem();

			WhenPlayerIsStarted();
			sut.MoveBackwards();

			ThenNotificationIsBroadcasted(MessageCommunicationProtocol.CountdownStageChanged);
		}

		#region Helper methods

		void GivenEmptyPlaylist()
		{
			playlist = new List<IRetroPlaylistItem>();

			PrepareSut();
		}

		void GivenPlaylistWithOneItem()
		{
			var item = Mock.Of<IRetroPlaylistItem>(i => i.OriginalTimebox == TimeSpan.FromMinutes(8)
												   && i.RemainingTimebox == i.OriginalTimebox);
			playlist = new List<IRetroPlaylistItem> { item };

			PrepareSut();
		}

		void GivenPlaylistWithMultipleItems()
		{
			var timeOne = TimeSpan.FromMinutes(8);
			var timeTwo = TimeSpan.FromMinutes(7);
			var timeThree = TimeSpan.FromMinutes(6);

			playlist = new List<IRetroPlaylistItem>
			{
				Mock.Of<IRetroPlaylistItem>(i => i.OriginalTimebox == timeOne
												   && i.RemainingTimebox == i.OriginalTimebox),
				Mock.Of<IRetroPlaylistItem>(i => i.OriginalTimebox == timeTwo
												   && i.RemainingTimebox == i.OriginalTimebox),
				Mock.Of<IRetroPlaylistItem>(i => i.OriginalTimebox == timeThree
												   && i.RemainingTimebox == i.OriginalTimebox)
			};

			PrepareSut();
		}

		void GivenAllItemsRemainingTimeboxesWereUpdated()
		{
			playlist.ElementAt(0).RemainingTimebox = TimeSpan.FromSeconds(0.1);
			playlist.ElementAt(1).RemainingTimebox = TimeSpan.FromSeconds(0.2);
			playlist.ElementAt(2).RemainingTimebox = TimeSpan.FromSeconds(0.3);
		}

		void GivenRemainingTimeIsGreaterThanZero()
		{
			timerRemainingTime = TimeSpan.FromSeconds(1);
		}

		IRetroPlaylistItem GivenCountdownOfFirstItem()
		{
			var item = playlist.First();
			GivenCountdownOfItem(item);
			return item;
		}

		IRetroPlaylistItem GivenCountdownOfSecondItem()
		{
			var item = playlist.Skip(1).First();
			GivenCountdownOfItem(item);
			return item;
		}

		IRetroPlaylistItem GivenCountdownOfSecondToLastItem()
		{
			var item = playlist.Reverse<IRetroPlaylistItem>().Skip(1).First();
			GivenCountdownOfItem(item);
			return item;
		}

		IRetroPlaylistItem GivenCountdownOfLastItem()
		{
			var item = playlist.Last();
			GivenCountdownOfItem(item);
			return item;
		}

		void GivenCountdownOfItem(IRetroPlaylistItem item)
		{
			GivenCountdownOfItem(item, TimeSpan.FromSeconds(item.OriginalTimebox.TotalSeconds / 2));
		}

		void GivenCountdownOfItem(IRetroPlaylistItem item, TimeSpan remainingItemTimebox)
		{
			var howManyFinishedEvents = playlist.IndexOf(item);
			sut.Cursor.Reset();
			for (int i = 0; i <= howManyFinishedEvents; i++)
			{
				sut.Cursor.MoveNext();
			}

			sut.Cursor.Current.RemainingTimebox = remainingItemTimebox;
		}

		void WhenPlayerIsStarted()
		{
			sut.StartOrResume();
		}

		void WhenTimerTicks()
		{
			timerTickedCallback(timerRemainingTime);
		}

		void WhenTimerFinishesCountdown()
		{
			timerFinishedCallback();
		}

		void WhenPlayerIsReset()
		{
			sut.Reset();
		}

		void WhenPlayerIsPaused()
		{
			sut.Pause();
			timerMock.SetupGet(tm => tm.IsPaused).Returns(true);
		}

		void ThenRetrospectiveFinishedEventWasRaised()
		{
			Assert.That(wasRetroFinishedRaised, Is.True);
		}

		void ThenRetrospectiveFinishedEventWasNotRaised()
		{
			Assert.That(wasRetroFinishedRaised, Is.False);
		}

		void ThenTimerIntervalIsOneSecond()
		{
			var expectedInterval = TimeSpan.FromSeconds(1);
			timerMock.Verify(tm => tm.StartCountdown(It.Is<CountdownInfo>(ci => ci.UpdateInterval == expectedInterval)));
		}

		void ThenTimerStartedCountdownOf(IRetroPlaylistItem item)
		{
			var expectedCountdown = item.OriginalTimebox;
			timerMock.Verify(t => t.StartCountdown(It.Is<CountdownInfo>(ci => ci.Countdown == expectedCountdown)));
		}

		void ThenTimerIsStopped()
		{
			timerMock.Verify(tm => tm.Stop(), Times.Once());
		}

		void ThenTimerIsPaused()
		{
			timerMock.Verify(tm => tm.Pause(), Times.Once());
		}

		void ThenTimerWasNeverCreated()
		{
			timerProvMock.Verify(tpm => tpm.CreateTimer(), Times.Never());
		}

		void ThenTimerIsResumedAndNoNewTimersAreCreated()
		{
			timerMock.Verify(t => t.Resume(), Times.Once());
			timerProvMock.Verify(tp => tp.CreateTimer(), Times.Once());
		}

		void ThenFirstItemRemainingTimeboxIsEqualTimerTick()
		{
			Assert.That(playlist.First().RemainingTimebox, Is.EqualTo(timerRemainingTime));
		}

		void ThenFirstItemRemainingTimeboxIsZero()
		{
			Assert.That(playlist.First().RemainingTimebox, Is.EqualTo(TimeSpan.Zero));
		}

		void ThenAllRemainingTimeboxesAreZero()
		{
			Assert.That(playlist, Has.All.Matches<IRetroPlaylistItem>(i => i.RemainingTimebox == TimeSpan.Zero));
		}

		void ThenAllRemainingTimeboxesAreReset()
		{
			Assert.That(playlist, Has.All.Matches<IRetroPlaylistItem>(i => i.OriginalTimebox == i.RemainingTimebox));
		}

		void ThenOnlyHeadOfListIsUpdated(IEnumerable<IRetroPlaylistItem> items)
		{
			var head = items.First();
			var others = items.Skip(1);

			var expectedRemainingTimebox = timerRemainingTime;

			Assert.That(head.RemainingTimebox, Is.EqualTo(expectedRemainingTimebox));
			Assert.That(others, Has.All.Matches<IRetroPlaylistItem>(i => i.OriginalTimebox == i.RemainingTimebox));
		}

		void ThenNotificationIsBroadcasted(string notif)
		{
			messengerMock.Verify(
				mm => mm.Send(
					It.Is<NotificationMessage>(
						msg => msg.Sender == sut
						&& msg.Notification == notif)));
		}

		void ThenNotificationNotBroadcasted(string notif)
		{
			messengerMock.Verify(
				mm => mm.Send(
					It.Is<NotificationMessage>(
						msg => msg.Sender == sut
						&& msg.Notification == notif)), Times.Never());
		}

		void ThenCheckIfPlayingFirstItemIs(bool expected)
		{
			Assert.That(sut.CheckIfPlayingFirstItem(), Is.EqualTo(expected));
		}

		void ThenCheckIfPlayingLastItemIs(bool expected)
		{
			Assert.That(sut.CheckIfPlayingLastItem(), Is.EqualTo(expected));
		}

		static void ThenThrowsInvalidOperationException(TestDelegate test)
		{
			Assert.That(test, Throws.InvalidOperationException);
		}

		void ThenCountdownIsSetOnItem(IRetroPlaylistItem item)
		{
			Assert.That(sut.Cursor.Current, Is.EqualTo(item));

			timerMock.Verify(tm => tm.StartCountdown(It.Is<CountdownInfo>(ci => ci.Countdown == item.OriginalTimebox)));
		}

		void ThenPlayerIsPaused()
		{
			timerMock.Verify(tm => tm.StartCountdown(It.IsAny<CountdownInfo>()), Times.Exactly(2));
			timerMock.Verify(tm => tm.Pause(), Times.Exactly(2));
		}

		void PrepareSut()
		{
			sut = new RetrospectivePlayerForTests(playlist, timerProvMock.Object);
			sut.RetrospectiveFinished += (sender, e) => wasRetroFinishedRaised = true;
		}

		static TimeSpan GetSumOfOriginalTimeboxes(IEnumerable<IRetroPlaylistItem> items)
		{
			var sumOfTimeboxes = items.Select(i => i.OriginalTimebox.Ticks).Sum();
			return TimeSpan.FromTicks(sumOfTimeboxes);
		}

		class RetrospectivePlayerForTests : RetrospectivePlayer
		{
			public Cursor<IRetroPlaylistItem> Cursor
			{
				get
				{
					return cursor;
				}
			}

			public RetrospectivePlayerForTests(IList<IRetroPlaylistItem> initialPlaylist, ICountdownTimerProvider timerProvider)
			: base(initialPlaylist, timerProvider)
			{
			}
		}

		#endregion
	}
}
