﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data;
using Moq;
using NUnit.Framework;
using Retrotimer.Data;
using Retrotimer.Model;
using Retrotimer.Services;
using Retrotimer.Services.Data;

namespace Services.Data
{
    [TestFixture]
    public class DataServiceTest
    {
        private Mock<IDataRepository> localRepoMock;
        private Mock<IDataSource> remoteSourceMock;
        private IDataService sut;

        [SetUp]
        public void Setup()
        {
            localRepoMock = new Mock<IDataRepository>();
            remoteSourceMock = new Mock<IDataSource>();

            sut = new DataService(localRepoMock.Object, remoteSourceMock.Object);
        }
        
        [Test]
        public void Ctor_Should_Throw_When_LocalRepoIsNull()
        {
            // Act
            void Test() => new DataService(null, remoteSourceMock.Object);

            // Assert
            Assert.That(Test, Throws.ArgumentNullException.With.Property("ParamName").EqualTo("localRepo"));
        }
        
        [Test]
        public void Ctor_Should_Throw_When_RemoteSourceIsNull()
        {
            // Act
            void Test() => new DataService(localRepoMock.Object, null);

            // Assert
            Assert.That(Test, Throws.ArgumentNullException.With.Property("ParamName").EqualTo("remoteSource"));
        }

        [Test]
        public async Task ReadActivitiesAsync_Should_ReturnLocalActivities_When_Available()
        {
            // Arrange
            var testData = TestData.CreateTestActivities();
            localRepoMock.Setup(lr => lr.ReadAllAsync<Activity>()).ReturnsAsync(testData);
            var wasNewActivitiesAvailableEventRaised = false;
            sut.NewActivitiesAvailable += (sender, e) => wasNewActivitiesAvailableEventRaised = true;
            
            // Act
            var actual = await sut.ReadActivitiesAsync();

            // Assert
            Assert.That(actual, Is.EqualTo(testData));
            Assert.That(wasNewActivitiesAvailableEventRaised, Is.False);
        }

        [Test]
        public async Task ReadActivitiesAsync_Should_SaveLoadedItemsInMemory()
        {
            // Arrange
            var testData = TestData.CreateTestActivities();
            localRepoMock.Setup(lr => lr.ReadAllAsync<Activity>()).ReturnsAsync(testData);

            // Act
            var first = await sut.ReadActivitiesAsync();
            var second = await sut.ReadActivitiesAsync();

            // Assert
            Assert.That(first, Is.EqualTo(second));
            localRepoMock.Verify(lr => lr.ReadAllAsync<Activity>(), Times.Once());
        }

        [Test]
        public async Task ReadActivitiesAsync_Should_FetchDataFromRemoteSource_When_NoLocalActivitiesAvailable()
        {
            // Arrange
            var testData = TestData.CreateTestActivities();
            remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ReturnsAsync(testData);
            localRepoMock.Setup(lr => lr.ReadAllAsync<Activity>()).ReturnsAsync(new List<Activity>());
            
            // Act
            var actual = await sut.ReadActivitiesAsync();

            // Assert
            Assert.That(actual, Is.EqualTo(testData));
        }

        [Test]
        public async Task ReadActivitiesAsync_Should_ReturnEmptyCollection_When_BothSourcesReturnEmtpyCollections()
        {
            // Arrange
            remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ReturnsAsync(new List<Activity>());
            localRepoMock.Setup(lr => lr.ReadAllAsync<Activity>()).ReturnsAsync(new List<Activity>());
            
            // Act
            var actual = await sut.ReadActivitiesAsync();
            
            // Assert
            Assert.That(actual, Is.Empty);
        }
        
        [Test]
        public async Task ReadActivitiesAsync_Should_SaveNewActivitiesLocally_When_FetchedDataFromRemoteSource()
        {
            // Arrange
            var testData = TestData.CreateTestActivities();
            remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ReturnsAsync(testData);
            localRepoMock.Setup(lr => lr.ReadAllAsync<Activity>()).ReturnsAsync(new List<Activity>());

            // Act
            await sut.ReadActivitiesAsync();

            // Assert
            localRepoMock.Verify(rp => rp.SaveAllAsync(testData));
        }

        [Test]
        public async Task ReadActivitiesAsync_Should_GetDataFromRemoteSource_When_LocalSourceThrowsException()
        {
            // Arrange
            localRepoMock.Setup(lr => lr.ReadAllAsync<Activity>()).ThrowsAsync(new Exception("Fail"));
            var testData = TestData.CreateTestActivities();
            remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ReturnsAsync(testData);
            
            // Act
            var actual = await sut.ReadActivitiesAsync();
            
            // Assert
            Assert.That(actual, Is.EqualTo(testData));
        }

        [Test]
        public async Task ReadActivitiesAsync_Should_RaiseNewActivitiesAvailable_WhenGettingDataFomrRemote()
        {
            // Arrange
            var testData = TestData.CreateTestActivities();
            IEnumerable<Activity> raisedActivities = null;
            remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ReturnsAsync(testData);
            localRepoMock.Setup(lr => lr.ReadAllAsync<Activity>()).ReturnsAsync(new List<Activity>());

            // Act
            sut.NewActivitiesAvailable += (sender, e) => raisedActivities = e.Value;
            var actual = await sut.ReadActivitiesAsync();

            // Assert
            Assert.That(raisedActivities, Is.EqualTo(testData));
        }

        [Test]
        public void ReadActivitiesAsync_Should_ThrowDataServiceException_When_LocalAndRemoteThrowExceptions()
        {
            // Arrange
            localRepoMock.Setup(lr => lr.ReadAllAsync<Activity>()).ThrowsAsync(new Exception("Fail"));
            remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ThrowsAsync(new Exception("Remote fails"));
            
            // Act
            AsyncTestDelegate test = async () => await sut.ReadActivitiesAsync();
            
            // Assert
            Assert.That(test, Throws.TypeOf<DataServiceException>());
        }

        [Test]
        public async Task ReadActivitiesAsync_Should_IgnoreExceptions_When_StoringDataLocally()
        {
            // Arrange
            var testData = TestData.CreateTestActivities();
            remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ReturnsAsync(testData);
            localRepoMock.Setup(lr => lr.ReadAllAsync<Activity>()).ReturnsAsync(new List<Activity>());
            localRepoMock.Setup(lr => lr.SaveAllAsync(testData)).ThrowsAsync(new Exception());

            // Act
            await sut.ReadActivitiesAsync();

            // Assert
            localRepoMock.Verify(rp => rp.SaveAllAsync(testData));
        }

        [Test]
        public async Task RefreshActivitiesAsync_Should_FetchRemoteActivities()
        {
            // Arrange
            var testData = TestData.CreateTestActivities();
            remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ReturnsAsync(testData);

            // Act
            var result = await sut.RefreshActivitiesAsync();

            // Assert
            Assert.That(result, Is.EqualTo(testData));
        }

        [Test]
        public async Task RefreshActivitiesAsync_Should_SaveLocallyDownloadedData()
        {
			// Arrange
			var testData = TestData.CreateTestActivities();
			remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ReturnsAsync(testData);

			// Act
			await sut.RefreshActivitiesAsync();

            // Assert
            localRepoMock.Verify(lr => lr.SaveAllAsync(testData));
        }

        [Test]
        public async Task RefreshActivitesAsync_Should_SaveDownloadedDataInMemoryBuffer()
        {
			// Arrange
			var testData = TestData.CreateTestActivities();
			remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ReturnsAsync(testData);

            // Act
            var resultRemote = await sut.RefreshActivitiesAsync();
            var resultLocal = await sut.ReadActivitiesAsync();

            // Assert
            Assert.That(resultRemote, Is.EqualTo(resultLocal));
            localRepoMock.Verify(lr => lr.ReadAllAsync<Activity>(), Times.Never());
        }

        [Test]
        public async Task RefreshActivitiesAsync_Should_RaiseNewActivitiesAvailable()
        {
			// Arrange
			var testData = TestData.CreateTestActivities();
			remoteSourceMock.Setup(rs => rs.GetAllAsync<Activity>()).ReturnsAsync(testData);
            IEnumerable<Activity> raisedActivities = null;

            // Act
            sut.NewActivitiesAvailable += (sender, e) => raisedActivities = e.Value;
            var resultRemote = await sut.RefreshActivitiesAsync();

            // Assert
            Assert.That(raisedActivities, Is.EqualTo(resultRemote));
        }
    }
}
