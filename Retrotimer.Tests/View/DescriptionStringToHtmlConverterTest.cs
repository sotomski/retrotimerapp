﻿using System;
using NUnit.Framework;
using Retrotimer.View;
using Xamarin.Forms;

namespace View
{
	[TestFixture]
    public class DescriptionStringToHtmlConverterTest
    {
        ActivityHtmlFormatter sut;

        const string DoctypeOpenMarkup = "<!DOCTYPE html>";
        const string HtmlOpenMarkup = "<html>";
        const string HeadOpenMarkup = "<head>";
        const string MetaMarkup = "<meta name=\"viewport\" content=\"width=device-width,height=device-height,user-scalable=no,initial-scale=1.0\"/>";
        const string StyleOpenMarkup = "<style type=\"text/css\">";
        const string StyleCloseMarkup = "</style>";
        const string HeadCloseMarkup = "</head>";
        const string BodyOpenMarkup = "<body>";
        const string BodyCloseMarkup = "</body>";
        const string HtmlCloseMarkup = "</html>";
        const string DoctypeCloseMarkup = "</!DOCTYPE>";

        /// <summary>
        /// Gets the markup content from html.
        /// </summary>
        /// <returns>The markup content from html.</returns>
        /// <param name="html">Html document.</param>
        /// <param name="openingMarkup">Opening HTML markup.</param>
        /// <param name="closingMarkup">Closing HTML markup.</param>
        static string GetMarkupContentFromHtml(string html, string openingMarkup, string closingMarkup)
        {
            var indexOpeningMarkup = html.IndexOf(openingMarkup, StringComparison.Ordinal);
            var indexClosingMarkup = html.IndexOf(closingMarkup, StringComparison.Ordinal);
            return html.Remove(indexClosingMarkup).Remove(0, indexOpeningMarkup + openingMarkup.Length);
        }

        [SetUp]
        public void SetUp()
        {
            sut = new ActivityHtmlFormatter();
        }

        [TearDown]
        public void TearDown()
        {
            sut = null;
        }

        [Test]
        public void FormatHtmlWithDescription_Should_ReturnNull_When_NullValuePassed()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(null, null);

            // Assert
            Assert.That(result, Is.Null);
        }

        [Test]
        public void FormatHtmlWithDescription_Should_ReturnHtmlWebViewSource_When_StringPassed()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);

            // Assert
            Assert.That(result, Is.TypeOf<HtmlWebViewSource>());
        }

        [Test]
        public void FormatHtmlWithDescription_Should_ReturnSourceWithHtml()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);

            // Assert
            Assert.That(result.Html, Is.Not.Null);
        }
            
        [Test]
        public void FormatHtmlWithDescription_Should_GenerateHtmlThatStartsWithDoctype()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);

            // Assert
            Assert.That(result.Html.StartsWith(DoctypeOpenMarkup, StringComparison.Ordinal));
        }

        [Test]
        public void FormatHtmlWithDescription_Should_GenerateHtmlThatEndsWithDoctype()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);

            // Assert
            Assert.That(result.Html.EndsWith(DoctypeCloseMarkup, StringComparison.Ordinal));
        }

        [Test]
        public void FormatHtmlWithDescription_Should_GenerateHtmlThatOpensHtmlInDoctype()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);
            var doctypeContent = result.Html.Replace(DoctypeOpenMarkup, "").Replace(DoctypeCloseMarkup, "");

            // Assert
            Assert.That(doctypeContent.StartsWith(HtmlOpenMarkup, StringComparison.Ordinal));
        }

        [Test]
        public void FormatHtmlWithDescription_Should_GenerateHtmlThatClosesHtmlInDoctype()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);
            var doctypeContent = result.Html.Replace(DoctypeOpenMarkup, "").Replace(DoctypeCloseMarkup, "");

            // Assert
            Assert.That(doctypeContent.EndsWith(HtmlCloseMarkup, StringComparison.Ordinal));
        }

        [Test]
        public void FormatHtmlWithDescription_Should_GenerateHtmlWithOpenedHead()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);
            var htmlContent = GetMarkupContentFromHtml(result.Html, HtmlOpenMarkup, HtmlCloseMarkup);

            // Assert
            Assert.That(htmlContent.Contains(HeadOpenMarkup));
        }

        [Test]
        public void FormatHtmlWithDescription_Should_GenerateHtmlWithClosedHead()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);
            var htmlContent = result.Html.Replace(DoctypeOpenMarkup + HtmlOpenMarkup, "").Replace(HtmlCloseMarkup + DoctypeCloseMarkup, "");

            // Assert
            Assert.That(htmlContent.Contains(HeadCloseMarkup));
        }
            
        [Test]
        public void FormatHtmlWithDescription_Should_GenerateHtmlThatOpendBodyAfterHeadInHtml()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);
            var headClosingIndex = result.Html.IndexOf(HeadCloseMarkup, StringComparison.Ordinal);
            var htmlContent = result.Html.Remove(0, headClosingIndex + HeadCloseMarkup.Length);

            // Assert
            Assert.That(htmlContent.StartsWith(BodyOpenMarkup, StringComparison.Ordinal));
        }
            
        [Test]
        public void FormatHtmlWithDescription_Should_GenerateHtmlThatCloseBodyInHtml()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);
            var htmlContent = result.Html.Replace(HtmlCloseMarkup + DoctypeCloseMarkup, "");

            // Assert
            Assert.That(htmlContent.EndsWith(BodyCloseMarkup, StringComparison.Ordinal));
        }

        [Test]
        public void FormatHtmlWithDescription_Should_PutDescriptionInHtmlBody()
        {
            // Arrange
            const string descr = "Some kind of test decsription";
            
            // Act
            var result = sut.FormatHtmlWithDescription(descr, null);
            var indexBodyOpen = result.Html.IndexOf(BodyOpenMarkup, StringComparison.Ordinal);
            var indexBodyClose = result.Html.IndexOf(BodyCloseMarkup, StringComparison.Ordinal);
            var bodyContent = result.Html.Remove(indexBodyClose).Remove(0, indexBodyOpen + BodyOpenMarkup.Length);

            // Assert
            Assert.That(bodyContent, Is.EqualTo(descr));
        }

        [Test]
        public void FormatHtmlWithDescription_Should_GenerateMetaForViewportInHead()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);
            var headContent = GetMarkupContentFromHtml(result.Html, HeadOpenMarkup, HeadCloseMarkup);

            // Assert
            Assert.That(headContent, Is.EqualTo(MetaMarkup));
        }

        [Test]
        public void FormatHtmlWithDescription_Should_NotGenerateCssMarkupInHtml_When_NullPassedAsCssDescriptor()
        {
            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, null);
            var hasCssStyleMarkup = result.Html.Contains(StyleOpenMarkup) || result.Html.Contains(StyleCloseMarkup);

            // Assert
            Assert.That(hasCssStyleMarkup, Is.False);
        }

        [Test]
        public void FormatHtmlWithDescription_Should_StartHtmlWithCssOpeningMarkup_When_EmptyDescriptorPassed()
        {
            // Arrange
            var cssStyle = new CssStyleDescriptor();

            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, cssStyle);
            var htmlContent = GetMarkupContentFromHtml(result.Html, HtmlOpenMarkup, HtmlCloseMarkup);

            // Assert
            Assert.That(htmlContent.StartsWith(StyleOpenMarkup, StringComparison.Ordinal), Is.True);
        }

        [Test]
        public void FormatHtmlWithDescription_Should_GenerateCssClosingMarkupBeforeHeadOpen_When_EmptyDescriptorPassed()
        {
            // Arrange
            var cssStyle = new CssStyleDescriptor();

            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, cssStyle);
            var htmlContent = GetMarkupContentFromHtml(result.Html, HtmlOpenMarkup, HtmlCloseMarkup);
            var indexStyleClose = htmlContent.IndexOf(StyleCloseMarkup, StringComparison.Ordinal);
            var firstPartOfHtmlContent = htmlContent.Remove(0, indexStyleClose + StyleCloseMarkup.Length);

            // Assert
            Assert.That(firstPartOfHtmlContent.StartsWith(HeadOpenMarkup, StringComparison.Ordinal), Is.True);
        }

        [Test]
        public void FormatHtmlWithDescription_Should_GenerateDefaultCssStyle_When_DescriptorIsNotChangedAfterInstantiation()
        {
            // Arrange
            const string expectedCss = "body{width:100%;margin:0px;white-space:wrap;color:#000000;background-color:#FFFFFF;font-family:\"Helvetica Neue\";font-size:12px;}";
            var cssStyle = new CssStyleDescriptor();

            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, cssStyle);
            var styleContent = GetMarkupContentFromHtml(result.Html, StyleOpenMarkup, StyleCloseMarkup);

            // Assert
            Assert.That(styleContent, Is.EqualTo(expectedCss));
        }

        [Test]
        public void FormatHtmlWithDescription_Should_GenerateAppropriateCssStyle_When_ConfiguredDescriptorPassed()
        {
            // Arrange
            const string expectedCss = "body{width:100%;margin:0px;white-space:wrap;color:#AA44B0;background-color:#00B989;font-family:\"Arial Black\";font-size:45px;}";
            var cssStyle = new CssStyleDescriptor()
            {
                TextColor = Color.FromHex("#AA44B0"),
                BackgroundColor = Color.FromHex("#00B989"),
                FontSize = 45,
                FontFamily = "Arial Black"
            };

            // Act
            var result = sut.FormatHtmlWithDescription(string.Empty, cssStyle);
            var styleContent = GetMarkupContentFromHtml(result.Html, StyleOpenMarkup, StyleCloseMarkup);

            // Assert
            Assert.That(styleContent, Is.EqualTo(expectedCss));
        }
    }
}

