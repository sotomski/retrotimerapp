﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Retrotimer;
using Retrotimer.View;

namespace View
{
	public class FocusNavigationServiceTest
	{
		[Test]
		public void Ctor_Should_ThrowArgumentNullException_When_ItemsListIsNull()
		{
			// Act
			TestDelegate test = () => new FocusNavigationService(null);

			// Assert
			Assert.That(test, Throws.TypeOf<ArgumentNullException>());
		}

		[Test]
		public void Ctor_Should_NotThrow_When_ItemsListIsEmpty()
		{
			// Arrange
			var items = new List<IFocusNavigatable>();

			// Act
			TestDelegate test = () => new FocusNavigationService(items);

			// Assert
			Assert.That(test, Throws.Nothing);
		}

		[Test]
		public void FocusNavigationIndex_Should_ThrowArgumentException_When_TwoElementsHaveIdenticallIndexes()
		{
			// Arrange
			var i1 = Mock.Of<IFocusNavigatable>(i => i.FocusNavigationIndex == 5);
			var i2 = Mock.Of<IFocusNavigatable>(i => i.FocusNavigationIndex == 5);
			var items = new List<IFocusNavigatable> { i1, i2 };

			// Act
			TestDelegate test = () => new FocusNavigationService(items);

			// Assert
			Assert.That(test, Throws.TypeOf<ArgumentException>().With.Message.EqualTo("FocusNavigationIndex has to be unique."));
		}

		[Test]
		public void FocusNextRequested_Should_FocusOnTheSameChild_When_ListHasOneItem()
		{
			// Arrange
			var item = new Mock<IFocusNavigatable>();
			var items = new List<IFocusNavigatable> { item.Object };
			var sut = new FocusNavigationService(items);

			// Act
			item.Raise(im => im.FocusNextRequested += null, new EventArgs<IFocusNavigatable>(item.Object));

			// Assert
			item.Verify(im => im.FocusNavigation(), Times.Once());
		}

		[Test]
		public void FocusPrevRequested_Should_FocusOnTheSameChild_When_ListHasOneItem()
		{
			// Arrange
			var item = new Mock<IFocusNavigatable>();
			var items = new List<IFocusNavigatable> { item.Object };
			var sut = new FocusNavigationService(items);

			// Act
			item.Raise(im => im.FocusPreviousRequested += null, new EventArgs<IFocusNavigatable>(item.Object));

			// Assert
			item.Verify(im => im.FocusNavigation(), Times.Once());
		}

		[Test]
		public void FocusNextRequested_Should_FocusOnSecondElement_When_RaisedByFirstElement()
		{
			// Arrange
			var i1 = new Mock<IFocusNavigatable>();
			i1.Setup(im => im.FocusNavigationIndex).Returns(4);
			var i2 = new Mock<IFocusNavigatable>();
			i2.Setup(im => im.FocusNavigationIndex).Returns(5);
			var items = new List<IFocusNavigatable> { i1.Object, i2.Object };
			var sut = new FocusNavigationService(items);

			// Act
			i1.Raise(im => im.FocusNextRequested += null, new EventArgs<IFocusNavigatable>(i1.Object));

			// Assert
			i2.Verify(im => im.FocusNavigation(), Times.Once());
		}

		[Test]
		public void FocusPreviousRequested_Should_FocusOnFirstElement_When_RaisedBySecondElement()
		{
			// Arrange
			var i1 = new Mock<IFocusNavigatable>();
			i1.Setup(im => im.FocusNavigationIndex).Returns(4);
			var i2 = new Mock<IFocusNavigatable>();
			i2.Setup(im => im.FocusNavigationIndex).Returns(5);
			var items = new List<IFocusNavigatable> { i1.Object, i2.Object };
			var sut = new FocusNavigationService(items);

			// Act
			i2.Raise(im => im.FocusPreviousRequested += null, new EventArgs<IFocusNavigatable>(i2.Object));

			// Assert
			i1.Verify(im => im.FocusNavigation(), Times.Once());
		}

		[Test]
		public void FocusNextRequested_Should_FocusOnFirstElement_When_RaisedByLastElement()
		{
			// Arrange
			var i1 = new Mock<IFocusNavigatable>();
			i1.Setup(im => im.FocusNavigationIndex).Returns(4);
			var i2 = new Mock<IFocusNavigatable>();
			i2.Setup(im => im.FocusNavigationIndex).Returns(5);
			var i3 = new Mock<IFocusNavigatable>();
			i3.Setup(im => im.FocusNavigationIndex).Returns(6);
			var items = new List<IFocusNavigatable> { i1.Object, i2.Object, i3.Object };
			var sut = new FocusNavigationService(items);

			// Act
			i3.Raise(im => im.FocusNextRequested += null, new EventArgs<IFocusNavigatable>(i3.Object));

			// Assert
			i1.Verify(im => im.FocusNavigation(), Times.Once());
		}

		[Test]
		public void FocusPreviousRequested_Should_FocusOnLastElement_When_RaisedByFirstElement()
		{
			// Arrange
			var i1 = new Mock<IFocusNavigatable>();
			i1.Setup(im => im.FocusNavigationIndex).Returns(4);
			var i2 = new Mock<IFocusNavigatable>();
			i2.Setup(im => im.FocusNavigationIndex).Returns(5);
			var i3 = new Mock<IFocusNavigatable>();
			i3.Setup(im => im.FocusNavigationIndex).Returns(6);
			var items = new List<IFocusNavigatable> { i1.Object, i2.Object, i3.Object };
			var sut = new FocusNavigationService(items);

			// Act
			i1.Raise(im => im.FocusPreviousRequested += null, new EventArgs<IFocusNavigatable>(i1.Object));

			// Assert
			i1.Verify(im => im.FocusNavigation(), Times.Never());
			i2.Verify(im => im.FocusNavigation(), Times.Never());
			i3.Verify(im => im.FocusNavigation(), Times.Once());
		}
	}
}
