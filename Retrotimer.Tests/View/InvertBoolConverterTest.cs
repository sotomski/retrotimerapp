﻿using System;
using NUnit.Framework;
using Retrotimer.View;

namespace View
{
	[TestFixture]
	public class CountdownStatusToBooleanConverterTest
	{
		InvertBoolConverter sut;

		[SetUp]
		public void Setup()
		{
			sut = new InvertBoolConverter();
		}

		[Test]
		public void Convert_Should_RetrunTrue_When_FalsePassed()
		{
			var result = sut.Convert(false, null, null, null);

			Assert.That(result, Is.True);
		}

		[Test]
		public void Convert_Should_RetrunTrue_When_TruePassed()
		{
			var result = sut.Convert(true, null, null, null);

			Assert.That(result, Is.False);
		}

		[Test]
		public void Convert_Should_ThrowArgumentNullException_When_ValueNull()
		{
			TestDelegate test = () => sut.Convert(null, null, null, null);

			Assert.That(test, Throws.TypeOf<ArgumentOutOfRangeException>());
		}

		[Test]
		public void Convert_Should_ThrowArgumentNullException_When_ValueNotBool()
		{
			TestDelegate test = () => sut.Convert("true", null, null, null);

			Assert.That(test, Throws.TypeOf<ArgumentOutOfRangeException>());
		}

		[Test]
		public void ConvertBack_Should_ThrowUnsupportedOperationException()
		{
			TestDelegate test = () => sut.ConvertBack(new object(), null, null, null);

			Assert.That(test, Throws.InvalidOperationException);
		}
	}
}
