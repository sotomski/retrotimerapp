﻿using System;
using NUnit.Framework;
using Retrotimer.Model;
using Retrotimer.View;

namespace View
{
	[TestFixture]
	public class RetroPhaseToStringConverterTest
	{
		[Test]
		public void Convert_Should_ThrowArgumentException_When_ValueIsNotRetroPhase()
		{
			// Arrange
			var sut = new RetroPhaseToStringConverter();
			var input = new object();

			// Act
			TestDelegate test = () => sut.Convert(input, null, null, null);

			// Assert
			Assert.That(test, Throws.ArgumentException
			            .With.Property(nameof(ArgumentException.ParamName))
						.EqualTo("value"));
		}

		[Test]
		[TestCase(RetroPhase.SetTheStage, "Set the stage")]
		[TestCase(RetroPhase.GatherData, "Gather data")]
		[TestCase(RetroPhase.GenerateInsights, "Generate insights")]
		[TestCase(RetroPhase.DecideWhatToDo, "Decide what to do")]
		[TestCase(RetroPhase.CloseTheRetrospective, "Close the retrospective")]
		public void Convert_Should_ReturnPrettyName_When_ValueIsValidRetroPhase(RetroPhase phase, string expectedText)
		{
			// Arrange
			var sut = new RetroPhaseToStringConverter();

			// Act
			var result = sut.Convert(phase, null, null, null);

			// Assert
			Assert.That(result, Is.EqualTo(expectedText));
		}
	}
}
