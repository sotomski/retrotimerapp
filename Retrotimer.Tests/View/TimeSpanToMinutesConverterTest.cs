﻿using System;
using NUnit.Framework;
using Retrotimer.View;

namespace View
{
	public class TimeSpanToMinutesConverterTest
	{
		[Test]
		public void ConvertBack_Should_ThrowInvalidOperationException_When_Invoked()
		{
			// Arrange
			var sut = new TimeSpanToMinutesConverter();

			// Act
			TestDelegate test = () => sut.ConvertBack(null, null, null, null);

			// Assert
			Assert.That(test, Throws.TypeOf<InvalidOperationException>());
		}

		[Test]
		public void Convert_Should_ThrowArgumentException_When_ValueIsNull()
		{
			// Arrange
			var sut = new TimeSpanToMinutesConverter();

			// Act
			TestDelegate test = () => sut.Convert(null, null, null, null);

			// Assert
			Assert.That(test, Throws.TypeOf<ArgumentException>()
						.With
						.Property(nameof(ArgumentException.ParamName))
						.EqualTo("value"));
		}

		[Test]
		public void Convert_Should_ThrowArgumentException_When_ValueIsNotTimeSpan()
		{
			// Arrange
			var sut = new TimeSpanToMinutesConverter();

			// Act
			TestDelegate test = () => sut.Convert(new object(), null, null, null);

			// Assert
			Assert.That(test, Throws.TypeOf<ArgumentException>()
			            .With
			            .Property(nameof(ArgumentException.ParamName))
			            .EqualTo("value"));
		}

		[Test]
		public void Convert_Should_ReturnExactMinutes_When_ValueInFullMinutes()
		{
			// Arrange
			var expected = 10;
			var input = TimeSpan.FromMinutes(expected);
			var sut = new TimeSpanToMinutesConverter();

			// Act
			var actual = sut.Convert(input, null, null, null);

			// Assert
			Assert.That(actual, Is.EqualTo(expected));
		}

		[Test]
		public void Convert_Should_ReturnFloorMinutes_When_ValueFractionMinutesIsLowerThanHalf()
		{
			// Arrange
			var input = TimeSpan.FromMinutes(10.3);
			var sut = new TimeSpanToMinutesConverter();

			// Act
			var actual = sut.Convert(input, null, null, null);

			// Assert
			Assert.That(actual, Is.EqualTo(10));
		}

		[Test]
		public void Convert_Should_ReturnFloorMinutes_When_ValueFractionMinutesIsEqualHalf()
		{
			// Arrange
			var input = TimeSpan.FromMinutes(180.5);
			var sut = new TimeSpanToMinutesConverter();

			// Act
			var actual = sut.Convert(input, null, null, null);

			// Assert
			Assert.That(actual, Is.EqualTo(180));
		}

		[Test]
		public void Convert_Should_ReturnFloorMinutes_When_ValueFractionMinutesIsGreaterThanHalf()
		{
			// Arrange
			var input = TimeSpan.FromMinutes(0.7);
			var sut = new TimeSpanToMinutesConverter();

			// Act
			var actual = sut.Convert(input, null, null, null);

			// Assert
			Assert.That(actual, Is.EqualTo(0));
		}
	}
}

