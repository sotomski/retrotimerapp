﻿using NUnit.Framework;
using Retrotimer.View;
using Retrotimer.ViewModel;
using Xamarin.Forms;
using Vsm = Retrotimer.View.StyleManager;

namespace View
{
	[TestFixture]
    public class VisualStyleToControlStyleConverterTest
    {
        [Test]
        public void LabelConvert_Should_ThrowArgumentException_When_TargetTypeOtherThanStyle()
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();
            
            // Act
            TestDelegate test = () => sut.Convert(null, typeof(Element), Vsm.LabelStyle.Micro, null);
            
            // Assert
            Assert.That(test, Throws.ArgumentException.With.Message.EqualTo("Target type has to be a Style"));
        }

        [Test]
        public void LabelConvert_Should_ThrowArgumentException_When_ValueNotVisualStyle()
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();

            // Act
            TestDelegate test = () => sut.Convert(5, typeof(Style), Vsm.LabelStyle.Micro, null);

            // Assert
            Assert.That(test, Throws.ArgumentException.With.Message.EqualTo("Value has to be a visual style"));
        }

        [Test]
        public void LabelConvert_Should_ThrowArgumentException_When_ParameterIsNull()
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();

            // Act
            TestDelegate test = () => sut.Convert(VisualStyle.Blue, typeof(Style), null, null);

            // Assert
            Assert.That(test, Throws.ArgumentException.With.Message.EqualTo("Parameter has to be a LabelStyle value"));
        }

        [Test]
        [TestCase(VisualStyle.LightBlue)]
        [TestCase(VisualStyle.LightOrange)]
        [TestCase(VisualStyle.Blue)]
        [TestCase(VisualStyle.DarkBlue)]
        [TestCase(VisualStyle.DarkOrange)]
        public void LabelConvert_Should_ReturnLabelStyle(VisualStyle style)
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();

            // Act
            var resultRaw = sut.Convert(style, typeof(Style), Vsm.LabelStyle.Micro, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.TargetType, Is.EqualTo(typeof(Label)));
        }

        [Test]
        [TestCase(VisualStyle.LightBlue)]
        [TestCase(VisualStyle.LightOrange)]
        [TestCase(VisualStyle.Blue)]
        [TestCase(VisualStyle.DarkBlue)]
        [TestCase(VisualStyle.DarkOrange)]
        public void LabelConvert_Should_ReturnMicroFontSize_When_LabelStyleIsMicro(VisualStyle style)
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();

            // Act
            var resultRaw = sut.Convert(style, typeof(Style), Vsm.LabelStyle.Micro, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(Label.FontSizeProperty)
                .And.Property("Value").EqualTo(11));
        }

        [Test]
        [TestCase(VisualStyle.LightBlue)]
        [TestCase(VisualStyle.LightOrange)]
        [TestCase(VisualStyle.Blue)]
        [TestCase(VisualStyle.DarkBlue)]
        [TestCase(VisualStyle.DarkOrange)]
        public void LabelConvert_Should_ReturnNormalFontSize_When_LabelStyleIsNormal(VisualStyle style)
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();

            // Act
            var resultRaw = sut.Convert(style, typeof(Style), Vsm.LabelStyle.Normal, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(Label.FontSizeProperty)
                .And.Property("Value").EqualTo(13));
        }

        [Test]
        [TestCase(VisualStyle.LightBlue)]
        [TestCase(VisualStyle.LightOrange)]
        [TestCase(VisualStyle.Blue)]
        [TestCase(VisualStyle.DarkBlue)]
        [TestCase(VisualStyle.DarkOrange)]
        public void LabelConvert_Should_ReturnNormalFontSize_When_LabelStyleIsNormalBold(VisualStyle style)
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();

            // Act
            var resultRaw = sut.Convert(style, typeof(Style), Vsm.LabelStyle.NormalBold, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(Label.FontSizeProperty)
                .And.Property("Value").EqualTo(13));
        }

        [Test]
        [TestCase(Vsm.LabelStyle.NormalBold)]
        [TestCase(Vsm.LabelStyle.Large)]
		[TestCase(Vsm.LabelStyle.Huge)]
        public void LabelConvert_Should_ReturnBoldAttribute_When_LabelStyleIsBold(Vsm.LabelStyle style)
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();

            // Act
            var resultRaw = sut.Convert(VisualStyle.LightBlue, typeof(Style), style, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(Label.FontAttributesProperty)
                .And.Property("Value").EqualTo(FontAttributes.Bold));
        }

        [Test]
        [TestCase(Vsm.LabelStyle.Micro)]
        [TestCase(Vsm.LabelStyle.Normal)]
        public void LabelConvert_Should_NotGenerateFontAttributes_When_StyleOtherThanNormalBold(Vsm.LabelStyle style)
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();

            // Act
            var resultRaw = sut.Convert(VisualStyle.Blue, typeof(Style), style, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.Setters, Has.None.With.Property("Property").EqualTo(Label.FontAttributesProperty));
        }

        [Test]
        [TestCase(VisualStyle.Blue)]
        [TestCase(VisualStyle.DarkBlue)]
        [TestCase(VisualStyle.DarkOrange)]
        public void LabelConvert_Should_ReturnLightStyle_When_VisualStyleIsDark(VisualStyle style)
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();
            var textColor = Color.FromHex("#FFFFFF");

            // Act
            var resultRaw = sut.Convert(style, typeof(Style), Vsm.LabelStyle.Micro, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(Label.TextColorProperty)
                .And.Property("Value").EqualTo(textColor));
        }

        [Test]
        [TestCase(VisualStyle.LightBlue)]
        [TestCase(VisualStyle.LightOrange)]
        [TestCase(VisualStyle.Blue)]
        [TestCase(VisualStyle.DarkBlue)]
        [TestCase(VisualStyle.DarkOrange)]
        public void LabelConvert_Should_ReturnLargeFontSize_When_LabelStyleIsLarge(VisualStyle style)
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();

            // Act
            var resultRaw = sut.Convert(style, typeof(Style), Vsm.LabelStyle.Large, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(Label.FontSizeProperty)
                .And.Property("Value").EqualTo(17));
        }

        [Test]
        [TestCase(VisualStyle.LightBlue)]
        [TestCase(VisualStyle.LightOrange)]
        public void LabelConvert_Should_ReturnDarkStyle_When_VisualStyleIsLight(VisualStyle style)
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();
            var textColor = Color.FromHex("#000000");

            // Act
            var resultRaw = sut.Convert(style, typeof(Style), Vsm.LabelStyle.Micro, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(Label.TextColorProperty)
                .And.Property("Value").EqualTo(textColor));
        }

        [Test]
        [TestCase("Micro", 11)]
        [TestCase("Normal", 13)]
        [TestCase("NormalBold", 13)]
        [TestCase("Large", 17)]
		[TestCase("Huge", 21)]
        public void LabelConvert_Should_GeneratePropertLabelStyle_When_StringParameterPassedInsteadOfEnum(string labelStyleString, int fontSize)
        {
            // Arrange
            var sut = new VisualStyleToLabelStyleConverter();

            // Act
            var resultRaw = sut.Convert(VisualStyle.LightBlue, typeof(Style), labelStyleString, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(Label.FontSizeProperty)
                .And.Property("Value").EqualTo(fontSize));
        }

		[Test]
		public void CountdownPickerConvert_Should_ThrowArgumentException_When_TargetTypeOtherThanStyle()
		{
			// Arrange
			var sut = new VisualStyleToCountdownPickerConverter();

			// Act
			TestDelegate test = () => sut.Convert(null, typeof(Element), null, null);

			// Assert
			Assert.That(test, Throws.ArgumentException.With.Message.EqualTo("Target type has to be a Style"));
		}

		[Test]
		public void CountdownPickerConvert_Should_ThrowArgumentException_When_ValueNotVisualStyle()
		{
			// Arrange
			var sut = new VisualStyleToCountdownPickerConverter();

			// Act
			TestDelegate test = () => sut.Convert(5, typeof(Style), null, null);

			// Assert
			Assert.That(test, Throws.ArgumentException.With.Message.EqualTo("Value has to be a visual style"));
		}

		[Test]
		[TestCase(VisualStyle.Blue)]
		[TestCase(VisualStyle.DarkBlue)]
		[TestCase(VisualStyle.DarkOrange)]
		public void CountdownPickerConvert_Should_ReturnLightStyle_When_VisualStyleIsDark(VisualStyle style)
		{
			// Arrange
			var sut = new VisualStyleToCountdownPickerConverter();
			var textColor = Color.FromHex("#FFFFFF");

			// Act
			var resultRaw = sut.Convert(style, typeof(Style), null, null);
			var resultStyle = resultRaw as Style;

			// Assert
			Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(CountdownPicker.TextColorProperty)
				.And.Property("Value").EqualTo(textColor));
		}

		[Test]
		[TestCase(VisualStyle.LightBlue)]
		[TestCase(VisualStyle.LightOrange)]
		public void CountdownPickerConvert_Should_ReturnDarkStyle_When_VisualStyleIsLight(VisualStyle style)
		{
			// Arrange
			var sut = new VisualStyleToCountdownPickerConverter();
			var textColor = Color.FromHex("#000000");

			// Act
			var resultRaw = sut.Convert(style, typeof(Style), null, null);
			var resultStyle = resultRaw as Style;

			// Assert
			Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(CountdownPicker.TextColorProperty)
				.And.Property("Value").EqualTo(textColor));
		}

        [Test]
        public void GridConvert_Should_ThrowArgumentException_When_TargetTypeOtherThanStyle()
        {
            // Arrange
            var sut = new VisualStyleToViewStyleConverter();

            // Act
            TestDelegate test = () => sut.Convert(null, typeof(Element), null, null);

            // Assert
            Assert.That(test, Throws.ArgumentException.With.Message.EqualTo("Target type has to be a Style"));
        }

        [Test]
        public void GridConvert_Should_ThrowArgumentException_When_ValueNotVisualStyle()
        {
            // Arrange
            var sut = new VisualStyleToViewStyleConverter();

            // Act
            TestDelegate test = () => sut.Convert(5, typeof(Style), null, null);

            // Assert
            Assert.That(test, Throws.ArgumentException.With.Message.EqualTo("Value has to be a visual style"));
        }

        [Test]
        public void GridConvert_Should_ReturnViewStyle()
        {
            // Arrange
            var sut = new VisualStyleToViewStyleConverter();

            // Act
            var resultRaw = sut.Convert(VisualStyle.LightBlue, typeof(Style), null, null);
            var resultStyle = resultRaw as Style;

            // Assert
            Assert.That(resultStyle.TargetType, Is.EqualTo(typeof(Xamarin.Forms.View)));
        }

        [Test]
        [TestCase(VisualStyle.LightBlue, "#B9E7E7")]
        [TestCase(VisualStyle.LightOrange, "#D3B256")]
        [TestCase(VisualStyle.Blue, "#017F7C")]
        [TestCase(VisualStyle.DarkBlue, "#023838")]
        [TestCase(VisualStyle.DarkOrange, "#AF7500")]
        public void GridConvert_Should_ReturnStyleWithAppropriateColor(VisualStyle style, string colorHex)
        {
            // Arrange
            var sut = new VisualStyleToViewStyleConverter();
            var expectedColor = Color.FromHex(colorHex);

                // Act
            var resultRaw = sut.Convert(style, typeof(Style), null, null);
            var resultStyle = resultRaw as Style;
            
            // Assert
            Assert.That(resultStyle.Setters, Has.Exactly(1).With.Property("Property").EqualTo(VisualElement.BackgroundColorProperty)
                .And.Property("Value").EqualTo(expectedColor));
        }

        [Test]
        public void StyleDescriptorConvert_Should_ThrowArgumentException_When_TargetTypeNotActivityWebView()
        {
            // Arrange
            var sut = new VisualStyleToStyleDescriptorConverter();

            // Act
            TestDelegate test = () => sut.Convert(null, typeof(string), null, null);

            // Assert
            Assert.That(test, Throws.ArgumentException.With.Message.EqualTo("Target type has to be a Css style descriptor"));
        }

        [Test]
        public void StyleDescriptorConvert_Should_ThrowArgumentException_When_ValueValueNotVisualStyle()
        {
            // Arrange
            var sut = new VisualStyleToStyleDescriptorConverter();

            // Act
            TestDelegate test = () => sut.Convert(-1, typeof(CssStyleDescriptor), null, null);

            // Assert
            Assert.That(test, Throws.ArgumentException.With.Message.EqualTo("Value has to be a visual style"));
        }

        [Test]
        [TestCase(VisualStyle.LightBlue)]
        [TestCase(VisualStyle.LightOrange)]
        [TestCase(VisualStyle.Blue)]
        [TestCase(VisualStyle.DarkBlue)]
        [TestCase(VisualStyle.DarkOrange)]
        public void StyleDescriptorConvert_Should_ReturnStyleDescription_When_VisualStylePassedIn(VisualStyle style)
        {
            // Arrange
            var sut = new VisualStyleToStyleDescriptorConverter();

            // Act
            var result = sut.Convert(style, typeof(CssStyleDescriptor), null, null) as CssStyleDescriptor;

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.FontFamily, Is.EqualTo("-apple-system"));
            Assert.That(result.FontSize, Is.EqualTo(13));
        }

        [Test]
        [TestCase(VisualStyle.Blue)]
        [TestCase(VisualStyle.DarkBlue)]
        [TestCase(VisualStyle.DarkOrange)]
        public void StyleDescriptorConvert_Should_ResolveFontColor_When_DarkVisualStylePassedIn(VisualStyle style)
        {
            // Arrange
            var sut = new VisualStyleToStyleDescriptorConverter();

            // Act
            var result = sut.Convert(style, typeof(CssStyleDescriptor), null, null) as CssStyleDescriptor;

            // Assert
            Assert.That(result.TextColor, Is.EqualTo(Color.White));
        }

        [Test]
        [TestCase(VisualStyle.LightBlue)]
        [TestCase(VisualStyle.LightOrange)]
        public void StyleDescriptorConvert_Should_ResolveFontColor_When_LightVisualStylePassedIn(VisualStyle style)
        {
            // Arrange
            var sut = new VisualStyleToStyleDescriptorConverter();

            // Act
            var result = sut.Convert(style, typeof(CssStyleDescriptor), null, null) as CssStyleDescriptor;

            // Assert
            Assert.That(result.TextColor, Is.EqualTo(Color.Black));
        }

        [Test]
        [TestCase(VisualStyle.LightBlue, "#B9E7E7")]
        [TestCase(VisualStyle.LightOrange, "#D3B256")]
        [TestCase(VisualStyle.Blue, "#017F7C")]
        [TestCase(VisualStyle.DarkBlue, "#023838")]
        [TestCase(VisualStyle.DarkOrange, "#AF7500")]
        public void StyleDescriptorConvert_Should_ResolveBackgroundColor_When_VisualStylePassedIn(VisualStyle style, string colorHex)
        {
            // Arrange
            var sut = new VisualStyleToStyleDescriptorConverter();

            // Act
            var result = sut.Convert(style, typeof(CssStyleDescriptor), null, null) as CssStyleDescriptor;

            // Assert
            Assert.That(result.BackgroundColor, Is.EqualTo(Color.FromHex(colorHex)));
        }
    }
}
