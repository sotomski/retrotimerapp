﻿using System;
using Moq;
using NUnit.Framework;
using Retrotimer;
using Retrotimer.Common;
using Retrotimer.ViewModel;

namespace ViewModel
{
	[TestFixture]
    public class AboutViewModelTest
    {
        [Test]
        public void CloseCommand_Should_PopSelfAsync_When_Executed()
        {
            // Arrange
            var navSvcMock = new Mock<INavigationService>();
            navSvcMock.Setup(m => m.PopModalAsync()).Verifiable();
            var sut = new AboutViewModel(navSvcMock.Object, null, null);

            // Act
            sut.CloseCommand.Execute(null);

            // Assert
            navSvcMock.Verify();
        }

        [Test]
        public void OpenTwitterCommand_Should_OpenUriFromParameter_When_Executed()
        {
            // Arrange
            var deviceMock = new Mock<IDevice>();
            const string testUriString = "http://www.test.com";
            var testUri = new Uri(testUriString);
            deviceMock.Setup(m => m.LaunchUriAsync(testUri)).Verifiable();
            var sut = new AboutViewModel(null, null, deviceMock.Object);

            // Act
            sut.OpenUriCommand.Execute(testUriString);

            // Assert
            deviceMock.Verify();
        }
    }
}
