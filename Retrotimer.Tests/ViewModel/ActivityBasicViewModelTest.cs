﻿using NUnit.Framework;
using Retrotimer.Model;
using Retrotimer.ViewModel;

namespace ViewModel
{
	[TestFixture]
	public class ActivityBasicViewModelTest
	{
		[Test]
		[TestCase("This tests &amp; character", "This tests & character")]
		[TestCase("This tests &gt; character", "This tests > character")]
		[TestCase("This tests &lt; character", "This tests < character")]
		public void Name_Should_DecodeHtmlEncodedString(string htmlEncoded, string decodedString)
		{
			// Arrange
			var model = new Activity(0, RetroPhase.CloseTheRetrospective, htmlEncoded);
			var sut = new ActivityBasicViewModel(model);

			// Act
			var result = sut.Name;

			// Assert
			Assert.That(result, Is.EqualTo(decodedString));
		}
	}
}

