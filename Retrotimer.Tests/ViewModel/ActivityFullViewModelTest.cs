﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;
using Moq;
using NUnit.Framework;
using Retrotimer.Model;
using Retrotimer.Services;
using Retrotimer.View;
using Retrotimer.ViewModel;

namespace ViewModel
{
	[TestFixture]
	public class ActivityFullViewModelTest
	{
		[TearDown]
		public void TearDown()
		{
			Messenger.Reset();
		}

		[Test]
		public void Ctor_Should_ProperlyInitializeOriginalTimebox_When_Invoked()
		{
			// Arrange
			var activity = new Activity(0, RetroPhase.SetTheStage, "");
			var timebox = TimeSpan.FromTicks(1);
			var awt = new ActivityWithTimebox(activity, timebox);

			// Act
			var sut = new ActivityFullViewModel(awt);

			// Assert
			Assert.That(sut.OriginalTimebox, Is.EqualTo(timebox));
		}

		[Test]
		public void RemainingTimebox_Should_EqualToOriginalTimebox_When_InstanceCreated()
		{
			// Arrange
			var activity = new Activity(0, RetroPhase.SetTheStage, "");
			var awt = new ActivityWithTimebox(activity, TimeSpan.Zero);

			// Act
			var sut = new ActivityFullViewModel(awt);

			// Assert
			Assert.That(sut.RemainingTimebox, Is.EqualTo(sut.OriginalTimebox));
		}

		[Test]
		public void RemainingTimebox_Should_SafeAssignedValue_When_SetterInvoked()
		{
			// Arrange
			var activity = new Activity(0, RetroPhase.SetTheStage, "");
			var awt = new ActivityWithTimebox(activity, TimeSpan.MaxValue);
			var sut = new ActivityFullViewModel(awt);

			// Act
			var testValue = TimeSpan.FromTicks(456);
			sut.RemainingTimebox = testValue;

			// Assert
			Assert.That(sut.RemainingTimebox, Is.EqualTo(testValue));
		}

		[Test]
		public void OriginalTimebox_Should_UpdateRemainingTimebox_When_NewValueAssigned()
		{
			// Arrange
			var activity = new Activity(0, RetroPhase.SetTheStage, "");
			var awt = new ActivityWithTimebox(activity, TimeSpan.FromTicks(456));
			var expected = TimeSpan.FromMinutes(11);
			var sut = new ActivityFullViewModel(awt);

			// Act
			var before = sut.RemainingTimebox;
			sut.OriginalTimebox = expected;

			// Arrange
			Assert.That(before, Is.Not.EqualTo(expected));
			Assert.That(sut.RemainingTimebox, Is.EqualTo(sut.OriginalTimebox));
		}

		[Test]
		public void RemainingTimebox_Should_ThrowOutOfRangeException_When_TimespanShorterThanZeroAssigned()
		{
			// Arrange
			var activity = new Activity(0, RetroPhase.SetTheStage, "");
			var awt = new ActivityWithTimebox(activity, TimeSpan.FromTicks(456));
			var sut = new ActivityFullViewModel(awt);

			// Act
			TestDelegate test = () => sut.RemainingTimebox = TimeSpan.Zero - TimeSpan.FromDays(5);

			// Assert
			Assert.That(test, Throws.TypeOf<ArgumentOutOfRangeException>());
		}

		[Test]
		public void RemainingTimebox_Should_ThrowOutOfRangeException_When_TimespanLongerThanOriginalTimeboxAssigned()
		{
			// Arrange
			var activity = new Activity(0, RetroPhase.SetTheStage, "");
			var awt = new ActivityWithTimebox(activity, TimeSpan.Zero);
			var sut = new ActivityFullViewModel(awt);

			// Act
			TestDelegate test = () => sut.RemainingTimebox = sut.OriginalTimebox + TimeSpan.FromDays(5);

			// Assert
			Assert.That(test, Throws.TypeOf<ArgumentOutOfRangeException>());
		}

		static ActivityFullViewModel CreateEmptyTestViewModel()
		{
			var model = new Activity(0, RetroPhase.CloseTheRetrospective, "");
			var item = new ActivityWithTimebox(model, TimeSpan.MinValue);
			var sut = new ActivityFullViewModel(item);
			return sut;
		}

		[Test]
		public void ProcessTitleTapCommand_Should_SendTappedMessage_When_Invoked()
		{
			// Arrange
			var sut = CreateEmptyTestViewModel();
			var messengerMock = new Mock<IMessenger>();
			messengerMock.Setup(m => m.Send(It.IsAny<NotificationMessage>()));
			Messenger.OverrideDefault(messengerMock.Object);

			// Act
			sut.ProcessTitleTapCommand.Execute(null);

			// Assert
			messengerMock.Verify(m => m.Send(It.Is<NotificationMessage>(
				msg => msg.Sender == sut && msg.Notification == MessageCommunicationProtocol.TitleTappedMessage)));
		}

		[Test]
		public void IsEditMode_Should_RaiseIsPlayModeChanged_When_ValueChanged()
		{
			// Arrange
			var sut = CreateEmptyTestViewModel();
			var namedOfChangedProperties = new List<string>();
			sut.PropertyChanged += (sender, e) => namedOfChangedProperties.Add(e.PropertyName);

			// Act
			sut.IsEditMode = !sut.IsEditMode;

			// Assert
			Assert.That(namedOfChangedProperties, Has.Exactly(1).EqualTo(nameof(sut.IsEditMode)));
		}

		[Test]
		public void NavigateFocusToPreviousElementCommand_Should_RaiseFocusPreviousRequestedEvent_When_Executed()
		{
			// Arrange
			var sut = CreateEmptyTestViewModel();
			IFocusNavigatable actual = null;
			sut.FocusPreviousRequested += (sender, e) => actual = e.Value;

			// Act
			sut.NavigateFocusToPreviousElementCommand.Execute(null);

			// Assert
			Assert.That(actual, Is.EqualTo(sut));
		}

		[Test]
		public void NavigateFocusToNextElementCommand_Should_RaiseFocusNextRequestedEvent_When_Executed()
		{
			// Arrange
			var sut = CreateEmptyTestViewModel();
			IFocusNavigatable actual = null;
			sut.FocusNextRequested += (sender, e) => actual = e.Value;

			// Act
			sut.NavigateFocusToNextElementCommand.Execute(null);

			// Assert
			Assert.That(actual, Is.EqualTo(sut));
		}

		[Test]
		public void FocusNavigation_Should_SendFocusNavigationNotification_When_Executed()
		{
			// Arrange
			var sut = CreateEmptyTestViewModel();
			var messengerMock = new Mock<IMessenger>();
			messengerMock.Setup(mm => mm.Send(It.Is<NotificationMessage>(
				n => n.Sender == sut && n.Notification == MessageCommunicationProtocol.FocusNavigationRequest)));
			Messenger.OverrideDefault(messengerMock.Object);

			// Act
			sut.FocusNavigation();

			// Assert
			messengerMock.Verify();
		}
	}
}
