﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Data;
using GalaSoft.MvvmLight.Messaging;
using Moq;
using NUnit.Framework;
using Retrotimer;
using Retrotimer.Common;
using Retrotimer.Data.State;
using Retrotimer.Model;
using Retrotimer.Services;
using Retrotimer.Services.Data;
using Retrotimer.View;
using Retrotimer.ViewModel;

namespace ViewModel
{
    [TestFixture]
	public class RetroPlanViewModelTest
	{
		Mock<IRetrospectiveInteractor> retroInteractorMock;
		Mock<INavigationService> navigationMock;
		Mock<IVisualStyleGenerator> styleGenMock;
		Mock<IRetrospectivePlayer> retroPlayerMock;
		Mock<IRetrospectivePlayerProvider> retroPlayerProvMock;
		Mock<IFocusNavigationService> focusNavSvcMock;
		Mock<IFocusNavigationServiceProvider> focusNavSvcProvMock;
		Mock<IViewModelLocator> viewModelLocatorMock;
		Mock<IMessenger> messengerMock;
		RetroPlanViewModel sut;

		[SetUp]
		public void SetUp()
		{
			messengerMock = new Mock<IMessenger>();
			Messenger.OverrideDefault(messengerMock.Object);

			retroInteractorMock = new Mock<IRetrospectiveInteractor>();
			retroInteractorMock.Setup(ssm => ssm.ReadAsync())
				.ThrowsAsync(new StateStorageException("", null));
            
			navigationMock = new Mock<INavigationService>();
			styleGenMock = new Mock<IVisualStyleGenerator>();
			retroPlayerMock = new Mock<IRetrospectivePlayer>();
			retroPlayerProvMock = new Mock<IRetrospectivePlayerProvider>();
			retroPlayerProvMock.Setup(pp => pp.CreatePlayerForPlan(It.IsAny<IList<IRetroPlaylistItem>>()))
							   .Returns(retroPlayerMock.Object);
			focusNavSvcMock = new Mock<IFocusNavigationService>();
			focusNavSvcProvMock = new Mock<IFocusNavigationServiceProvider>();
			focusNavSvcProvMock.Setup(fpm => fpm.CreateFocusNavigationService(It.IsAny<IList<IFocusNavigatable>>()))
							   .Returns(focusNavSvcMock.Object);
			viewModelLocatorMock = new Mock<IViewModelLocator>();

			sut = new RetroPlanViewModel(retroInteractorMock.Object, navigationMock.Object,
										 styleGenMock.Object, retroPlayerProvMock.Object,
										 focusNavSvcProvMock.Object, viewModelLocatorMock.Object);
		}

		[TearDown]
		public void TearDown()
		{
			sut = null;
			retroInteractorMock = null;
			navigationMock = null;
			styleGenMock = null;
			retroPlayerMock = null;
			focusNavSvcProvMock = null;
			viewModelLocatorMock = null;

			Messenger.Reset();
		}

		static List<ActivityFullViewModel> GetCurrentActivitiesFrom(RetroPlanViewModel retroPlanViewModel)
		{
			return new List<ActivityFullViewModel>
			{
				retroPlanViewModel.CurrentSetTheStageActivity,
				retroPlanViewModel.CurrentGatherDataActivity,
				retroPlanViewModel.CurrentGenerateInsightsActivity,
				retroPlanViewModel.CurrentDecideWhatToDoActivity,
				retroPlanViewModel.CurrentCloseTheRetrospectiveActivity
			};
		}

		[Test]
		public void Ctor_Should_RegisterItselfForActivitiesTitleTappedNotification()
		{
			// Arrange
			var retroGenerator = Mock.Of<IRetrospectiveInteractor>();
			var navigation = Mock.Of<INavigationService>();
			var styleGen = Mock.Of<IVisualStyleGenerator>();
			var retroPlayerProv = Mock.Of<IRetrospectivePlayerProvider>();
			var focusNavSvcProv = Mock.Of<IFocusNavigationServiceProvider>();
			var viewModelLocator = Mock.Of<IViewModelLocator>();

			// Act
			var newSut = new RetroPlanViewModel(retroGenerator, navigation, styleGen,
												retroPlayerProv, focusNavSvcProv, viewModelLocator);

			// Arrange
			messengerMock.Verify(mm => mm.Register(newSut, It.IsAny<Action<NotificationMessage>>()), Times.Once());
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_AssignRandomActivityViewModels_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			SetupRetroInteractorMockWithSecondTestActivityCollection();

			// Act
			sut.LoadRetrospectiveCommand.Execute(null);
			var setStage2 = sut.CurrentSetTheStageActivity;
			var gatherData2 = sut.CurrentGatherDataActivity;
			var generInsights2 = sut.CurrentGenerateInsightsActivity;
			var decide2 = sut.CurrentDecideWhatToDoActivity;
			var close2 = sut.CurrentCloseTheRetrospectiveActivity;

			// Assert
			retroInteractorMock.Verify();
			AssertActivityViewModelHasActivity(setStage2, TestData.SecondSetTheStageActivityWithTimebox.Activity);
			AssertActivityViewModelHasActivity(gatherData2, TestData.SecondGatherDataActivityWithTimebox.Activity);
			AssertActivityViewModelHasActivity(generInsights2, TestData.SecondGenerateInsightsActivityWithTimebox.Activity);
			AssertActivityViewModelHasActivity(decide2, TestData.SecondDecideWhatToDoActivityWithTimebox.Activity);
			AssertActivityViewModelHasActivity(close2, TestData.SecondCloseTheRetrospectiveActivityWithTimebox.Activity);
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_RaisedPropertyChangedForCurrentActivities_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();

			var isTestedPropertyChangedEventRaised = new Dictionary<string, bool>();
			isTestedPropertyChangedEventRaised.Add(nameof(sut.CurrentSetTheStageActivity), false);
			isTestedPropertyChangedEventRaised.Add(nameof(sut.CurrentGatherDataActivity), false);
			isTestedPropertyChangedEventRaised.Add(nameof(sut.CurrentGenerateInsightsActivity), false);
			isTestedPropertyChangedEventRaised.Add(nameof(sut.CurrentDecideWhatToDoActivity), false);
			isTestedPropertyChangedEventRaised.Add(nameof(sut.CurrentCloseTheRetrospectiveActivity), false);

			sut.PropertyChanged += (sender, e) =>
			{
				if (isTestedPropertyChangedEventRaised.ContainsKey(e.PropertyName))
				{
					isTestedPropertyChangedEventRaised[e.PropertyName] = true;
				}
			};

			// Act
			sut.LoadRetrospectiveCommand.Execute(null);

			// Assert
			Assert.That(isTestedPropertyChangedEventRaised.Values, Has.All.EqualTo(true));
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_UseVisualStyleGeneratorToStyleActvities_When_NoStylesRestoredFromStorage()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			var expectedStyles = new List<VisualStyle>
			{
				VisualStyle.DarkOrange,
				VisualStyle.LightBlue,
				VisualStyle.DarkBlue,
				VisualStyle.DarkOrange,
				VisualStyle.Blue
			};
			styleGenMock.Setup(sgm => sgm.GenerateVisualStylesWithoutIdenticalNeighbours(5))
						.Returns(expectedStyles);
			retroInteractorMock.Setup(ssm => ssm.ReadAsync())
								.ThrowsAsync(new StateStorageException("", null));

			// Act
			sut.LoadRetrospectiveCommand.Execute(null);
			var resultStyles = GetCurrentStylesFrom(sut);

			// Assert
			Assert.That(resultStyles, Is.EquivalentTo(expectedStyles));
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_UseRestoredStylesToStyleActivities_When_StylesSuccessfullyRestoredFromStorage()
		{
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			var expectedStyles = new List<VisualStyle>
			{
				VisualStyle.DarkOrange,
				VisualStyle.DarkBlue,
				VisualStyle.LightOrange,
				VisualStyle.Blue,
				VisualStyle.DarkOrange
			};
			var testState = new RetroPlanState(default(RetrospectivePlan), expectedStyles);
			retroInteractorMock.Setup(cm => cm.ReadAsync())
								.ReturnsAsync(testState);
			styleGenMock.Reset();

			// Act
			sut.LoadRetrospectiveCommand.Execute(null);
			var result = GetCurrentStylesFrom(sut);

			// Assert
			Assert.That(result, Is.EquivalentTo(expectedStyles));
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_ResetPlayer_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);
			retroPlayerMock.ResetCalls();

			// Act
			sut.LoadRetrospectiveCommand.Execute(null);

			// Assert
			retroPlayerMock.Verify(rp => rp.Reset(), Times.Once());
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_SetIsCountdownActiveToFalse()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			var before = sut.IsCountdownActive;
			sut.LoadRetrospectiveCommand.Execute(null);
			var after = sut.IsCountdownActive;

			// Assert
			Assert.That(before, Is.Not.False);
			Assert.That(after, Is.False);
		}

		[Test]
		public void IsCountdownActive_Should_BeFalseByDefault()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);

			// Act
			var actual = sut.IsCountdownActive;

			// Assert
			Assert.That(actual, Is.False);
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_SetEditModeOnActivitiesToTrue_When_Invoked()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();

			// Act
			sut.LoadRetrospectiveCommand.Execute(null);
			var currents = GetCurrentsFrom(sut);

			// Assert
			Assert.That(currents.Select(a => a.IsEditMode), Has.None.False);
		}

		[Test]
		public void GenerateRandomRetrospectiveCommand_Should_ResetPlayer_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);
			retroPlayerMock.ResetCalls();

			// Act
			sut.GenerateRandomRetrospectiveCommand.Execute(null);

			// Assert
			retroPlayerMock.Verify(rp => rp.Reset(), Times.Once());
		}

		[Test]
		public void GenerateRandomRetrospectiveCommand_Should_DeactivateCountdown_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			var before = sut.IsCountdownActive;
			sut.GenerateRandomRetrospectiveCommand.Execute(null);
			var after = sut.IsCountdownActive;

			// Assert
			Assert.That(before, Is.True);
			Assert.That(after, Is.False);
		}

		[Test]
		public void GenerateRandomRetrospectiveCommand_Should_AssignRandomActivityViewModels_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			SetupRetroInteractorMockWithSecondTestActivityCollection();

			// Act
			sut.GenerateRandomRetrospectiveCommand.Execute(null);
			var setStage2 = sut.CurrentSetTheStageActivity;
			var gatherData2 = sut.CurrentGatherDataActivity;
			var generInsights2 = sut.CurrentGenerateInsightsActivity;
			var decide2 = sut.CurrentDecideWhatToDoActivity;
			var close2 = sut.CurrentCloseTheRetrospectiveActivity;

			// Assert
			retroInteractorMock.Verify();
			AssertActivityViewModelHasActivity(setStage2, TestData.SecondSetTheStageActivityWithTimebox.Activity);
			AssertActivityViewModelHasActivity(gatherData2, TestData.SecondGatherDataActivityWithTimebox.Activity);
			AssertActivityViewModelHasActivity(generInsights2, TestData.SecondGenerateInsightsActivityWithTimebox.Activity);
			AssertActivityViewModelHasActivity(decide2, TestData.SecondDecideWhatToDoActivityWithTimebox.Activity);
			AssertActivityViewModelHasActivity(close2, TestData.SecondCloseTheRetrospectiveActivityWithTimebox.Activity);
		}

		[Test]
		public void GenerateRandomRetrospectiveCommand_Should_SetEditModeOnActivitiesToTrue_When_Invoked()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);

			// Act
			sut.GenerateRandomRetrospectiveCommand.Execute(null);
			var currents = GetCurrentsFrom(sut);

			// Assert
			Assert.That(currents.Select(a => a.IsEditMode), Has.All.True);
		}

		[Test]
		public void GenerateRandomRetrospectiveCommand_Should_UseVisualStyleGeneratorToStyleActvities()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			var expectedStyles = new List<VisualStyle>
			{
				VisualStyle.DarkOrange,
				VisualStyle.LightBlue,
				VisualStyle.DarkBlue,
				VisualStyle.DarkOrange,
				VisualStyle.Blue
			};
			styleGenMock.Setup(sgm => sgm.GenerateVisualStylesWithoutIdenticalNeighbours(5))
						.Returns(expectedStyles);

			// Act
			sut.GenerateRandomRetrospectiveCommand.Execute(null);
			var resultStyles = GetCurrentStylesFrom(sut);

			// Assert
			Assert.That(resultStyles, Is.EquivalentTo(expectedStyles));
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_SetFocusNavigationIndexOfActivities()
		{
			// Arrange
			var expectedIndexes = new List<int> { 1, 2, 3, 4, 5 };
			SetupRetroInteractorMockWithFirstTestActivityCollection();

			// Act
			sut.LoadRetrospectiveCommand.Execute(null);
			var currents = GetCurrentsFrom(sut);

			// Assert
			Assert.That(currents.Select(a => a.FocusNavigationIndex), Is.EquivalentTo(expectedIndexes));
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_CreateFocusNavigationServiceWithActivities()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();

			// Act
			sut.LoadRetrospectiveCommand.Execute(null);
			var expectedCollection = new List<IFocusNavigatable>
			{
				sut.CurrentSetTheStageActivity,
				sut.CurrentGatherDataActivity,sut.CurrentGenerateInsightsActivity,
				sut.CurrentDecideWhatToDoActivity,
				sut.CurrentCloseTheRetrospectiveActivity
			};

			// Assert
			focusNavSvcProvMock.Verify(
				fm => fm.CreateFocusNavigationService(
					It.Is<IList<IFocusNavigatable>>(
						a => CheckCollectionEquality(a, expectedCollection))));
		}

		[Test]
		public void StartRetrospective_Should_UseActivitiesViewModels_When_GeneratingRetroPlayer()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var expectedPlaylist = new List<IRetroPlaylistItem>
			{
				sut.CurrentSetTheStageActivity,
				sut.CurrentGatherDataActivity,
				sut.CurrentGenerateInsightsActivity,
				sut.CurrentDecideWhatToDoActivity,
				sut.CurrentCloseTheRetrospectiveActivity
			};

			// Act
			sut.StartRetrospectiveCommand.Execute(null);

			// Assert
			retroPlayerProvMock.Verify(pp => pp.CreatePlayerForPlan(It.Is<IList<IRetroPlaylistItem>>(l => l.SequenceEqual(expectedPlaylist))));
		}

		[Test]
		public void StartRetrospective_Should_StartCountdown_When_NoActiveCountdown()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);

			// Act
			sut.StartRetrospectiveCommand.Execute(null);

			// Assert
			retroPlayerMock.Verify(p => p.StartOrResume(), Times.Once());
		}

		[Test]
		public void StartRetrospective_Should_ChangeCountdownStatusToRunning()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);

			// Act
			sut.StartRetrospectiveCommand.Execute(null);
			var result = sut.IsCountdownActive;

			// Assert
			Assert.That(result, Is.True);
		}

		[Test]
		public void StartRetrospective_Should_ResumeCountdown_When_CountdownIsPaused()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);
			sut.PauseRetrospectiveCommand.Execute(null);
			retroPlayerMock.ResetCalls();

			// Act
			var before = sut.IsCountdownActive;
			sut.StartRetrospectiveCommand.Execute(null);
			var after = sut.IsCountdownActive;

			// Assert
			retroPlayerMock.Verify(p => p.StartOrResume(), Times.Once());
			Assert.That(before, Is.False);
			Assert.That(after, Is.True);
		}

		[Test]
		public void StartRetrospectiveCommand_Should_SetEditModeOnActivitiesToFalse_When_Invoked()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);

			// Act
			sut.StartRetrospectiveCommand.Execute(null);
			var currents = GetCurrentsFrom(sut);

			// Assert
			Assert.That(currents.Select(a => a.IsEditMode), Has.All.False);
		}

		[Test]
		public void PauseRetrospective_Should_PausePlayer_When_RetrospectiveHasBeenStarted()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			sut.PauseRetrospectiveCommand.Execute(null);

			// Assert
			retroPlayerMock.Verify(rp => rp.Pause(), Times.Once());
		}

		[Test]
		public void PauseRetrospective_Should_ChangeCountdownStatusToPaused_When_RetrospectiveHasBeenStarted()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			var before = sut.IsCountdownActive;
			sut.PauseRetrospectiveCommand.Execute(null);
			var after = sut.IsCountdownActive;

			// Assert
			Assert.That(before, Is.True);
			Assert.That(after, Is.False);
		}

		[Test]
		public void PauseRetrospective_Should_NotPausePlayer_When_RetrospectiveWasntStarted()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);

			// Act
			sut.PauseRetrospectiveCommand.Execute(null);

			// Assert
			retroPlayerMock.Verify(rp => rp.Pause(), Times.Never());
		}

		[Test]
		public void PauseRetrospective_Should_NotChangeCountdownStatus_When_RetrospectiveWasntStarted()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);

			// Act
			var before = sut.IsCountdownActive;
			sut.PauseRetrospectiveCommand.Execute(null);
			var after = sut.IsCountdownActive;

			// Assert
			Assert.That(before, Is.False);
			Assert.That(after, Is.False);
		}

		[Test]
		public void PauseRetrospectiveCommand_Should_SetEditModeOnActivitiesToFalse_When_RetrospectiveHasBeenStarted()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			sut.PauseRetrospectiveCommand.Execute(null);
			var currents = GetCurrentsFrom(sut);

			// Assert
			Assert.That(currents.Select(a => a.IsEditMode), Has.All.False);
		}

		[Test]
		public void MoveToPreviousActivity_Should_BeDisabled_When_CurrentActivityIsTheFirstOne()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			retroPlayerMock.Setup(rpm => rpm.CheckIfPlayingFirstItem()).Returns(true);

			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			var result = sut.MoveToPreviousActivity.CanExecute(null);

			// Assert
			Assert.That(result, Is.False);
		}

		[Test]
		public void MoveToPreviousActivity_Should_BeDisabled_When_RetroNeverStarted()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			retroPlayerMock.Setup(rpm => rpm.CheckIfPlayingFirstItem()).Returns(false);

			// Act
			var result = sut.MoveToNextActivity.CanExecute(null);

			// Assert
			Assert.That(result, Is.False);
		}

		[Test]
		public void MoveToPreviousActivity_Should_BeEnabled_When_CurrentActivityIsNotTheFirstOne()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			retroPlayerMock.Setup(rpm => rpm.CheckIfPlayingFirstItem()).Returns(false);

			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			var result = sut.MoveToPreviousActivity.CanExecute(null);

			// Assert
			Assert.That(result, Is.True);
		}

		[Test]
		public void MoveToPreviousActivity_Should_MoveRetroPlayerBackwards_When_RetroInProgress()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			sut.MoveToPreviousActivity.Execute(null);

			// Assert
			retroPlayerMock.Verify(rpm => rpm.MoveBackwards());
		}

		[Test]
		public void MoveToPreviousCanExecute_Should_Execute_When_PlayerRaisesPlayedItemChanged()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			var canExecuteChanged = false;
			sut.MoveToPreviousActivity.CanExecuteChanged += (sender, e) => canExecuteChanged = true;
			var args = new EventArgs<IRetroPlaylistItem>(null);

			// Act
			retroPlayerMock.Raise(rpm => rpm.PlayedItemChanged += null, args);

			// Assert
			Assert.That(canExecuteChanged, Is.True);
		}

		[Test]
		public void StartRetrospective_Should_RefreshMoveToPreviousActivityAvailability_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);

			var canExecuteChanged = false;
			sut.MoveToPreviousActivity.CanExecuteChanged += (sender, e) => canExecuteChanged = true;

			// Act
			sut.StartRetrospectiveCommand.Execute(null);

			// Assert
			Assert.That(canExecuteChanged, Is.True);
		}

		[Test]
		public void PauseRetrospective_Should_RefreshMoveToPreviousActivityAvailability_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			var canExecuteChanged = false;
			sut.MoveToPreviousActivity.CanExecuteChanged += (sender, e) => canExecuteChanged = true;

			// Act
			sut.PauseRetrospectiveCommand.Execute(null);

			// Assert
			Assert.That(canExecuteChanged, Is.True);
		}

		[Test]
		public void ResetRetrospective_Should_RefreshMoveToPreviousActivityAvailability_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			var canExecuteChanged = false;
			sut.MoveToNextActivity.CanExecuteChanged += (sender, e) => canExecuteChanged = true;

			// Act
			sut.ResetRetrospectiveCommand.Execute(null);
			var result = sut.MoveToNextActivity.CanExecute(null);

			// Assert
			Assert.That(canExecuteChanged, Is.True);
			Assert.That(result, Is.False);
		}

		[Test]
		public void MoveToNextActivity_Should_BeDisabled_When_CurrentActivityIsTheLastOne()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			retroPlayerMock.Setup(rpm => rpm.CheckIfPlayingLastItem()).Returns(true);

			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			var result = sut.MoveToNextActivity.CanExecute(null);

			// Assert
			Assert.That(result, Is.False);
		}

		[Test]
		public void MoveToNextActivity_Should_BeDisabled_When_RetroNeverStarted()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			retroPlayerMock.Setup(rpm => rpm.CheckIfPlayingLastItem()).Returns(false);

			// Act
			var result = sut.MoveToNextActivity.CanExecute(null);

			// Assert
			Assert.That(result, Is.False);
		}

		[Test]
		public void MoveToNextActivity_Should_BeEnabled_When_CurrentActivityIsNotTheLastOne()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			retroPlayerMock.Setup(rpm => rpm.CheckIfPlayingLastItem()).Returns(false);

			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			var result = sut.MoveToNextActivity.CanExecute(null);

			// Assert
			Assert.That(result, Is.True);
		}

		[Test]
		public void MoveToNextActivity_Should_MoveRetroPlayerForwards_When_RetroInProgress()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			sut.MoveToNextActivity.Execute(null);

			// Assert
			retroPlayerMock.Verify(rpm => rpm.MoveForwards());
		}

		[Test]
		public void MoveToNextCanExecute_Should_Execute_When_PlayerRaisesPlayedItemChanged()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			var canExecuteChanged = false;
			sut.MoveToNextActivity.CanExecuteChanged += (sender, e) => canExecuteChanged = true;
			var args = new EventArgs<IRetroPlaylistItem>(null);

			// Act
			retroPlayerMock.Raise(rpm => rpm.PlayedItemChanged += null, args);

			// Assert
			Assert.That(canExecuteChanged, Is.True);
		}

		[Test]
		public void StartRetrospective_Should_RefreshMoveToNextActivityAvailability_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);

			var canExecuteChanged = false;
			sut.MoveToNextActivity.CanExecuteChanged += (sender, e) => canExecuteChanged = true;

			// Act
			sut.StartRetrospectiveCommand.Execute(null);

			// Assert
			Assert.That(canExecuteChanged, Is.True);
		}

		[Test]
		public void PauseRetrospective_Should_RefreshMoveToNextActivityAvailability_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			var canExecuteChanged = false;
			sut.MoveToNextActivity.CanExecuteChanged += (sender, e) => canExecuteChanged = true;

			// Act
			sut.PauseRetrospectiveCommand.Execute(null);

			// Assert
			Assert.That(canExecuteChanged, Is.True);
		}

		[Test]
		public void ResetRetrospective_Should_RefreshMoveToNextActivityAvailability_When_Executed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			var canExecuteChanged = false;
			sut.MoveToNextActivity.CanExecuteChanged += (sender, e) => canExecuteChanged = true;

			// Act
			sut.ResetRetrospectiveCommand.Execute(null);
			var result = sut.MoveToNextActivity.CanExecute(null);

			// Assert
			Assert.That(canExecuteChanged, Is.True);
			Assert.That(result, Is.False);
		}

		[Test]
		public void PauseRetrospectiveCommand_Should_SetEditModeOnActivitiesToTrue_When_CountdownPaused()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);
			sut.PauseRetrospectiveCommand.Execute(null);

			// Act
			sut.ResetRetrospectiveCommand.Execute(null);
			var currents = GetCurrentsFrom(sut);

			// Assert
			Assert.That(currents.Select(a => a.IsEditMode), Has.All.True);
		}

		[Test]
		public void ResetRetrospective_Should_ChangeCountdownStatusToInactive_When_CountdownRunning()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			sut.ResetRetrospectiveCommand.Execute(null);
			var actual = sut.IsCountdownActive;

			// Assert
			Assert.That(actual, Is.False);
		}

		[Test]
		public void ResetRetrospective_Should_ResetPlayer_When_CountdownRunning()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			sut.ResetRetrospectiveCommand.Execute(null);

			// Assert
			retroPlayerMock.Verify(p => p.Reset(), Times.Once());
		}

		[Test]
		public void ResetRetrospective_Should_ResetPlayer_When_CountdownPaused()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);
			sut.PauseRetrospectiveCommand.Execute(null);

			// Act
			sut.ResetRetrospectiveCommand.Execute(null);

			// Assert
			retroPlayerMock.Verify(p => p.Reset(), Times.Once());
		}

		[Test]
		public void ResetRetrospective_Should_DeactivateCountdown_When_CountdownPaused()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);
			sut.PauseRetrospectiveCommand.Execute(null);

			// Act
			sut.ResetRetrospectiveCommand.Execute(null);
			var actual = sut.IsCountdownActive;

			// Assert
			Assert.That(actual, Is.False);
		}

		[Test]
		public void CurrentlyPlayedActivity_Should_BeSet_When_PlayerRaisedPlayedItemChangedEvent()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			var testActivity = sut.CurrentGatherDataActivity as IRetroPlaylistItem;

			// Act
			var args = new EventArgs<IRetroPlaylistItem>(testActivity);
			retroPlayerMock.Raise(rpm => rpm.PlayedItemChanged += null, args);

			// Assert
			Assert.That(sut.CurrentlyPlayedActivity, Is.EqualTo(testActivity));
		}

		[Test]
		public void CurrentlyPlayedActivity_Should_BeSet_When_PlayerRaisedPlayedItemChangedWithNull()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			var args = new EventArgs<IRetroPlaylistItem>(null);
			retroPlayerMock.Raise(rpm => rpm.PlayedItemChanged += null, args);

			// Assert
			Assert.That(sut.CurrentlyPlayedActivity, Is.Null);
		}

		[Test]
		public void CurrentlyPlayedActivity_Should_RaiseItsChanged_When_NewValueIsAssigned()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			var testActivity = sut.CurrentGatherDataActivity as IRetroPlaylistItem;
			var wasEventRaised = false;
			sut.PropertyChanged += (sender, e) => wasEventRaised |= e.PropertyName == nameof(RetroPlanViewModel.CurrentlyPlayedActivity);

			// Act
			var args = new EventArgs<IRetroPlaylistItem>(testActivity);
			retroPlayerMock.Raise(rpm => rpm.PlayedItemChanged += null, args);

			// Assert
			Assert.That(wasEventRaised, Is.True);
		}

		[Test]
		public void RetrospectiveFinished_Should_ResetPlayer_When_Raised()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			retroPlayerMock.Raise(rpm => rpm.RetrospectiveFinished += null, EventArgs.Empty);

			// Assert
			retroPlayerMock.Verify(rpm => rpm.Reset());
		}

		[Test]
		public void RetrospectiveFinished_Should_DeactivateCountdown_When_Raised()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			sut.StartRetrospectiveCommand.Execute(null);

			// Act
			var before = sut.IsCountdownActive;
			retroPlayerMock.Raise(rpm => rpm.RetrospectiveFinished += null, EventArgs.Empty);
			var after = sut.IsCountdownActive;

			// Assert
			Assert.That(before, Is.True);
			Assert.That(after, Is.False);
		}

		[Test]
		public void TitleTappedMessage_ShouldNot_TriggerNavigation_When_SentByNonCurrentActivityViewModel()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			Action<NotificationMessage> callback = null;
			messengerMock.Setup(mm => mm.Register(It.IsAny<RetroPlanViewModel>(), It.IsAny<Action<NotificationMessage>>()))
						 .Callback<object, Action<NotificationMessage>>((r, a) => callback = a);

			var fakeAvm = new ActivityFullViewModel(TestData.FirstGatherDataActivityWithTimebox);
			// Local sut is necessary because messenger mock is invoked in constructor.
			var localSut = new RetroPlanViewModel(retroInteractorMock.Object, navigationMock.Object,
										 styleGenMock.Object, retroPlayerProvMock.Object,
												  focusNavSvcProvMock.Object, viewModelLocatorMock.Object);
			localSut.LoadRetrospectiveCommand.Execute(null);

			// Act
			var msg = new NotificationMessage(fakeAvm, MessageCommunicationProtocol.TitleTappedMessage);
			callback(msg);

			// Assert
            navigationMock.Verify(nm => 
	            nm.PushAsync(NavigationPages.BrowseStagePage, true, It.IsAny<IStageBrowserViewModel>()), Times.Never());
		}

		[Test]
		public void TitleTappedMessage_ShouldNot_NotTriggerNavigation_When_CountdownIsActive()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			Action<NotificationMessage> callback = null;
			messengerMock.Setup(mm => mm.Register(It.IsAny<RetroPlanViewModel>(), It.IsAny<Action<NotificationMessage>>()))
						 .Callback<object, Action<NotificationMessage>>((r, a) => callback = a);

			// Local sut is necessary because messenger mock is invoked in constructor.
			var localSut = new RetroPlanViewModel(retroInteractorMock.Object, navigationMock.Object,
										 styleGenMock.Object, retroPlayerProvMock.Object,
												  focusNavSvcProvMock.Object, viewModelLocatorMock.Object);
			localSut.LoadRetrospectiveCommand.Execute(null);
			localSut.StartRetrospectiveCommand.Execute(null);

			// Act
			var msg = new NotificationMessage(localSut.CurrentSetTheStageActivity, MessageCommunicationProtocol.TitleTappedMessage);
			callback(msg);

			// Assert
			navigationMock.Verify(nm => nm.PushAsync(NavigationPages.BrowseStagePage, true, It.IsAny<IStageBrowserViewModel>()), Times.Never());
		}

		[Test]
		public void TitleTappedMessage_ShouldNot_NotTriggerNavigation_When_CountdownIsPaused()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			Action<NotificationMessage> callback = null;
			messengerMock.Setup(mm => mm.Register(It.IsAny<RetroPlanViewModel>(), It.IsAny<Action<NotificationMessage>>()))
						 .Callback<object, Action<NotificationMessage>>((r, a) => callback = a);

			// Local sut is necessary because messenger mock is invoked in constructor.
			var localSut = new RetroPlanViewModel(retroInteractorMock.Object, navigationMock.Object,
										 styleGenMock.Object, retroPlayerProvMock.Object,
												  focusNavSvcProvMock.Object, viewModelLocatorMock.Object);
			localSut.LoadRetrospectiveCommand.Execute(null);
			localSut.StartRetrospectiveCommand.Execute(null);
			localSut.PauseRetrospectiveCommand.Execute(null);

			// Act
			var msg = new NotificationMessage(localSut.CurrentSetTheStageActivity, MessageCommunicationProtocol.TitleTappedMessage);
			callback(msg);

			// Assert
			navigationMock.Verify(nm => nm.PushAsync(NavigationPages.BrowseStagePage, true, null), Times.Never());
		}

		[Test]
		public void TitleTappedMessage_Should_PassViewModelToStageBrowser_When_NavigationTriggered()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			Action<NotificationMessage> callback = null;
			messengerMock.Setup(mm => mm.Register(It.IsAny<RetroPlanViewModel>(), It.IsAny<Action<NotificationMessage>>()))
						 .Callback<object, Action<NotificationMessage>>((r, a) => callback = a);

			var activitiesForBrowser = new List<ActivitySelectableBasicViewModel>(
						TestData.CreateTestActivitiesWithTimebox().Where(at => at.Activity.Phase == RetroPhase.GenerateInsights)
												.Select(at => new ActivitySelectableBasicViewModel(at.Activity)));

			var stageViewModelMock = new Mock<IStageBrowserViewModel>();
			stageViewModelMock.Setup(svm => svm.ActivitiesInPhase).Returns(activitiesForBrowser);
            viewModelLocatorMock.Setup(vm => vm.GetViewModel<IStageBrowserViewModel>(null))
								.Returns(stageViewModelMock.Object);

			// Local sut is necessary because messenger mock is invoked in constructor.
			var localSut = new RetroPlanViewModel(retroInteractorMock.Object, navigationMock.Object,
										 styleGenMock.Object, retroPlayerProvMock.Object,
												  focusNavSvcProvMock.Object, viewModelLocatorMock.Object);
			localSut.LoadRetrospectiveCommand.Execute(null);

			// Act
			var msg = new NotificationMessage(
				localSut.CurrentGenerateInsightsActivity, MessageCommunicationProtocol.TitleTappedMessage);
			callback(msg);

			// Assert
			navigationMock.Verify(nm => 
				nm.PushAsync(NavigationPages.BrowseStagePage, true, stageViewModelMock.Object), Times.Once());
		}

		[Test]
		[TestCase(RetroPhase.SetTheStage)]
		[TestCase(RetroPhase.GatherData)]
		[TestCase(RetroPhase.GenerateInsights)]
		[TestCase(RetroPhase.DecideWhatToDo)]
		[TestCase(RetroPhase.CloseTheRetrospective)]
		public void TitleTappedMessage_Should_SetSelectedActivityOnStageBrowserToCurrentPhase_When_NavigationTriggered(
			RetroPhase inputPhase)
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			Action<NotificationMessage> callback = null;
			messengerMock.Setup(mm => mm.Register(It.IsAny<RetroPlanViewModel>(), It.IsAny<Action<NotificationMessage>>()))
						 .Callback<object, Action<NotificationMessage>>((r, a) => callback = a);

			var activitiesForBrowser = new List<ActivitySelectableBasicViewModel>(
						TestData.CreateTestActivitiesWithTimebox().Where(at => at.Activity.Phase == inputPhase)
												.Select(at => new ActivitySelectableBasicViewModel(at.Activity)));

			var stageViewModelMock = new Mock<IStageBrowserViewModel>();
			stageViewModelMock.Setup(svm => svm.ActivitiesInPhase).Returns(activitiesForBrowser);
            viewModelLocatorMock.Setup(vm => vm.GetViewModel<IStageBrowserViewModel>(null))
								.Returns(stageViewModelMock.Object);

			// Local sut is necessary because messenger mock is invoked in constructor.
			var localSut = new RetroPlanViewModel(retroInteractorMock.Object, navigationMock.Object,
										 styleGenMock.Object, retroPlayerProvMock.Object,
												  focusNavSvcProvMock.Object, viewModelLocatorMock.Object);
			localSut.LoadRetrospectiveCommand.Execute(null);
			var currents = GetCurrentActivitiesFrom(localSut);
			var messageSender = currents.First(a => a.Phase == inputPhase);

			// Act
			var msg = new NotificationMessage(messageSender, MessageCommunicationProtocol.TitleTappedMessage);
			callback(msg);

			// Assert
			stageViewModelMock.VerifySet(svm => 
				svm.SelectedActivity = It.Is<ActivitySelectableBasicViewModel>(a => a.Id == messageSender.Id));
		}

		[Test]
		public void FocusNavigationRequestMessage_ShouldNot_TriggerNavigation_When_ReceivedByDispatch()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			Action<NotificationMessage> callback = null;
			messengerMock.Setup(mm => mm.Register(It.IsAny<RetroPlanViewModel>(), It.IsAny<Action<NotificationMessage>>()))
						 .Callback<object, Action<NotificationMessage>>((r, a) => callback = a);

			var localSut = new RetroPlanViewModel(retroInteractorMock.Object, navigationMock.Object,
										 styleGenMock.Object, retroPlayerProvMock.Object,
												  focusNavSvcProvMock.Object, viewModelLocatorMock.Object);
			localSut.LoadRetrospectiveCommand.Execute(null);

			// Act
			var msg = new NotificationMessage(localSut.CurrentSetTheStageActivity, MessageCommunicationProtocol.FocusNavigationRequest);
			callback(msg);

			// Assert
			navigationMock.Verify(nm => nm.PushAsync(NavigationPages.BrowseStagePage, true, null), Times.Never());
		}

		[Test]
		[TestCase(RetroPhase.SetTheStage, nameof(RetroPlanViewModel.CurrentSetTheStageActivity))]
		[TestCase(RetroPhase.GatherData, nameof(RetroPlanViewModel.CurrentGatherDataActivity))]
		[TestCase(RetroPhase.GenerateInsights, nameof(RetroPlanViewModel.CurrentGenerateInsightsActivity))]
		[TestCase(RetroPhase.DecideWhatToDo, nameof(RetroPlanViewModel.CurrentDecideWhatToDoActivity))]
		[TestCase(RetroPhase.CloseTheRetrospective, nameof(RetroPlanViewModel.CurrentCloseTheRetrospectiveActivity))]
		public void UpdateRetroPlanWith_Should_RaiseSetTheStagePropertyChanged_When_SetTheStageInstancePassed(
			RetroPhase testPhase, string currentPropertyName)
		{
			// Arrange
			SetupRetroInteractorMockWithSecondTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.CreateTestActivitiesWithTimebox().Where(a => a.Activity.Phase == testPhase)
											  .Select(a => a.Activity)
											  .First();

			var eventRaised = false;
			sut.PropertyChanged += (sender, e) =>
			{
				eventRaised |= e.PropertyName == currentPropertyName;
			};

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			Assert.That(eventRaised, Is.True);
		}

		[Test]
		[TestCase(RetroPhase.SetTheStage)]
		[TestCase(RetroPhase.GatherData)]
		[TestCase(RetroPhase.GenerateInsights)]
		[TestCase(RetroPhase.DecideWhatToDo)]
		[TestCase(RetroPhase.CloseTheRetrospective)]
		public void UpdateRetroPlanWith_Should_EnableEditMode(RetroPhase testPhase)
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.CreateTestActivitiesWithTimebox().Where(a => a.Activity.Phase == testPhase)
											  .Select(a => a.Activity)
											  .First();

			// Act
			sut.UpdateRetroPlanWith(input);
			var currents = GetCurrentsFrom(sut);

			// Assert
			Assert.That(currents.Select(a => a.IsEditMode), Has.All.True);
		}

		[Test]
		public void UpdateRetroPlanWith_Should_UpdateCurrentSetTheStageActivity_When_SetTheStageInstancePassed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.SecondSetTheStageActivityWithTimebox.Activity;

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			AssertActivityViewModelHasActivity(sut.CurrentSetTheStageActivity, input);
		}

		[Test]
		public void UpdateRetroPlanWith_Should_NotChangeSetTheStageStyleAndTimeboxes_When_SetTheStageInstancePassed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.SecondSetTheStageActivityWithTimebox.Activity;
			var styleBefore = sut.CurrentSetTheStageActivity.CurrentVisualStyle = VisualStyle.LightBlue;
			var originalTimeboxBefore = sut.CurrentSetTheStageActivity.OriginalTimebox = TimeSpan.FromMinutes(13);

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			Assert.That(sut.CurrentSetTheStageActivity.CurrentVisualStyle, Is.EqualTo(styleBefore));
			Assert.That(sut.CurrentSetTheStageActivity.OriginalTimebox, Is.EqualTo(originalTimeboxBefore));
		}

		[Test]
		public void UpdateRetroPlanWith_Should_UpdateCurrentGatherDataActivity_When_GatherDataInstancePassed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.SecondGatherDataActivityWithTimebox.Activity;

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			AssertActivityViewModelHasActivity(sut.CurrentGatherDataActivity, input);
		}

		[Test]
		public void UpdateRetroPlanWith_Should_NotChangeGatherDataStyleAndTimeboxes_When_GatherDataInstancePassed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.SecondGatherDataActivityWithTimebox.Activity;
			var styleBefore = sut.CurrentGatherDataActivity.CurrentVisualStyle = VisualStyle.DarkBlue;
			var originalTimeboxBefore = sut.CurrentGatherDataActivity.OriginalTimebox = TimeSpan.FromMinutes(14);

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			Assert.That(sut.CurrentGatherDataActivity.CurrentVisualStyle, Is.EqualTo(styleBefore));
			Assert.That(sut.CurrentGatherDataActivity.OriginalTimebox, Is.EqualTo(originalTimeboxBefore));
		}

		[Test]
		public void UpdateRetroPlanWith_Should_UpdateCurrentGenerateInsightsActivity_When_GenerateInsightsInstancePassed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.SecondGenerateInsightsActivityWithTimebox.Activity;

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			AssertActivityViewModelHasActivity(sut.CurrentGenerateInsightsActivity, input);
		}

		[Test]
		public void UpdateRetroPlanWith_Should_NotChangeGenerateInsightsStyleAndTimeboxes_When_GenerateInsightsInstancePassed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.SecondGenerateInsightsActivityWithTimebox.Activity;
			var styleBefore = sut.CurrentGenerateInsightsActivity.CurrentVisualStyle = VisualStyle.Blue;
			var originalTimeboxBefore = sut.CurrentGenerateInsightsActivity.OriginalTimebox = TimeSpan.FromMinutes(17);

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			Assert.That(sut.CurrentGenerateInsightsActivity.CurrentVisualStyle, Is.EqualTo(styleBefore));
			Assert.That(sut.CurrentGenerateInsightsActivity.OriginalTimebox, Is.EqualTo(originalTimeboxBefore));
		}

		[Test]
		public void UpdateRetroPlanWith_Should_UpdateCurrentDecideWhatToDoActivity_When_DecideWhatToDoInstancePassed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.SecondDecideWhatToDoActivityWithTimebox.Activity;

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			AssertActivityViewModelHasActivity(sut.CurrentDecideWhatToDoActivity, input);
		}

		[Test]
		public void UpdateRetroPlanWith_Should_NotChangeDecideWhatToDoStyleAndTimeboxes_When_DecideWhatToDoInstancePassed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.SecondDecideWhatToDoActivityWithTimebox.Activity;
			var styleBefore = sut.CurrentDecideWhatToDoActivity.CurrentVisualStyle = VisualStyle.Blue;
			var originalTimeboxBefore = sut.CurrentDecideWhatToDoActivity.OriginalTimebox = TimeSpan.FromMinutes(23);

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			Assert.That(sut.CurrentDecideWhatToDoActivity.CurrentVisualStyle, Is.EqualTo(styleBefore));
			Assert.That(sut.CurrentDecideWhatToDoActivity.OriginalTimebox, Is.EqualTo(originalTimeboxBefore));
		}

		[Test]
		public void UpdateRetroPlanWith_Should_UpdateCurrentCloseTheRetrospectiveActivity_When_CloseTheRetrospectiveInstancePassed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.SecondCloseTheRetrospectiveActivityWithTimebox.Activity;

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			AssertActivityViewModelHasActivity(sut.CurrentCloseTheRetrospectiveActivity, input);
		}

		[Test]
		public void UpdateRetroPlanWith_Should_NotChangeCloseTheRetrospectiveStyleAndTimeboxes_When_CloseTheRetrospectiveInstancePassed()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);
			var input = TestData.SecondCloseTheRetrospectiveActivityWithTimebox.Activity;
			var styleBefore = sut.CurrentCloseTheRetrospectiveActivity.CurrentVisualStyle = VisualStyle.Blue;
			var originalTimeboxBefore = sut.CurrentCloseTheRetrospectiveActivity.OriginalTimebox = TimeSpan.FromMinutes(27);

			// Act
			sut.UpdateRetroPlanWith(input);

			// Assert
			Assert.That(sut.CurrentCloseTheRetrospectiveActivity.CurrentVisualStyle, Is.EqualTo(styleBefore));
			Assert.That(sut.CurrentCloseTheRetrospectiveActivity.OriginalTimebox, Is.EqualTo(originalTimeboxBefore));
		}

		[Test]
		public void NavigateToAboutViewCommand_Should_TriggerNavigationToInfoPage_When_Invoked()
		{
			// Arrange
			var nav = Mock.Of<INavigationService>();
			var version = Mock.Of<IVersionNumberProvider>();
			var device = Mock.Of<IDevice>();
			var injected = new AboutViewModel(nav, version, device);
			viewModelLocatorMock.Setup(vm => vm.GetViewModel<AboutViewModel>(null))
								.Returns(injected);

			// Act
			sut.NavigateToAboutViewCommand.Execute(null);

			// Assert
			navigationMock.Verify(m => m.PushModalAsync(NavigationPages.InfoPage, true, injected));
		}

		[Test]
		public void SaveRetroPlanToDiskCommandExecute_Should_UseCacheToPersistCurrentPlan_When_PlanDoesNotChange()
		{
			// Arrange
			var planToSave = new RetrospectivePlan
			{
				SetTheStage = TestData.FirstSetTheStageActivityWithTimebox,
				GatherData = TestData.FirstGatherDataActivityWithTimebox,
				GenerateInsights = TestData.FirstGenerateInsightsActivityWithTimebox,
				DecideWhatToDo = TestData.FirstDecideWhatToDoActivityWithTimebox,
				CloseTheRetrospective = TestData.FirstCloseTheRetrospectiveActivityWithTimebox
			};
			retroInteractorMock
				.Setup(m => m.GenerateRetrospectivePlan())
				.ReturnsAsync(planToSave);
			retroInteractorMock
				.Setup(ssm => ssm.ReadAsync())
				.ThrowsAsync(new StateStorageException("test", null));
			sut.LoadRetrospectiveCommand.Execute(null);

			// Act
			sut.SaveRetroPlanToDiskCommand.Execute(null);

			// Assert
			retroInteractorMock.Verify(cm => 
				cm.SaveAsync(It.Is<RetroPlanState>(s => s.ToRetrospectivePlan().Equals(planToSave))));
		}

		[Test]
		public void SaveRetroPlanToDiskCommandExecute_Should_UseCacheToPersistCurrentPlan_When_EveryActivityWasChanged()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			sut.LoadRetrospectiveCommand.Execute(null);

			var expectedPlan = new RetrospectivePlan
			{
				SetTheStage = TestData.SecondSetTheStageActivityWithTimebox,
				GatherData = TestData.SecondGatherDataActivityWithTimebox,
				GenerateInsights = TestData.SecondGenerateInsightsActivityWithTimebox,
				DecideWhatToDo = TestData.SecondDecideWhatToDoActivityWithTimebox,
				CloseTheRetrospective = TestData.SecondCloseTheRetrospectiveActivityWithTimebox
			};

			sut.UpdateRetroPlanWith(expectedPlan.SetTheStage.Activity);
			sut.CurrentSetTheStageActivity.OriginalTimebox = expectedPlan.SetTheStage.Timebox;
			sut.UpdateRetroPlanWith(expectedPlan.GatherData.Activity);
			sut.CurrentGatherDataActivity.OriginalTimebox = expectedPlan.GatherData.Timebox;
			sut.UpdateRetroPlanWith(expectedPlan.GenerateInsights.Activity);
			sut.CurrentGenerateInsightsActivity.OriginalTimebox = expectedPlan.GenerateInsights.Timebox;
			sut.UpdateRetroPlanWith(expectedPlan.DecideWhatToDo.Activity);
			sut.CurrentDecideWhatToDoActivity.OriginalTimebox = expectedPlan.DecideWhatToDo.Timebox;
			sut.UpdateRetroPlanWith(expectedPlan.CloseTheRetrospective.Activity);
			sut.CurrentCloseTheRetrospectiveActivity.OriginalTimebox = expectedPlan.CloseTheRetrospective.Timebox;

			// Act
			sut.SaveRetroPlanToDiskCommand.Execute(null);

			// Assert
			retroInteractorMock.Verify(cm => 
				cm.SaveAsync(It.Is<RetroPlanState>(s => s.ToRetrospectivePlan().Equals(expectedPlan))));
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_GenerateNewRetrospectivePlan_When_CacheIsEmpty()
		{
			// Arrange
			SetupRetroInteractorMockWithFirstTestActivityCollection();
			retroInteractorMock
				.Setup(cm => cm.ReadAsync())
				.ThrowsAsync(new StateStorageException("test", null));

			// Act
			sut.LoadRetrospectiveCommand.Execute(null);

			// Assert
			retroInteractorMock.Verify();
		}

		[Test]
		public void LoadRetrospectiveCommand_Should_RestoreSavedPlan_When_PlanStoredInCache()
		{
			// Arrange
			var testPlan = new RetrospectivePlan
			{
				SetTheStage = TestData.SecondSetTheStageActivityWithTimebox,
				GatherData = TestData.SecondGatherDataActivityWithTimebox,
				GenerateInsights = TestData.SecondGenerateInsightsActivityWithTimebox,
				DecideWhatToDo = TestData.SecondDecideWhatToDoActivityWithTimebox,
				CloseTheRetrospective = TestData.SecondCloseTheRetrospectiveActivityWithTimebox
			};
			var testStyles = new List<VisualStyle>();
			var testState = new RetroPlanState(testPlan, testStyles);
			retroInteractorMock.Reset();
			retroInteractorMock
				.Setup(cm => cm.ReadAsync())
				.ReturnsAsync(testState)
				.Verifiable();

			// Act
			sut.LoadRetrospectiveCommand.Execute(null);

			// Assert
			retroInteractorMock.Verify(rgm => rgm.GenerateRetrospectivePlan(), Times.Never());
			retroInteractorMock.VerifyAll();
			AssertActivityViewModelHasActivity(sut.CurrentSetTheStageActivity, testPlan.SetTheStage.Activity);
			Assert.That(sut.CurrentSetTheStageActivity.OriginalTimebox, Is.EqualTo(testPlan.SetTheStage.Timebox));
			AssertActivityViewModelHasActivity(sut.CurrentGatherDataActivity, testPlan.GatherData.Activity);
			Assert.That(sut.CurrentGatherDataActivity.OriginalTimebox, Is.EqualTo(testPlan.GatherData.Timebox));
			AssertActivityViewModelHasActivity(sut.CurrentGenerateInsightsActivity, testPlan.GenerateInsights.Activity);
			Assert.That(sut.CurrentGenerateInsightsActivity.OriginalTimebox, Is.EqualTo(testPlan.GenerateInsights.Timebox));
			AssertActivityViewModelHasActivity(sut.CurrentDecideWhatToDoActivity, testPlan.DecideWhatToDo.Activity);
			Assert.That(sut.CurrentDecideWhatToDoActivity.OriginalTimebox, Is.EqualTo(testPlan.DecideWhatToDo.Timebox));
			AssertActivityViewModelHasActivity(sut.CurrentCloseTheRetrospectiveActivity, testPlan.CloseTheRetrospective.Activity);
			Assert.That(sut.CurrentCloseTheRetrospectiveActivity.OriginalTimebox, Is.EqualTo(testPlan.CloseTheRetrospective.Timebox));
		}

        [Test]
        public void OnRetrospectivePlanOutdated_Should_MergeCurrentRetroPlan()
        {
            // Arrange
	        var expectedPlan = TestData.CreateTestRetroPlanState().ToRetrospectivePlan();
	        expectedPlan.SetTheStage.Activity.Name = "onetimeonlysetthestage";
	        
	        Func<RetrospectivePlan, RetrospectivePlan> merger = plan => expectedPlan;
	        var args = new RetrospectivePlanOutdatedEventArgs(merger);

	        // Act
            retroInteractorMock.Raise(ri => ri.RetrospectivePlanOutdated += null, args);

            // Assert
            Assert.That(sut.CurrentSetTheStageActivity.Name, Is.EqualTo(expectedPlan.SetTheStage.Activity.Name));
        }

		void SetupRetroInteractorMockWithFirstTestActivityCollection()
		{
			retroInteractorMock
				.Setup(m => m.GenerateRetrospectivePlan())
				.ReturnsAsync(new RetrospectivePlan
				{
					SetTheStage = TestData.FirstSetTheStageActivityWithTimebox,
					GatherData = TestData.FirstGatherDataActivityWithTimebox,
					GenerateInsights = TestData.FirstGenerateInsightsActivityWithTimebox,
					DecideWhatToDo = TestData.FirstDecideWhatToDoActivityWithTimebox,
					CloseTheRetrospective = TestData.FirstCloseTheRetrospectiveActivityWithTimebox
				})
				.Verifiable("Should get first activities from retro interactor.");
		}

		void SetupRetroInteractorMockWithSecondTestActivityCollection()
		{
			retroInteractorMock
				.Setup(m => m.GenerateRetrospectivePlan())
				.ReturnsAsync(new RetrospectivePlan
				{
					SetTheStage = TestData.SecondSetTheStageActivityWithTimebox,
					GatherData = TestData.SecondGatherDataActivityWithTimebox,
					GenerateInsights = TestData.SecondGenerateInsightsActivityWithTimebox,
					DecideWhatToDo = TestData.SecondDecideWhatToDoActivityWithTimebox,
					CloseTheRetrospective = TestData.SecondCloseTheRetrospectiveActivityWithTimebox
				})
				.Verifiable("Should get second activities from retro interactor.");
		}

		static bool CheckCollectionEquality(IEnumerable<IFocusNavigatable> a, IEnumerable<IFocusNavigatable> b)
		{
			var aExceptB = a.Except(b);
			var bExceptA = b.Except(a);

			return !aExceptB.Any() || !bExceptA.Any();
		}

		static IEnumerable<VisualStyle> GetCurrentStylesFrom(RetroPlanViewModel retroPlanViewModel)
		{
			return GetCurrentsFrom(retroPlanViewModel).Select(a => a.CurrentVisualStyle);
		}

		static IEnumerable<ActivityFullViewModel> GetCurrentsFrom(RetroPlanViewModel retroPlanViewModel)
		{
			return new List<ActivityFullViewModel>
			{
				retroPlanViewModel.CurrentSetTheStageActivity,
			   	retroPlanViewModel.CurrentGatherDataActivity,
			   	retroPlanViewModel.CurrentGenerateInsightsActivity,
			   	retroPlanViewModel.CurrentDecideWhatToDoActivity,
			   retroPlanViewModel.CurrentCloseTheRetrospectiveActivity
			};
		}

		static void AssertActivityViewModelHasActivity(ActivityFullViewModel viewModel, Activity activity)
		{
			Assert.That(viewModel.Id, Is.EqualTo(activity.ID));
			Assert.That(viewModel.Phase, Is.EqualTo(activity.Phase));
			Assert.That(viewModel.Name, Is.EqualTo(activity.Name));
		}
	}
}
