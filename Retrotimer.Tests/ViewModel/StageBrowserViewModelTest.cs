﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using Retrotimer.Model;
using Retrotimer.Services.Analytics;
using Retrotimer.Services.Data;
using Retrotimer.ViewModel;

namespace ViewModel
{
    [TestFixture]
    public class StageBrowserViewModelTest
    {
        static readonly Activity FirstSetTheStageTestData = new Activity(14, RetroPhase.SetTheStage, "Name set stage");
        static readonly Activity FirstGatherDataTestData = new Activity(1, RetroPhase.GatherData, "Name gather data");
        static readonly Activity FirstGenerateInsightsTestData = new Activity(2, RetroPhase.GenerateInsights, "Name generate insights");
        static readonly Activity FirstDecideWhatToDoTestData = new Activity(3, RetroPhase.DecideWhatToDo, "Name decide what do to");
        static readonly Activity FirstCloseTheRetrospectiveTestData = new Activity(4, RetroPhase.CloseTheRetrospective, "Name close the retrospective");
        static readonly Activity SecondSetTheStageTestData = new Activity(140, RetroPhase.SetTheStage, "Name set stage");
        static readonly Activity SecondGatherDataTestData = new Activity(10, RetroPhase.GatherData, "Name gather data");
        static readonly Activity SecondGenerateInsightsTestData = new Activity(20, RetroPhase.GenerateInsights, "Name generate insights");
        static readonly Activity SecondDecideWhatToDoTestData = new Activity(30, RetroPhase.DecideWhatToDo, "Name decide what do to");
        static readonly Activity SecondCloseTheRetrospectiveTestData = new Activity(40, RetroPhase.CloseTheRetrospective, "Name close the retrospective");

        private static readonly List<Activity> TestData = new List<Activity>
        {
            FirstSetTheStageTestData, FirstGatherDataTestData, FirstGenerateInsightsTestData,
            FirstDecideWhatToDoTestData, FirstCloseTheRetrospectiveTestData,
            SecondSetTheStageTestData, SecondGatherDataTestData, SecondGenerateInsightsTestData,
            SecondDecideWhatToDoTestData, SecondCloseTheRetrospectiveTestData
        };

        Mock<IDataService> dataSvcMock;
        Mock<IVisualStyleGenerator> styleGenMock;
        Mock<IViewModelLocator> viewModelLocatorMock;

        StageBrowserViewModel sut;
        private Mock<IAnalytics> analyticsMock;

        [SetUp]
        public void Setup()
        {
            analyticsMock = new Mock<IAnalytics>();
            Analytics.OverrideDefaultInstance(analyticsMock.Object);
            
            dataSvcMock = SetupDataServiceMockWithCompleteTestData();
            styleGenMock = SetupStyleGeneratorMock();
            viewModelLocatorMock = SetupViewModelLocatorMock();
            
            sut = new StageBrowserViewModel(dataSvcMock.Object, viewModelLocatorMock.Object, styleGenMock.Object);
        }

        [TearDown]
        public void Teardown()
        {
            Analytics.Reset();
        }

        [Test]
        public void Ctor_Should_ThrowArgumentNullException_When_DataServiceIsNull()
        {
            // Act
            TestDelegate test = () => new StageBrowserViewModel(null, null, styleGenMock.Object);

            // Assert
            Assert.That(test, Throws.ArgumentNullException.With.Property(
                nameof(ArgumentNullException.ParamName)).EqualTo(
                    "dataService"));
        }

        [Test]
        public void Ctor_Should_ThrowArgumentNullException_When_VisualStyleGeneratorIsNull()
        {
            // Act
            TestDelegate test = () => new StageBrowserViewModel(dataSvcMock.Object, null, null);

            // Assert
            Assert.That(test, Throws.ArgumentNullException.With.Property(
                nameof(ArgumentNullException.ParamName)).EqualTo(
                    "styleGenerator"));
        }

        [Test]
        public void Ctor_Should_ThrowArgumentNullException_When_ViewModelLocatorIsNull()
        {
            // Act
            TestDelegate test = () => new StageBrowserViewModel(dataSvcMock.Object, null, styleGenMock.Object);
            
            // Assert
            Assert.That(test, Throws.ArgumentNullException.With.Property(
                nameof(ArgumentNullException.ParamName)).EqualTo(
                "viewModelLocator"));
        }

        [Test]
        public void Activities_Should_HaveStylesRandomlyGenerated()
        {
            // Arrange
            var testActivities = new List<Activity> {FirstSetTheStageTestData, SecondSetTheStageTestData};
            dataSvcMock.Reset();
            dataSvcMock.Setup(ds => ds.ReadActivitiesAsync()).ReturnsAsync(testActivities);
            
            var expectedStyles = new List<VisualStyle>
            {
                VisualStyle.Blue,
                VisualStyle.DarkOrange
            };
            styleGenMock.Setup(smg => smg.GenerateVisualStylesWithoutIdenticalNeighbours(2))
                        .Returns(expectedStyles);

            // Act
            sut.LoadActivitiesCommand.Execute(null);
            var actual = sut.ActivitiesInPhase.Select(a => a.CurrentVisualStyle);

            // Assert
            Assert.That(actual, Is.EquivalentTo(expectedStyles));
        }

        [Test]
        public void SelectedActivity_Should_ReturnNull_When_InstanceCreated()
        {
            // Arrange

            // Act
            var actual = sut.SelectedActivity;

            // Assert
            Assert.That(actual, Is.Null);
        }

        [Test]
        public void SelectedActivity_Should_IgnoreNull_When_ItIsAssigned()
        {
            // Arrange
            sut.LoadActivitiesCommand.Execute(null);
            sut.SelectedActivity = sut.ActivitiesInPhase.First();
            var before = sut.SelectedActivity;

            // Act
            sut.SelectedActivity = null;

            // Assert
            Assert.That(sut.SelectedActivity, Is.EqualTo(before));
        }

        /// <summary>
        /// Because of how listview is implemented in Xamarin Forms, the viewmodel needs
        /// to be ready for nulls coming in as selectedItem.
        /// From app's point of view this is an invalid state (selectedItem == null) that
        /// needs to be blocked.
        /// Blocking works by ignoring the null value (test SelectedActivity_Should_IgnoreNull_When_ItIsAssigned)
        /// and raising propertyChanged event which refreshes the binding with previous selectedItem value,
        /// which effectively blocks the possibility to have no selected item.
        /// </summary>
        /// <remarks>
        /// https://developer.xamarin.com/guides/xamarin-forms/user-interface/listview/interactivity/#Selection_Taps
        /// </remarks>
        [Test]
        public void SelectedActivity_Should_RaisePropertyChangedEvent_When_NullAssigned()
        {
            // Arrange
            sut.LoadActivitiesCommand.Execute(null);
            sut.SelectedActivity = sut.ActivitiesInPhase.First();
            var eventRaised = false;
            sut.PropertyChanged += (sender, e) =>
            {
                eventRaised |= e.PropertyName == nameof(StageBrowserViewModel.SelectedActivity);
            };

            // Act
            sut.SelectedActivity = null;

            // Assert
            Assert.That(eventRaised, Is.True);
        }

        [Test]
        public void SelectedActivity_Should_RaisePropertyChangedEvent_When_ValueChanged()
        {
            // Arrange
            sut.LoadActivitiesCommand.Execute(null);
            var oldValue = sut.ActivitiesInPhase.ElementAt(0);
            var newValue = sut.ActivitiesInPhase.ElementAt(1);
            sut.SelectedActivity = oldValue;

            bool eventRaised = false;
            sut.PropertyChanged += (sender, e) =>
            {
                eventRaised |= e.PropertyName == nameof(StageBrowserViewModel.SelectedActivity);
            };

            // Act
            sut.SelectedActivity = newValue;

            // Assert
            Assert.That(eventRaised, Is.True);
        }

        [Test]
        public void SelectedActivity_Should_RaisePropertyChangedEvent_When_CurrentValueReassigned()
        {
            // Arrange
            sut.LoadActivitiesCommand.Execute(null);
            var newValue = sut.ActivitiesInPhase.First();
            sut.SelectedActivity = newValue;

            bool eventRaised = false;
            sut.PropertyChanged += (sender, e) =>
            {
                eventRaised |= e.PropertyName == nameof(StageBrowserViewModel.SelectedActivity);
            };

            // Act
            sut.SelectedActivity = newValue;

            // Assert
            Assert.That(eventRaised, Is.True);
        }

        [Test]
        public void SelectedActivity_Should_InformOldActivityThatItIsDeselected_When_ValueChanged()
        {
            // Arrange
            sut.LoadActivitiesCommand.Execute(null);
            var oldValue = sut.ActivitiesInPhase.First();
            sut.SelectedActivity = oldValue;
            var newValue = sut.ActivitiesInPhase.Skip(1).First();

            // Act
            sut.SelectedActivity = newValue;

            // Assert
            Assert.That(oldValue.IsSelected, Is.False);
        }

        [Test]
        public void SelectedActivity_Should_InformNewActivityThatItIsSelected_When_ValueChanged()
        {
            // Arrange
            sut.LoadActivitiesCommand.Execute(null);
            var newValue = sut.ActivitiesInPhase.First();

            // Act
            var before = newValue.IsSelected;
            sut.SelectedActivity = newValue;

            // Assert
            Assert.That(before, Is.False);
            Assert.That(newValue.IsSelected, Is.True);
        }

		[Test]
		public void SelectedActivity_Should_PassSelectedActivityToRetroPlanViewModel_When_ValueChanged()
		{
            // Arrange
            sut.LoadActivitiesCommand.Execute(null);
            var newValue = sut.ActivitiesInPhase.First();
			var expectedId = newValue.Id;

		    var retroPlanVmMock = new Mock<IRetroPlanViewModel>();
		    viewModelLocatorMock.Setup(vml => vml.GetViewModel<IRetroPlanViewModel>(null))
		        .Returns(retroPlanVmMock.Object);

		    // Act
			sut.SelectedActivity = newValue;

			// Assert
			retroPlanVmMock.Verify(
			    rpv => rpv.UpdateRetroPlanWith(It.Is<Activity>(a => a.ID == expectedId)), Times.Once());
		}

		[Test]
        public void Phase_Should_OnlyStoreNewValue_When_ActivitesNotLoaded()
        {
            // Act
            var before = sut.Phase;
            sut.Phase = RetroPhase.GenerateInsights;
            var after = sut.Phase;

            // Assert
            Assert.That(before, Is.Not.EqualTo(after));
            Assert.That(after, Is.EqualTo(RetroPhase.GenerateInsights));
        }

        [Test]
        public void Phase_Should_RegenerateActivitiesInPhase_When_ValueChanged()
        {
            // Arrange
            var expectedAfterIds = TestData.Where(a => a.Phase == RetroPhase.CloseTheRetrospective)
                                           .Select(a => a.ID);

			sut.LoadActivitiesCommand.Execute(null);
            sut.Phase = RetroPhase.GatherData;

            // Act
            var before = sut.ActivitiesInPhase;
            sut.Phase = RetroPhase.CloseTheRetrospective;
            var after = sut.ActivitiesInPhase;
            var afterIds = after.Select(a => a.Id);

            // Assert
            Assert.That(before, Is.Not.EquivalentTo(after));
            Assert.That(afterIds, Is.EqualTo(expectedAfterIds));
        }

        [Test]
		[TestCase(RetroPhase.SetTheStage)]
		[TestCase(RetroPhase.GatherData)]
		[TestCase(RetroPhase.GenerateInsights)]
		[TestCase(RetroPhase.DecideWhatToDo)]
		[TestCase(RetroPhase.CloseTheRetrospective)]
        public void LoadActivitiesCommand_Should_LoadActivitiesFromDataService(RetroPhase testPhase)
        {
            // Arrange
            var expectedActivities = TestData.Where(a => a.Phase == testPhase)
                                             .Select(a => a.ID);
            sut.Phase = testPhase;

            // Act
            sut.LoadActivitiesCommand.Execute(null);
            var actualActivitiesIds = sut.ActivitiesInPhase.Select(asbvm => asbvm.Id).ToList();

            // Assert
            Assert.That(actualActivitiesIds, Is.EqualTo(expectedActivities));
        }

        [Test]
        public void LoadActivitesCommand_Should_NotUpdateActivitiesInPhase_When_ItsRunSecondTime()
        {
            // Arrange

            // Act
            sut.LoadActivitiesCommand.Execute(null);
            var first = sut.ActivitiesInPhase;
            sut.LoadActivitiesCommand.Execute(null);
            var second = sut.ActivitiesInPhase;

            // Assert
            Assert.That(second, Is.SameAs(first));
        }

        [Test]
        public void LoadActivitiesCommand_Should_ExposeEmptyActivityCollection_When_DataServiceIsEmpty()
        {
            // Arrange
            dataSvcMock.Reset();
            dataSvcMock.Setup(ds => ds.ReadActivitiesAsync()).ReturnsAsync(new List<Activity>());

            sut.Phase = RetroPhase.GatherData;

            // Act
            sut.LoadActivitiesCommand.Execute(null);

            // Assert
            Assert.That(sut.ActivitiesInPhase, Is.Empty);
        }

        [Test]
        public void LoadActivitiesCommand_Should_EnsureCorrectSelectedActivityInstance_When_ActivitiesContainIt()
        {
            // Arrange
            var vmOutsideOfActivityCollection = new ActivitySelectableBasicViewModel(FirstGatherDataTestData);

            // Act
            sut.SelectedActivity = vmOutsideOfActivityCollection;
            sut.Phase = RetroPhase.GatherData;
            sut.LoadActivitiesCommand.Execute(null);
            var actualSelected = sut.SelectedActivity;

            // When
            Assert.That(sut.ActivitiesInPhase.Contains(actualSelected));
        }

        [Test]
        public void LoadActivitiesCommand_Should_IgnoreSelectedActivity_When_ActivitiesDoNotContainIt()
        {
			// Arrange
			var vmOutsideOfActivityCollection = new ActivitySelectableBasicViewModel(FirstGatherDataTestData);
            var expectedSelected = TestData.First(a => a.Phase == RetroPhase.CloseTheRetrospective);

			// Act
			sut.SelectedActivity = vmOutsideOfActivityCollection;
            sut.Phase = RetroPhase.CloseTheRetrospective;
			sut.LoadActivitiesCommand.Execute(null);
			var actualSelected = sut.SelectedActivity;

			// When
            Assert.That(actualSelected.Model, Is.EqualTo(expectedSelected));
        }

        [Test]
        public void RefreshActivitiesCommand_Should_SetIsRefreshingActivitiesProprety()
        {
            // Arrange
            var actualBeforeIsRefeshingActivities = false;
            dataSvcMock.Setup(ds => ds.RefreshActivitiesAsync())
                .Callback(() => actualBeforeIsRefeshingActivities = sut.IsRefreshingActivities)
                .ReturnsAsync(TestData);
            
            // Act
            sut.RefreshActivitiesCommand.Execute(null);
            var actualAfterIsRefreshingActivities = sut.IsRefreshingActivities;
            
            // Assert
            Assert.That(actualBeforeIsRefeshingActivities, Is.True, 
                nameof(sut.IsRefreshingActivities) + " should be true during sync");
            Assert.That(actualAfterIsRefreshingActivities, Is.False, 
                nameof(sut.IsRefreshingActivities) + " should be false when sync is finished");
        }

        [Test]
        [TestCase(RetroPhase.SetTheStage)]
        [TestCase(RetroPhase.GatherData)]
        [TestCase(RetroPhase.GenerateInsights)]
        [TestCase(RetroPhase.DecideWhatToDo)]
        [TestCase(RetroPhase.CloseTheRetrospective)]
        public void RefreshActivitiesCommand_Should_UpdateActivitiesWithDownloadedDataset(RetroPhase setTheStage)
        {
            // Arrange
            var newData = TestData.Select(a => new Activity(a.ID + 1000, a.Phase, a.Name + "__new__"));
            var expectedActivities = newData.Where(a => a.Phase == setTheStage);
            dataSvcMock.Setup(ds => ds.RefreshActivitiesAsync()).ReturnsAsync(expectedActivities);
            
            // Act
            sut.LoadActivitiesCommand.Execute(null);
            sut.Phase = setTheStage;
            sut.RefreshActivitiesCommand.Execute(null);
            var actualActivities = sut.ActivitiesInPhase.Select(asbvm => asbvm.Model);

            // Assert
            Assert.That(actualActivities, Is.EqualTo(expectedActivities));
        }
        
        [Test]
        public void RefreshActivitiesCommand_Should_EnsureCorrectSelectedActivityInstance_When_ActivitiesContainIt()
        {
            // Arrange
            const RetroPhase testPhase = RetroPhase.GatherData;
            var newData = TestData.Select(a => new Activity(a.ID + 1000, a.Phase, a.Name + "__new__")).ToList();
            var gatherDataActivity = newData.First(a => a.Phase == testPhase);
            dataSvcMock.Setup(ds => ds.RefreshActivitiesAsync()).ReturnsAsync(newData);
            var vmOutsideOfActivityCollection = new ActivitySelectableBasicViewModel(gatherDataActivity);

            // Act
            sut.LoadActivitiesCommand.Execute(null);
            sut.SelectedActivity = vmOutsideOfActivityCollection;
            sut.Phase = testPhase;
            sut.RefreshActivitiesCommand.Execute(null);
            var actualSelected = sut.SelectedActivity;

            // When
            Assert.That(sut.ActivitiesInPhase.Contains(actualSelected));
        }

        [Test]
        public void RefreshActivitiesCommand_Should_SelectFirstAvailableActivity_When_CurrentlySelectedNotInDataset()
        {
            // Arrange
            var newData = TestData.Select(a => new Activity(a.ID + 1000, a.Phase, a.Name + "__new__")).ToList();
            var gatherDataActivity = newData.First(a => a.Phase == RetroPhase.GatherData);
            dataSvcMock.Setup(ds => ds.RefreshActivitiesAsync()).ReturnsAsync(newData);
            var vmOutsideOfActivityCollection = new ActivitySelectableBasicViewModel(gatherDataActivity);
            var expectedSelected = newData.First(a => a.Phase == RetroPhase.CloseTheRetrospective);

            // Act
            sut.LoadActivitiesCommand.Execute(null);
            sut.SelectedActivity = vmOutsideOfActivityCollection;
            sut.Phase = RetroPhase.CloseTheRetrospective;
            sut.RefreshActivitiesCommand.Execute(null);
            var actualSelected = sut.SelectedActivity;

            // When
            Assert.That(actualSelected.Model, Is.EqualTo(expectedSelected));
        }
         
        [Test]
        public void RefreshActivitiesCommand_Should_GracefullyHandleErrors()
        {
            // Arrange
            const string expectedMessage = "Test message for analytics";
            var testException = new InvalidOperationException(expectedMessage);
            var expectedError = testException.GetType().ToString();
            dataSvcMock.Setup(ds => ds.RefreshActivitiesAsync()).ThrowsAsync(testException);
            
            // Act
            sut.RefreshActivitiesCommand.Execute(null);
            
            // Assert
            Assert.That(sut.IsRefreshingActivities, Is.False);
            Func<ActivityRefreshFailEvent, bool> assertLogEvent = 
                e => e.ErrorType == expectedError && e.Message == expectedMessage;
            analyticsMock.Verify(a => a.LogEvent(
                It.Is<ActivityRefreshFailEvent>(e => assertLogEvent(e))));
        }

        #region Helper methods

        static Mock<IDataService> SetupDataServiceMockWithCompleteTestData()
        {
            var dataServiceMock = new Mock<IDataService>();
            dataServiceMock.Setup(m => m.ReadActivitiesAsync()).ReturnsAsync(TestData);

            return dataServiceMock;
        }

        static Mock<IVisualStyleGenerator> SetupStyleGeneratorMock()
        {
            var styleGenMock = new Mock<IVisualStyleGenerator>();
            styleGenMock
                .Setup(sgm => sgm.GenerateVisualStylesWithoutIdenticalNeighbours(
                    It.IsInRange<uint>(0, uint.MaxValue, Range.Inclusive)))
                .Returns((uint arg) =>
                {
                    var styles = new List<VisualStyle>();
                    for (uint i = 0; i < arg; i++)
                        styles.Add(VisualStyle.Blue);

                    return styles;
                });

            return styleGenMock;
        }
        
        private Mock<IViewModelLocator> SetupViewModelLocatorMock()
        {
            var locatorMock = new Mock<IViewModelLocator>();
            var retroPlanVmMock = Mock.Of<IRetroPlanViewModel>();
            locatorMock.Setup(l => l.GetViewModel<IRetroPlanViewModel>(null)).Returns(retroPlanVmMock);
            
            return locatorMock;
        }

        #endregion

    }
}
