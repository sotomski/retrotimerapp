﻿using System.Diagnostics;
using Moq;
using NUnit.Framework;
using Retrotimer;
using Retrotimer.ViewModel;

namespace ViewModel
{
    [TestFixture]
    public class StartViewModelTest
    {
        StubDataService dataService;
        Mock<IViewModelLocator> viewModelLocator;
        Mock<INavigationService> navigationSvc;
        StartViewModel sut;

        [SetUp]
        public void Setup()
        {
			ConsoleTraceListener c = new ConsoleTraceListener(true);
			Trace.Listeners.Add(c);
            
            dataService = new StubDataService();
            viewModelLocator = new Mock<IViewModelLocator>();
            navigationSvc = new Mock<INavigationService>();

            sut = new StartViewModel(dataService, navigationSvc.Object, viewModelLocator.Object);
        }

        [Test]
        public void TestInitialValuesAfterCreation()
        {
            // Act
            var localSut = new StartViewModel(null, null, null);

            // Assert
            Assert.That(localSut.CurrentVisualStyle, Is.EqualTo(VisualStyle.DarkBlue));
            Assert.That(localSut.IsRetryAvailable, Is.False);
            Assert.That(localSut.IsSynchronizing, Is.False);
            Assert.That(localSut.Message, Is.EqualTo(string.Empty));
        }

        [Test]
        public void IsSynchronizing_Should_BeTrue_When_SyncStarted()
        {
			// Act
			sut.SynchronizeCommand.Execute(null);
            var actualIsSyncing = sut.IsSynchronizing;
            var actualMessage = sut.Message;
            dataService.ShouldFinishRefresh = true;

			// Assert
            Assert.That(actualIsSyncing, Is.True);
            Assert.That(actualMessage, Is.EqualTo("Synchronizing with the Retromat website..."));
        }

        [Test]
        public void SycnhronizeCommand_Should_RefreshDataInDataService()
        {
			// Arrange
			dataService.ShouldFinishRefresh = true;

            // Act
            sut.SynchronizeCommand.Execute(null);

            // Assert
            Assert.That(dataService.DidStartRefresh, Is.True);
        }

        [Test]
        public void TestSynchronizationSuccess()
        {
            // Arrange
            dataService.ShouldRefreshSynchronously = true;
			dataService.ShouldFinishRefresh = true;
            var retroPlanVm = Mock.Of<IRetroPlanViewModel>();
            viewModelLocator.Setup(vml => vml.GetViewModel<IRetroPlanViewModel>(null))
                            .Returns(retroPlanVm);

            // Act
            sut.SynchronizeCommand.Execute(null);

            // Assert
            Assert.That(sut.IsSynchronizing, Is.False);
            Assert.That(sut.Message, Is.EqualTo("Synchronizing with the Retromat website..."));
            Assert.That(sut.IsRetryAvailable, Is.False);
            navigationSvc.Verify(ns => ns.PushAsync(NavigationPages.RetroPlanPage, false, retroPlanVm));
        }

        [Test]
        public void TestSynchronizationFailure()
        {
            // Arrange
            dataService.ShouldRefreshSynchronously = true;
            dataService.ShouldRefreshThrowException = true;

            // Act
            sut.SynchronizeCommand.Execute(null);

			// Assert
			Assert.That(sut.IsSynchronizing, Is.False);
			Assert.That(sut.Message, Is.EqualTo("Synchronization failed. Please try again later."));
            Assert.That(sut.IsRetryAvailable, Is.True);
        }
    }
}
