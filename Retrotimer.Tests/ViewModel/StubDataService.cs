﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Retrotimer;
using Retrotimer.Model;
using Retrotimer.Services.Data;

namespace ViewModel
{
    public class StubDataService : IDataService
    {
        public bool ShouldFinishRefresh;
        public bool ShouldRefreshSynchronously;
        public bool ShouldRefreshThrowException;
        public bool DidStartRefresh;

        public event EventHandler<EventArgs<IEnumerable<Activity>>> NewActivitiesAvailable;

        public Task<IEnumerable<Activity>> ReadActivitiesAsync()
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<Activity>> IDataService.RefreshActivitiesAsync()
        {
            DidStartRefresh = true;

            var task = new Task<IEnumerable<Activity>>(() =>
            {
                if (ShouldRefreshThrowException)
                {
                    throw new Exception();
                }

                var counter = 10;
                while (!ShouldFinishRefresh && --counter > 0)
                {
                    Thread.Sleep(100);
                }

                IEnumerable<Activity> data = new List<Activity>();

                return data;
            });

            if (ShouldRefreshSynchronously)
            {
                task.RunSynchronously();
            }
            else
            {
                task.Start();
            }

            return task;
        }
    }
}