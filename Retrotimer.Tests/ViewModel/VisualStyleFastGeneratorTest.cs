﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using Retrotimer.Services;
using Retrotimer.ViewModel;

namespace ViewModel
{
    [TestFixture]
    public class VisualStyleFastGeneratorTest
    {
        static readonly VisualStyle[] Styles = {
				VisualStyle.Blue, VisualStyle.DarkOrange, VisualStyle.LightBlue, VisualStyle.DarkBlue,
				VisualStyle.LightOrange, VisualStyle.LightBlue, VisualStyle.DarkOrange, VisualStyle.Blue,
				VisualStyle.LightBlue, VisualStyle.LightOrange };

        Mock<IRandomGenerator> mockGenerator;
        VisualStyleFastGenerator sut;

        [SetUp]
        public void SetUp()
        {
            mockGenerator = new Mock<IRandomGenerator>();
            sut = new VisualStyleFastGenerator(mockGenerator.Object);
        }

        [Test]
        [TestCaseSource(typeof(SmallCountsData), "TestCases")]
        public void Generate_Should_ReturnPredefinedCollectionOfStyles_When_CountLessThanOrEqual10(uint count)
        {
            // Arrange
            var expectedStyles = Styles.Take((int)count);

            // Act
            var result = sut.GenerateVisualStylesWithoutIdenticalNeighbours(count);

            // Assert
            Assert.That(result, Is.EqualTo(expectedStyles));
        }

        [Test]
        public void Generate_Should_WrapTheStyleList_When_CountLargerThan10()
        {
            // Arrange
            var count = 14u;
            var wrappedStyles = Styles.Concat(Styles).ToArray();
            var expectedStyles = wrappedStyles.Take((int)count);

            // Act
            var result = sut.GenerateVisualStylesWithoutIdenticalNeighbours(count);

            // Assert
            Assert.That(result, Is.EqualTo(expectedStyles));
        }

        [Test]
        public void Generate_Should_RandomlyOffsetPredefinedStyles()
        {
            // Arrange
            var wrappedStyles = Styles.Concat(Styles).ToArray();
            var offset = 3;
            var count = 4u;
            var expectedStyles = wrappedStyles.Skip(offset).Take((int)count);
            mockGenerator.Setup(g => g.GetInt(0, 10)).Returns(offset);

            // Act
            var result = sut.GenerateVisualStylesWithoutIdenticalNeighbours(count);

            // Assert
            Assert.That(result, Is.EqualTo(expectedStyles));
        }

        class SmallCountsData
        {
            public static IEnumerable<uint> TestCases
            {
                get
                {
                    for (uint i = 0; i <= 10; i++)
                    {
                        yield return i;
                    }
                }
            }
        }
    }
}
