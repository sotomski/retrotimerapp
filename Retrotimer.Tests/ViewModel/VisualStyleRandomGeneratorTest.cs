﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using Retrotimer.Services;
using Retrotimer.ViewModel;

namespace ViewModel
{
	[TestFixture]
	public class VisualStyleRandomGeneratorTest
	{
		Mock<IRandomGenerator> randMock;
		VisualStyleRandomGenerator sut;

		[SetUp]
		public void Setup()
		{
			randMock = new Mock<IRandomGenerator>();
			sut = new VisualStyleRandomGenerator(randMock.Object);
		}

		[TearDown]
		public void Teardown()
		{
			sut = null;
			randMock = null;
		}

		[Test]
		public void Ctor_Should_ThrowArgumentNullException_When_RandIsNull()
		{
			// Act
			TestDelegate test = () => new VisualStyleRandomGenerator(null);

			// Assert
			Assert.That(test, Throws.ArgumentNullException);
		}

		[Test]
		public void Ctor_Should_CreateInstance_When_RandNotNull()
		{
			// Act
			var localSut = new VisualStyleRandomGenerator(randMock.Object);

			// Assert
			Assert.That(localSut, Is.Not.Null);
		}

		[Test]
		public void GenerateVisualStyles_Should_ReturnEmptyCollection_When_ArgumentIsZero()
		{
			// Act
			var result = sut.GenerateVisualStylesWithoutIdenticalNeighbours(0);

			// Assert
			Assert.That(result.Count(), Is.EqualTo(0));
		}

		[Test]
		public void GenerateVisualStyles_Should_GenerateTwoDifferentStyles_When_ArgumentIsTwo()
		{
			// Act
			var result = sut.GenerateVisualStylesWithoutIdenticalNeighbours(2);

			// Assert
			Assert.That(result.ElementAt(0), Is.Not.EqualTo(result.ElementAt(1)));
		}

		[Test]
		public void GenerateVisualStyles_Should_GenerateCollectionWithDifferentAdjacentStyles_When_ArgumentPositive()
		{
			// Arrange
			var expectedStyles = new List<VisualStyle>
			{
				VisualStyle.LightOrange,
				VisualStyle.DarkOrange,
				VisualStyle.LightOrange,
				VisualStyle.DarkOrange,
				VisualStyle.LightOrange,
				VisualStyle.DarkOrange,
				VisualStyle.LightOrange,
				VisualStyle.DarkOrange
			};

			// Act
			var result = sut.GenerateVisualStylesWithoutIdenticalNeighbours(8);

			// Assert
			Assert.That(result.Count(), Is.EqualTo(expectedStyles.Count));
			Assert.That(result, Is.EquivalentTo(expectedStyles));
		}
	}
}

