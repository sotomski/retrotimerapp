﻿using System.IO;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using PCLStorage;
using Retrotimer.Common;
using Retrotimer.Data;
using Retrotimer.Data.State;
using Retrotimer.Model;
using Retrotimer.Services;
using Retrotimer.Services.Analytics;
using Retrotimer.Services.Data;
using Retrotimer.Services.Settings;
using Retrotimer.Services.Timer;
using Retrotimer.View;
using Retrotimer.ViewModel;
using Xamarin.Forms;

namespace Retrotimer
{
    public partial class App : Application
    {
        const string RetroPlanSavedStateFilename = "RetrospectivePlanState.xml";

        readonly IDependencyContainer dependencies;
        readonly IAudioPlayer player;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Retrotimer.App"/> class.
        /// Exists only to use design time XAML preview.
        /// </summary>
        public App()
        {
            InitializeComponent();
        }

        public App(IDependencyContainer container)
        {
            Analytics.OverrideDefaultInstance(new MobileCenterAnalytics());
            
            dependencies = container;

            InitializeComponent();

            // Dependency registration
            RegisterDependenciesIn(dependencies);

            player = dependencies.Resolve<IAudioPlayer>();
        }

        static string GetFullStoragePath(string storageFilename, IFileSystem fileSystem)
        {
            return Path.Combine(fileSystem.LocalStorage.Path, storageFilename);
        }

        void RegisterDependenciesIn(IDependencyContainer container)
        {
            container.Register<IDevice, FormsDevice>();

            container.Register<ISettings, XamarinSettings>();
            container.Register<IStorage, Storage>();
            container.Register<IFileSystem>(() => container.Resolve<IStorage>().FileSystem);
            container.Register<IAppStateManager>(() =>
            {
                var fileSystem = container.Resolve<IFileSystem>();
                string cachePath = GetFullStoragePath(RetroPlanSavedStateFilename, fileSystem);

                return new XmlAppStateManager(cachePath, fileSystem);
            });

            // Trial of JSON data service
            container.Register<ISerializer, JsonSerializer>();
            container.Register<IDataRepository, LocalFileDataRepository>();
            container.Register<IDataSource, HttpDataSource>();
            container.Register<IDataService, DataService>();

            container.Register<IRandomGenerator, RandomGenerator>();

            container.Register<IVisualStyleGenerator, VisualStyleFastGenerator>();
            container.Register<IRetrospectiveInteractor, RetrospectiveInteractor>();
            container.Register<ICountdownTimerProvider, FormsCountdownTimerProvider>();
            container.Register<IRetrospectivePlayerProvider, RetrospectivePlayerProvider>();
            container.Register<IFocusNavigationServiceProvider, FocusNavigationServiceProvider>();

            //	// View models
            container.Register<IRetroPlanViewModel, RetroPlanViewModel>();
            container.Register<IStageBrowserViewModel, StageBrowserViewModel>();
            container.Register<AboutViewModel, AboutViewModel>();
            container.Register<StartViewModel>();
			container.Register<IViewModelLocator>(() => new ViewModelLocator(container));

            //	// Views
            container.Register<RetroPlanView, RetroPlanView>();
            container.Register<StartView, StartView>();

            //// Navigation is necessary for navigation service
            //// The following two lines break cycle dependency on navigation
            /// // HACK HACK HACK: Clean up dependencies, so that MainPage does not have to be shoveled here like that.
            var main = container.Resolve<StartView>();
            MainPage = new NavigationPage(main);

            //// Navigation
            container.Register<Application>(() => this);
            container.Register<INavigation>(() => MainPage.Navigation);
            container.Register<INavigationService, NavigationService>();

            main.BindingContext = dependencies.Resolve<StartViewModel>();
        }

        void StartListeningForNotifications()
        {
            Messenger.Default.Register<NotificationMessage>(this, (msg) =>
            {
                switch (msg.Notification)
                {
                    case MessageCommunicationProtocol.CountdownStageChanged:
                        player.PlaySoundAsync(Sounds.Chime);
                        break;

                    case MessageCommunicationProtocol.CountdownCompleted:
                        player.PlaySoundAsync(Sounds.DoubleChime);
                        break;

                }
            });
        }

        void StopListeningForNotifications()
        {
            Messenger.Default.Unregister(this);
        }

#pragma warning disable RECS0165 // Asynchronous methods should return a Task instead of void
        protected override async void OnStart()
        {
            var settings = dependencies.Resolve<ISettings>();
            await StartAnalyticsAsync(settings);

            Messenger.Default.Send(new NotificationMessage(MessageCommunicationProtocol.AppDidStart));
            StartListeningForNotifications();

            settings.IsCleanAppStart = false;
        }

        async Task StartAnalyticsAsync(ISettings settings)
        {
            if (settings.IsCleanAppStart)
            {
                var dialogSvc = dependencies.Resolve<IDialogService>();
                var result = await dialogSvc.ShowDialogAsync(
                    "To fix bugs as fast as possible, I need some diagnostic data from the app.\n"
                    + "I very much respect you privacy, so it's all anonymous and voluntary.\n"
                    + "I'd really appreciate your help, though!"
                    + System.Environment.NewLine
                    + System.Environment.NewLine
                    + "PS. You can change this later in system settings.",
                "App diagnostics",
                "Sure, I'll help",
                "Nope");

                settings.IsAnalyticsAllowed = result;
            }

            Analytics.Instance.IsEnabled = settings.IsAnalyticsAllowed;
        }
#pragma warning restore RECS0165 // Asynchronous methods should return a Task instead of void

        protected override void OnSleep()
        {
            Messenger.Default.Send(new NotificationMessage(MessageCommunicationProtocol.AppWillSleep));
            StopListeningForNotifications();
        }

        protected override void OnResume()
        {
            Messenger.Default.Send(new NotificationMessage(MessageCommunicationProtocol.AppDidResume));
            StartListeningForNotifications();
        }
    }
}
