﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Retrotimer.Common
{
	public class Cursor<T>
	{
		const int INDEX_BEFORE_FIRST = -1;
		IEnumerable<T> enumerable;
		int currIndex;

		public Cursor(IEnumerable<T> enumerable)
		{
            this.enumerable = enumerable ?? throw new ArgumentNullException(nameof(enumerable));
			currIndex = INDEX_BEFORE_FIRST;
		}

		public T Current
		{
			get
			{
				return enumerable.ElementAtOrDefault(currIndex);
			}
		}

		public bool MoveNext()
		{
			currIndex++;
			return currIndex < enumerable.Count();
		}

		public bool MovePrevious()
		{
			currIndex--;
			return 0 <= currIndex;
		}

		public void Reset()
		{
			currIndex = INDEX_BEFORE_FIRST;
		}
	}

	/// <summary>
	/// Extension method for generic List allowing to easily create a cursor.
	/// </summary>
	public static class CursorExtension
	{
		public static Cursor<T> GetCursor<T>(this List<T> list)
		{
			return new Cursor<T>(list);
		}
	}
}
