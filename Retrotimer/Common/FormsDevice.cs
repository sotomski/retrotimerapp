﻿using System;
using Xamarin.Forms;

namespace Retrotimer.Common
{
    /// <summary>
    /// Cross platform implementation of <c>IDevice</c> based on Xamarin Forms.
    /// </summary>
    public class FormsDevice : IDevice
    {
        public void LaunchUriAsync(Uri uriToOpen)
        {
            Device.OpenUri(uriToOpen);
        }
    }
}
