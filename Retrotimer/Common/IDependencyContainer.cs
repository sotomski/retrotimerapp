﻿using System;
namespace Retrotimer.Common
{
    public interface IDependencyContainer
    {
		bool ContainsCreated<TClass>();

		bool IsRegistered<T>();

        void Register<TClass>(Func<TClass> factory, bool createInstanceImmediately) where TClass : class;
		void Register<TClass>(Func<TClass> factory) where TClass : class;
		void Register<TInterface, TClass>() where TInterface : class where TClass : class;
		void Register<TInterface, TClass>(bool createInstanceImmediately) where TInterface : class where TClass : class;
		void Register<TClass>() where TClass : class;
		void Register<TClass>(bool createInstanceImmediately) where TClass : class;

		void Reset();

        TClass Resolve<TClass>() where TClass : class;
    }
}
