﻿﻿using System;

namespace Retrotimer.Common
{
    public interface IDevice
    {
        void LaunchUriAsync(Uri uriToOpen);
    }
}
