﻿using System;
using GalaSoft.MvvmLight.Ioc;

namespace Retrotimer.Common
{
    public class MvvMLightDependencyContainer : IDependencyContainer
    {
        readonly SimpleIoc container;

        public MvvMLightDependencyContainer()
        {
            container = SimpleIoc.Default;
        }

        public bool ContainsCreated<TClass>()
        {
            return container.ContainsCreated<TClass>();
        }

        public bool IsRegistered<T>()
        {
            return container.IsRegistered<T>();
        }

        public void Register<TClass>(Func<TClass> factory, bool createInstanceImmediately) where TClass : class
        {
            container.Register(factory, createInstanceImmediately);
        }

        public void Register<TClass>(Func<TClass> factory) where TClass : class
        {
            container.Register(factory);
        }

        public void Register<TInterface, TClass>()
            where TInterface : class
            where TClass : class
        {
            container.Register<TInterface, TClass>();
        }

        public void Register<TInterface, TClass>(bool createInstanceImmediately)
            where TInterface : class
            where TClass : class
        {
            container.Register<TInterface, TClass>(createInstanceImmediately);
        }

        public void Register<TClass>() where TClass : class
        {
            container.Register<TClass>();
        }

        public void Register<TClass>(bool createInstanceImmediately) where TClass : class
        {
            container.Register<TClass>(createInstanceImmediately);
        }

        public void Reset()
        {
            container.Reset();
        }

        public TClass Resolve<TClass>() where TClass : class
        {
            return container.GetInstance<TClass>();
        }
    }
}
