﻿namespace Retrotimer.Common
{
    public static class StringExtensions
    {
        public static string Truncate(this string str, int length)
        {
            return str.PadRight(length).Substring(0, length).Trim();
        }
    }
}