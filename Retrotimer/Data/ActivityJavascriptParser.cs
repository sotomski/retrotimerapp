﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Retrotimer.Model;

namespace Retrotimer.Data
{
	public static class ActivityJavascriptParser
	{
		static string StartOfActivitiesDefinitions = "all_activities = [];";

        public static IEnumerable<Activity> Parse(string htmlContent)
		{
			if (string.IsNullOrEmpty(htmlContent))
			{
				return new List<Activity>();
			}

			var contentForParsing = htmlContent;

			// Find activities' definitions.
			var indexOfActivitiesDeclaraions = htmlContent.IndexOf(StartOfActivitiesDefinitions, StringComparison.Ordinal);
			if (indexOfActivitiesDeclaraions != -1)
			{
				contentForParsing = htmlContent.Substring(indexOfActivitiesDeclaraions + StartOfActivitiesDefinitions.Length);
			}

			var activities = from expression in contentForParsing.Split(new[] { "};" }, StringSplitOptions.RemoveEmptyEntries)
							 let nullableId = NullableTryFindId(expression.Trim())
							 where nullableId != null
							 let subExpressions = from Match match in Regex.Matches(expression.Trim(), @"[\w]+:[\s]*([0-9]+|""[\w\W]*?""|[\w]+)")
												  where !string.IsNullOrEmpty(match.Value)
												  select match.Value
							 let phase = TryFindNullablePhase(subExpressions)
							 where phase != null
							 let name = TryFindStringPropertyByName("name", subExpressions)
							 where !string.IsNullOrEmpty(name)
							 select new Activity(nullableId.Value + 1, phase.Value, name);

			return activities.ToList();
		}

		static int? NullableTryFindId(string text)
		{
			var validId = from char _ in text
						  where text.StartsWith("all_activities[", StringComparison.Ordinal)
						  let start = text.IndexOf("[", StringComparison.Ordinal)
						  let end = text.IndexOf("]", StringComparison.Ordinal)
						  let idCandidate = text.Substring(start + 1, end - start - 1)
						  let nullableID = idCandidate.NullableTryParseInt32()
						  where nullableID != null
						  where nullableID.Value >= 0
						  select nullableID;

			return validId.FirstOrDefault();
		}

		static RetroPhase? TryFindNullablePhase(IEnumerable<string> expressions)
		{
			// Source of mapping:
			// var phase_titles = ['Set the stage', 'Gather data', 'Generate insights', 'Decide what to do', 'Close the retrospective', 'Something completely different'];
			var phaseCandidates = from subExp in expressions
								  where subExp.Trim().Contains("phase:")
								  let colonIndex = subExp.IndexOf(':')
								  let intCandidate = subExp.Substring(colonIndex + 1).Trim()
								  let intPhase = intCandidate.NullableTryParseInt32()
								  where intPhase != null
									  && Enum.IsDefined(typeof(RetroPhase), intPhase)
								  select (RetroPhase?)intPhase;

			return phaseCandidates.FirstOrDefault();
		}

		/// <summary>
		/// Searches for a string-typed property in the provided expressions.
		/// Will only find strings with quotes ("") at the beginning and end. 
		/// </summary>
		/// <param name="propertyName">Name of a string property to search for.</param>
		/// <param name="expressions">Dataset to search.</param>
		/// <returns>Property value if found; <c>null</c> otherwise</returns>
		static string TryFindStringPropertyByName(string propertyName, IEnumerable<string> expressions)
		{
			var nameCandidates = from subExp in expressions
								 where subExp.Trim().Contains(propertyName + ":")
								 let colonIndex = subExp.IndexOf(':')
								 let nameWithQuotes = subExp.Substring(colonIndex + 1).Trim()
								 let firstQuote = nameWithQuotes.IndexOf('"')
								 let lastQuote = nameWithQuotes.LastIndexOf('"')
								 where firstQuote != -1 && lastQuote != -1
								 let nameCandidate = nameWithQuotes.Substring(firstQuote + 1, lastQuote - firstQuote - 1)
								 where !string.IsNullOrEmpty(nameCandidate)
								 select nameCandidate;

			return nameCandidates.FirstOrDefault();
		}
	}
}
