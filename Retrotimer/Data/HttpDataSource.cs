﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Retrotimer.Model;

namespace Retrotimer.Data
{
    public class HttpDataSource : IDataSource
    {
        static readonly Dictionary<string, string> TypeToUriMap = new Dictionary<string, string>
        {
            { typeof(Activity).ToString(), "https://raw.githubusercontent.com/findingmarbles/Retromat/master/backend/web/static/lang/activities_en.js" }
        };

        readonly HttpClient client;

        public HttpDataSource()
        {
            client = new HttpClient();
        }

        public async Task<IEnumerable<T>> GetAllAsync<T>()
        {
            var uri = GetUriForType(typeof(T));
            var rawData = await client.GetStringAsync(uri);
            var entities = Parse<T>(rawData);

            return entities;
        }

        static string GetUriForType(Type type)
        {
            var typeName = type.ToString();
            if (!TypeToUriMap.TryGetValue(typeName, out string uri))
            {
                throw new ArgumentOutOfRangeException(nameof(type), typeName, "Source Uri for type not found.");
            }

            return uri;
        }

        static IEnumerable<T> Parse<T>(string rawData)
        {
            if (typeof(T) == typeof(Activity))
            {
                return (IEnumerable<T>)ActivityJavascriptParser.Parse(rawData);
            }

            throw new ArgumentException($"Cannot parse type {typeof(T)}.");
        }
    }
}
