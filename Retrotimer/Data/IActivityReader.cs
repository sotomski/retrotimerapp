﻿﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Retrotimer.Model;

namespace Retrotimer.Data
{
	/// <summary>
	/// Provider of <c>Activity</c> objects.
	/// </summary>
	public interface IActivityReader
    {
        /// <summary>
        /// Asynchronously reads all activities from underlying storage.
        /// </summary>
        /// <returns>The all activities.</returns>
        Task<IList<Activity>> ReadAllActivitiesAsync();
    }
}
