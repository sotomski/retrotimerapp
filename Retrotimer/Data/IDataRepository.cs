﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Retrotimer.Data
{
    public interface IDataRepository
    {
        /// <summary>
        /// Reads all the entities of T from repository.
        /// </summary>
        /// <returns>All entities from repository.</returns>
        /// <typeparam name="T">Type of entities to be returned.</typeparam>
        Task<IEnumerable<T>> ReadAllAsync<T>();

        /// <summary>
        /// Saves provided entities to the underlying store. 
        /// </summary>
        /// <returns>Task for synchronisation.</returns>
        /// <param name="input">Entities to be saved.</param>
        /// <typeparam name="T"></typeparam>
        Task SaveAllAsync<T>(IEnumerable<T> input);
    }
}
