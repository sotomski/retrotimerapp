﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Retrotimer.Data
{
    public interface IDataSource
    {
        Task<IEnumerable<T>> GetAllAsync<T>();
    }
}
