﻿namespace Retrotimer.Data
{
    public interface ISerializer
    {
        string Serialize<T>(T valueToSerialize);
        T Deserialize<T>(string valueToDeserialize);
    }
}
