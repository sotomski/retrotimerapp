﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Retrotimer.Model;
using Retrotimer.Services.Data;

namespace Retrotimer.Data
{
    /// <summary>
    /// TEMPORARY Json activity reader class created to test the Json data repository.
    /// </summary>
    public class JsonActivityReader : IActivityReader
    {
        readonly IDataService dataService;

        public JsonActivityReader(IDataService dataSvc)
        {
            throw new InvalidOperationException("Please use IDataService directly instead.");

            this.dataService = dataSvc;
        }

        public async Task<IList<Activity>> ReadAllActivitiesAsync()
        {
            var activities = await dataService.ReadActivitiesAsync();

            return activities.ToList();
        }
    }
}
