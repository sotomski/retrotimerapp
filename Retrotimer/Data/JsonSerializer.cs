﻿using System;
using Newtonsoft.Json;

namespace Retrotimer.Data
{
    public class JsonSerializer : ISerializer
    {
        public T Deserialize<T>(string valueToDeserialize)
        {
            return JsonConvert.DeserializeObject<T>(valueToDeserialize);
        }

        public string Serialize<T>(T valueToSerialize)
        {
            return JsonConvert.SerializeObject(valueToSerialize);
        }
    }
}
