﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PCLStorage;

namespace Retrotimer.Data
{
    /// <summary>
    /// Allows local caching of data in files. Requires a serializer to work. 
    /// Supports only a global expiration setting.
    /// </summary>
    public class LocalFileDataRepository : IDataRepository
    {
        const string CacheFolder = "data";
        readonly ISerializer serializer;
        readonly IFileSystem fileSystem;

        public LocalFileDataRepository(IFileSystem fileSystem, ISerializer serializer)
        {
            this.fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
            this.serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
        }

        public async Task<IEnumerable<T>> ReadAllAsync<T>()
        {
			IEnumerable<T> deserializedEntities = new List<T>();

            try
            {
				var cacheFolder = await OpenCacheFolder();
				var typename = typeof(T).FullName;
				var allCacheFiles = await cacheFolder.GetFilesAsync();
				if (allCacheFiles.Count > 0)
				{
				    var fileWithSelectedType = allCacheFiles.First(f => f.Name.StartsWith(typename));
					var content = await fileWithSelectedType.ReadAllTextAsync();
					deserializedEntities = serializer.Deserialize<IEnumerable<T>>(content);
				}
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(
                    "Deserialization from local file cache filed with exception: " + ex.Message);
                System.Diagnostics.Debug.WriteLine("Stacktrace: " + ex.StackTrace);
            }

            return deserializedEntities;
        }

		async Task<IFolder> OpenCacheFolder()
		{
			return await fileSystem.LocalStorage.CreateFolderAsync(CacheFolder, CreationCollisionOption.OpenIfExists);
		}

        public async Task SaveAllAsync<T>(IEnumerable<T> input)
        {
            var cacheFolder = await OpenCacheFolder();

            var filename = input.GetType().GenericTypeArguments.First().ToString();
            var file = await cacheFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);

            var serializedData = serializer.Serialize(input);
            await file.WriteAllTextAsync(serializedData);
        }
    }
}
