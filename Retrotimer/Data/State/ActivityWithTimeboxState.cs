﻿using System;
using Retrotimer.Model;

namespace Retrotimer.Data.State
{
	public struct ActivityWithTimeboxState
	{
		public Activity Activity;
		public string Timebox;

		public ActivityWithTimeboxState(ActivityWithTimebox awt)
		{
			Activity = awt.Activity;
			Timebox = awt.Timebox.ToString();
		}

		public ActivityWithTimebox ToActivityWithTimebox()
		{
			TimeSpan timespan;
			TimeSpan.TryParse(Timebox, out timespan);
			return new ActivityWithTimebox(Activity, timespan);
		}

		public override string ToString()
		{
			return $"[{GetType()}: {Activity}, {Timebox}]";
		}
	}
}
