﻿﻿using System.Threading.Tasks;

namespace Retrotimer.Data.State
{
	/// <summary>
	/// Retrospective plan cache allows storing of one plan.
	/// Perfect for saving state for navigation or before closing of the app.
	/// </summary>
	/// <remarks>
	/// This class uses always the same file for storing the plan, so multiple instances will
	/// overwrite previous data.
	/// </remarks>
	public interface IAppStateManager
	{
		Task SaveAsync(RetroPlanState plan);

		/// <summary>
		/// Retrieves retrospective plan from underlying cache.
		/// </summary>
		/// <returns>A retrospective plan if read operation successful; null otherwise</returns>
		/// <exception cref="StateStorageException">Thrown when deserialization have failed</exception>
		Task<RetroPlanState> ReadAsync();
	}
}
