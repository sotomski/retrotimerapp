﻿using System.Collections.Generic;
using Retrotimer.Model;
using Retrotimer.ViewModel;

namespace Retrotimer.Data.State
{
    public struct RetroPlanState
	{
		public ActivityWithTimeboxState SetTheStage;
		public ActivityWithTimeboxState GatherData;
		public ActivityWithTimeboxState GenerateInsights;
		public ActivityWithTimeboxState DecideWhatToDo;
		public ActivityWithTimeboxState CloseTheRetrospective;
		public VisualStyleCollectionState VisualStyles;

		public RetroPlanState(RetrospectivePlan plan, IEnumerable<VisualStyle> styles)
		{
			SetTheStage = new ActivityWithTimeboxState(plan.SetTheStage);
			GatherData = new ActivityWithTimeboxState(plan.GatherData);
			GenerateInsights = new ActivityWithTimeboxState(plan.GenerateInsights);
			DecideWhatToDo = new ActivityWithTimeboxState(plan.DecideWhatToDo);
			CloseTheRetrospective = new ActivityWithTimeboxState(plan.CloseTheRetrospective);

			VisualStyles = new VisualStyleCollectionState(styles);
		}

		public RetrospectivePlan ToRetrospectivePlan()
		{
			return new RetrospectivePlan
			{
				SetTheStage = SetTheStage.ToActivityWithTimebox(),
				GatherData = GatherData.ToActivityWithTimebox(),
				GenerateInsights = GenerateInsights.ToActivityWithTimebox(),
				DecideWhatToDo = DecideWhatToDo.ToActivityWithTimebox(),
				CloseTheRetrospective = CloseTheRetrospective.ToActivityWithTimebox()
			};
		}

		public IEnumerable<VisualStyle> ToVisualStyles()
		{
			return VisualStyles.ToStyleCollection();
		}

		public override string ToString()
		{
			return GetType()
			       + $": {SetTheStage}, {GatherData}, {GenerateInsights}, {DecideWhatToDo}, {CloseTheRetrospective}, "
			       + VisualStyles;
		}
	}
}
