﻿using System;

namespace Retrotimer.Data.State
{
	public class StateStorageException : Exception
	{
		public StateStorageException(string message, Exception innerException)
		: base(message, innerException)
		{
		}
	}
}
