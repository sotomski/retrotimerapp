﻿﻿using System.Collections.Generic;
using Retrotimer.ViewModel;

namespace Retrotimer.Data.State
{
	/// <summary>
	/// Allows easy serialization of generic list of visualstyles.
	/// </summary>
	public struct VisualStyleCollectionState
	{
		public List<VisualStyle> StyleCollection;

		public VisualStyleCollectionState(IEnumerable<VisualStyle> styles)
		{
			StyleCollection = new List<VisualStyle>(styles);
		}

		public IEnumerable<VisualStyle> ToStyleCollection()
		{
			return new List<VisualStyle>(StyleCollection);
		}
	}
}
