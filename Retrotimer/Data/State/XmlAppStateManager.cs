﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using PCLStorage;

namespace Retrotimer.Data.State
{
    public class XmlAppStateManager : IAppStateManager
	{
		readonly string filePath;
		readonly XmlRetrospectivePlanStateWriter writer;
		readonly XmlRetrospectivePlanStateReader reader;

		public XmlAppStateManager(string filePath, IFileSystem fileSystem)
		{
			if (fileSystem == null)
				throw new ArgumentNullException(nameof(fileSystem));

			writer = new XmlRetrospectivePlanStateWriter(fileSystem);
			reader = new XmlRetrospectivePlanStateReader(fileSystem);

			this.filePath = filePath;
		}

		public async Task SaveAsync(RetroPlanState plan)
		{
			try
			{
				await writer.WriteAsync(plan, filePath);
			}
			catch (Exception ex)
			{
				Debug.WriteLine("XmlStateStorage.SaveAsync >> Writer threw exception.");
				Debug.WriteLine(ex);
			}
		}

		public async Task<RetroPlanState> ReadAsync()
		{
			try
			{
				var retrievedPlan = await reader.ReadAsync(filePath);
				return retrievedPlan;
			}
			catch (Exception ex)
			{
				Debug.WriteLine("XmlStateStorage.ReadAsync >> Reader threw exception.");
				Debug.WriteLine(ex);
				throw new StateStorageException("Deserialisation failed.", ex);
			}
		}
	}
}
