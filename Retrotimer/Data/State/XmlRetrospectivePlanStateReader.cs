﻿using System.Threading.Tasks;
using System.Xml.Serialization;
using PCLStorage;

namespace Retrotimer.Data.State
{
	/// <summary>
	/// Deserializes XML files back to a state of a retro plan from given Uri.
	/// </summary>
	/// <remarks>
	/// This class is designed to "fail fast" and throw all kinds of exceptions.
	/// The responsibility for handling them lies on the caller.
	/// </remarks>
	class XmlRetrospectivePlanStateReader
	{
		readonly IFileSystem fileSystem;
		readonly XmlSerializer serializer;

		public XmlRetrospectivePlanStateReader(IFileSystem fileSystem)
		{
			this.fileSystem = fileSystem;
			serializer = new XmlSerializer(typeof(RetroPlanState));
		}

		public async Task<RetroPlanState> ReadAsync(string absoluteUri)
		{
			RetroPlanState deserializedPlan;
			var file = await fileSystem.GetFileFromPathAsync(absoluteUri);
			using (var stream = await file.OpenAsync(PCLStorage.FileAccess.Read))
			{
				deserializedPlan = await Task.Factory.StartNew(() =>
				{
					return (RetroPlanState)serializer.Deserialize(stream);
				});
			}

			return deserializedPlan;
		}
	}
}
