﻿using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using PCLStorage;

namespace Retrotimer.Data.State
{
    /// <summary>
    /// Serializes the state of a retro plan to XML file specified by the Uri.
    /// </summary>
    /// <remarks>
    /// This class is designed to "fail fast" and throw all kinds of exceptions.
    /// The responsibility for handling them lies on the caller.
    /// </remarks>
    class XmlRetrospectivePlanStateWriter
	{
		readonly IFileSystem fileSystem;
		readonly XmlSerializer serializer;

		public XmlRetrospectivePlanStateWriter(IFileSystem fileSystem)
		{
			this.fileSystem = fileSystem;
			serializer = new XmlSerializer(typeof(RetroPlanState));
		}

		public async Task WriteAsync(RetroPlanState state, string absoluteUri)
		{
			var filename = Path.GetFileName(absoluteUri);
			var directoryName = Path.GetDirectoryName(absoluteUri);

			var dir = await fileSystem.GetFolderFromPathAsync(directoryName);
			var file = await dir.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);

			using (var stream = await file.OpenAsync(FileAccess.ReadAndWrite))
			{
				await Task.Factory.StartNew(() =>
				{
					serializer.Serialize(stream, state);
				});
			}
		}
	}
}
