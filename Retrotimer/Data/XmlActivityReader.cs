﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Linq;
using System.IO;
using Retrotimer.Services;
using Retrotimer.Model;

namespace Retrotimer.Data
{
	/// <summary>
	/// Activity provider using XML as storage of activities.
	/// </summary>
	public class XmlActivityReader : IActivityReader
    {
        readonly string xmlResourceId;
        readonly IStorage storage;

        public string XmlResourceId
        {
            get
            {
                return xmlResourceId;
            }
        }
        
        public XmlActivityReader(string xmlResId, IStorage fileSystem)
        {
            throw new InvalidOperationException("Please use IDataService instead.");

			xmlResourceId = xmlResId;
			storage = fileSystem;
        }
        
        public async Task<IList<Activity>> ReadAllActivitiesAsync()
        {
            string xmlRawContent = await ReadFileContentAsync();

            XDocument xml = XDocument.Parse(xmlRawContent);
            XNamespace ns = "http://tempuri.org/Activities.xsd";

            var activitiesQuery = from mainNodes in xml.Descendants(ns + "activities")
                                           from activityNode in mainNodes.Elements(ns + "activity")
                                           let nullableId = activityNode.Attribute("activityId").Value.NullableTryParseInt32()
                                           where nullableId != null && nullableId >= 0
                                           let nullablePhase = TryFindNullablePhase(activityNode.Element(ns + "phase").Value)
                                           where nullablePhase != null
                                           let name = activityNode.Element(ns + "name").Value
                                           where !string.IsNullOrEmpty(name)
                                           select new Activity(nullableId.Value, nullablePhase.Value, name);
            
            return activitiesQuery.ToList();
        }

        async Task<string> ReadFileContentAsync()
        {
            string xmlRawContent = null;

            try
            {
                var stream = await storage.EmbeddedResources.OpenStreamToResource(XmlResourceId);

                using(var reader = new StreamReader(stream))
                {
                    xmlRawContent = await reader.ReadToEndAsync();
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("From Retromat: I/O operation failed", ex);
            }

            return xmlRawContent;
        }

        static RetroPhase? TryFindNullablePhase(string phaseString)
        {
            RetroPhase phase;

            return Enum.TryParse(phaseString, out phase) ? (RetroPhase?)phase : null;
        }
    }
}
