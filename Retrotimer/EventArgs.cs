﻿﻿using System;

namespace Retrotimer
{
    public class EventArgs<T> : EventArgs
    {
        public T Value { get; private set; }

        public EventArgs(T aValue)
        {
            Value = aValue;
        }
    }
}
