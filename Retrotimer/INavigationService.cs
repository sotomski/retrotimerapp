﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Retrotimer
{
	public enum NavigationPages 
    {
        InfoPage,
		BrowseStagePage,
        StartPage,
        RetroPlanPage
    }

    public interface INavigationService
    {
        Task PushModalAsync(NavigationPages page, bool animated, INotifyPropertyChanged bindingContext = null);
        Task PopModalAsync();

		Task PushAsync(NavigationPages page, bool animated, INotifyPropertyChanged bindingContext = null);
		Task PopAsync();
    }
}
