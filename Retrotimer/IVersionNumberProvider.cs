﻿namespace Retrotimer
{
	public interface IVersionNumberProvider
	{
		/// <summary>
		/// Gets the app version number.
		/// </summary>
		/// <returns>The app version number.</returns>
		string GetAppVersionNumber();
	}
}
