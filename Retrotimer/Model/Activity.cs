﻿using System;

namespace Retrotimer.Model
{
	/// <summary>
	/// Different stages of the retrospective.
	/// </summary>
	/// <remarks>
	/// var phase_titles = ['Set the stage', 'Gather data', 'Generate insights', 'Decide what to do', 'Close the retrospective', 'Something completely different'];
	/// </remarks>
	public enum RetroPhase
    {
        SetTheStage = 0,
        GatherData = 1,
        GenerateInsights = 2,
        DecideWhatToDo = 3,
        CloseTheRetrospective = 4
    }
    
    public struct Activity
    {
        public int ID;
        public RetroPhase Phase;
        public string Name;

        public Activity(int id, RetroPhase phase, string name)
        {
            ID = id;
            Phase = phase;
            Name = name;
        }

        public override string ToString()
        {
			return $"[Activity: ID={ID}, Phase={Phase}, Name={Name}]";
        }
    }
}