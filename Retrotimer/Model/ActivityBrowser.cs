﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Retrotimer.Model
{
	public class ActivityBrowser : IActivityBrowser
	{
		readonly IList<Activity> activities;

		public ActivityBrowser(IList<Activity> activities)
		{
            throw new InvalidOperationException("Please use IDataService instead.");

			if(activities == null)
				throw new ArgumentNullException(nameof(activities));

			this.activities = activities;
		}

		public IList<Activity> GetActivitiesForPhase(RetroPhase phase)
		{
			return activities.Where(a => a.Phase == phase).ToList();
		}

		public IList<Activity> GetAllActivities()
		{
			return new List<Activity>(activities);
		}
	}
}
