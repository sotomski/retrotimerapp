﻿using System;
using System.Threading.Tasks;
using Retrotimer.Data;

namespace Retrotimer.Model
{
	public class ActivityBrowserProvider : IActivityBrowserProvider
	{
		readonly IActivityReader reader;

		public ActivityBrowserProvider(IActivityReader reader)
		{
            this.reader = reader ?? throw new ArgumentNullException(nameof(reader));
		}

		public async Task<IActivityBrowser> CreateActivityBrowserAsync()
		{
            //IActivityBrowser newBrowser = null;
            //var loadedActivities = await reader.ReadAllActivitiesAsync();

            //if(loadedActivities.Count > 0)
            //{
            //	newBrowser = new ActivityBrowser(loadedActivities);
            //}

            //return newBrowser;

            throw new InvalidOperationException("Please use IDataService instead.");
		}
	}
}
