﻿
namespace Retrotimer.Model
{
    public static class ActivityExtensions
    {
        public static int? NullableTryParseInt32(this string text)
        {
            int value;
            return int.TryParse(text, out value) ? (int?)value : null;
        }
    }
}

