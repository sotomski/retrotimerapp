using System;

namespace Retrotimer.Model
{
	public struct ActivityWithTimebox
	{
		public Activity Activity;
		public TimeSpan Timebox;

		public ActivityWithTimebox(Activity activity, TimeSpan timebox)
		{
			Activity = activity;
			Timebox = timebox;
		}

		public override string ToString()
		{
			return string.Format("Activity:s {0}\t\tTimebox: {1}", Activity, Timebox);
		}
	}
}
