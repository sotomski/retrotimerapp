﻿using System.Collections.Generic;

namespace Retrotimer.Model
{
	public interface IActivityBrowser
	{
		IList<Activity> GetAllActivities();
		IList<Activity> GetActivitiesForPhase(RetroPhase phase);
	}
}
