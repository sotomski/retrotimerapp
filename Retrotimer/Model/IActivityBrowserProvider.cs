﻿using System.Threading.Tasks;

namespace Retrotimer.Model
{
	public interface IActivityBrowserProvider
	{
		/// <summary>
		/// Creates the activity browser.
		/// </summary>
		/// <returns>The activity browser.</returns>
		Task<IActivityBrowser> CreateActivityBrowserAsync();
	}
}
