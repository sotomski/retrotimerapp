﻿using System;

namespace Retrotimer.Model
{
	public interface IRetroPlaylistItem
	{
		TimeSpan OriginalTimebox { get; }
		TimeSpan RemainingTimebox { get; set; }
	}
}
