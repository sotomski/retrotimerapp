﻿using System;
using System.Threading.Tasks;
using Retrotimer.Data.State;

namespace Retrotimer.Model
{
    public interface IRetrospectiveInteractor
    {
	    event EventHandler<RetrospectivePlanOutdatedEventArgs> RetrospectivePlanOutdated;
		
        Task<RetrospectivePlan> GenerateRetrospectivePlan();

		Task SaveAsync(RetroPlanState plan);
		Task<RetroPlanState> ReadAsync();
	}
}
