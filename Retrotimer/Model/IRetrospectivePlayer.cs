﻿using System;

namespace Retrotimer.Model
{
	/// <summary>
	/// Retrospective player is responsible for controlling the flow of timeboxes in the retrospective.
	/// </summary>
	public interface IRetrospectivePlayer
	{
		/// <summary>
		/// Occurs when played item changed.
		/// </summary>
		event EventHandler<EventArgs<IRetroPlaylistItem>> PlayedItemChanged;
		
		/// <summary>
		/// Occurs when retrospective finished (i.e. all timeboxes are finished).
		/// </summary>
		event EventHandler RetrospectiveFinished;

		/// <summary>
		/// Starts/resumes retrospective playback.
		/// </summary>
		void StartOrResume();

		/// <summary>
		/// Pauses the retrospective playback.
		/// </summary>
		void Pause();

		/// <summary>
		/// Stops the retrospective playbacks and resets timeboxes. 
		/// All the timeboxes inside the retrospective will be reset to their original length.
		/// </summary>
		void Reset();

		bool CheckIfPlayingFirstItem();

		bool CheckIfPlayingLastItem();

		void MoveForwards();

		void MoveBackwards();
	}
}
