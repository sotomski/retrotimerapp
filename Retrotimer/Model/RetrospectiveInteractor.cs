﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Retrotimer.Data.State;
using Retrotimer.Services;
using Retrotimer.Services.Data;

namespace Retrotimer.Model
{
    public class RetrospectiveInteractor : IRetrospectiveInteractor
    {
        readonly IAppStateManager appStateManager;
        readonly IDataService dataSvc;
        readonly IRandomGenerator rand;
        IEnumerable<Activity> activities;

        public event EventHandler<RetrospectivePlanOutdatedEventArgs> RetrospectivePlanOutdated;

        public RetrospectiveInteractor(
            IDataService dataService, IAppStateManager appStateManager, IRandomGenerator randomizer)
        {
            this.appStateManager = appStateManager ?? throw new ArgumentNullException(nameof(appStateManager));
            rand = randomizer ?? throw new ArgumentNullException(nameof(randomizer));
            dataSvc = dataService ?? throw new ArgumentNullException(nameof(dataService));
            dataSvc.NewActivitiesAvailable += OnNewActivitiesAvailableInDataSet;
        }

        /// <summary>
        /// Generates the retrospective plan with randomly picked activities.
        /// </summary>
        /// <returns>The retrospective plan.</returns>
        public async Task<RetrospectivePlan> GenerateRetrospectivePlan()
        {
            if (activities == null)
            {
                activities = await dataSvc.ReadActivitiesAsync();
            }

            ValidateReadinessForRetroPlanCreation();

            var generatedPlan = new RetrospectivePlan
            {
                SetTheStage =
                    new ActivityWithTimebox(GetRandomActivityForPhase(RetroPhase.SetTheStage), TimeSpan.FromMinutes(5)),
                GatherData =
                    new ActivityWithTimebox(GetRandomActivityForPhase(RetroPhase.GatherData), TimeSpan.FromMinutes(20)),
                GenerateInsights = new ActivityWithTimebox(GetRandomActivityForPhase(RetroPhase.GenerateInsights),
                    TimeSpan.FromMinutes(30)),
                DecideWhatToDo = new ActivityWithTimebox(GetRandomActivityForPhase(RetroPhase.DecideWhatToDo),
                    TimeSpan.FromMinutes(30)),
                CloseTheRetrospective =
                    new ActivityWithTimebox(GetRandomActivityForPhase(RetroPhase.CloseTheRetrospective),
                        TimeSpan.FromMinutes(5))
            };

            return generatedPlan;
        }

        void ValidateReadinessForRetroPlanCreation()
        {
            ValidatePresenceOfActivitiesForPhase(RetroPhase.SetTheStage);
            ValidatePresenceOfActivitiesForPhase(RetroPhase.GatherData);
            ValidatePresenceOfActivitiesForPhase(RetroPhase.GenerateInsights);
            ValidatePresenceOfActivitiesForPhase(RetroPhase.DecideWhatToDo);
            ValidatePresenceOfActivitiesForPhase(RetroPhase.CloseTheRetrospective);
        }

        Activity GetRandomActivityForPhase(RetroPhase phase)
        {
			var activitiesInPhase = activities.Where(a => a.Phase == phase).ToList();
            var idx = rand.GetInt(0, activitiesInPhase.Count);
            return activitiesInPhase[idx];
        }

        void ValidatePresenceOfActivitiesForPhase(RetroPhase phase)
        {
            if (activities.All(a => a.Phase != phase))
            {
                var message = $"Required activity for {phase} phase not found.";
                throw new InvalidOperationException(message);
            }
        }
        
        public async Task SaveAsync(RetroPlanState plan)
        {
            await appStateManager.SaveAsync(plan);
        }

        public async Task<RetroPlanState> ReadAsync()
        {
            var loadedRetroState = await appStateManager.ReadAsync();         
            var mergedState = await MergeLoadedStateWithCurrentDataSet(loadedRetroState);
            
            return mergedState;
        }

        private async Task<RetroPlanState> MergeLoadedStateWithCurrentDataSet(RetroPlanState stateToMerge)
        {
            var dataset = await dataSvc.ReadActivitiesAsync();
            var datasetList = dataset as IList<Activity> ?? dataset.ToList();
            
            var oldPlan = stateToMerge.ToRetrospectivePlan();
            var newPlan = MergePlanWithCurrentDataSet(oldPlan, datasetList);
            var mergedState = new RetroPlanState(newPlan, stateToMerge.ToVisualStyles());
            
            return mergedState;
        }

        private static RetrospectivePlan MergePlanWithCurrentDataSet(
            RetrospectivePlan planToMerge, IList<Activity> dataSet)
        {
            var mergedPlan = new RetrospectivePlan
            {
                SetTheStage = GetUpToDateActivityWithTimebox(planToMerge.SetTheStage, dataSet),
                GatherData = GetUpToDateActivityWithTimebox(planToMerge.GatherData, dataSet),
                GenerateInsights = GetUpToDateActivityWithTimebox(planToMerge.GenerateInsights, dataSet),
                DecideWhatToDo = GetUpToDateActivityWithTimebox(planToMerge.DecideWhatToDo, dataSet),
                CloseTheRetrospective = GetUpToDateActivityWithTimebox(planToMerge.CloseTheRetrospective, dataSet)
            };

            return mergedPlan;
        }

        private static ActivityWithTimebox GetUpToDateActivityWithTimebox(
            ActivityWithTimebox outOfDate, IList<Activity> dataset)
        {
            var upToDateActivity = GetUpToDateActivityFor(outOfDate.Activity, dataset);
            return new ActivityWithTimebox(upToDateActivity, outOfDate.Timebox);
        }

        private static Activity GetUpToDateActivityFor(Activity outOfDate, IList<Activity> datasetList)
        {
            var upToDateTwins = datasetList.Where(a => a.ID == outOfDate.ID).ToList();
            var firstInPhase = datasetList.First(a => a.Phase == outOfDate.Phase);
            upToDateTwins.Add(firstInPhase);

            return upToDateTwins.First();
        }
        
        private void OnNewActivitiesAvailableInDataSet(object sender, EventArgs<IEnumerable<Activity>> eventArgs)
        {
            var retroEventArgs = new RetrospectivePlanOutdatedEventArgs(oldPlan =>
            {
                var datasetList = eventArgs.Value.ToList();
                var newPlan = MergePlanWithCurrentDataSet(oldPlan, datasetList);
                return newPlan;
            });
            
            RetrospectivePlanOutdated?.Invoke(this, retroEventArgs);
        }
    }
}
