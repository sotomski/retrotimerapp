﻿using System.Collections.Generic;

namespace Retrotimer.Model
{
	/// <summary>
	/// Retrospective plan containing all necessary activities.
	/// </summary>
	public struct RetrospectivePlan
	{
		public ActivityWithTimebox SetTheStage;
		public ActivityWithTimebox GatherData;
		public ActivityWithTimebox GenerateInsights;
		public ActivityWithTimebox DecideWhatToDo;
		public ActivityWithTimebox CloseTheRetrospective;

		public RetrospectivePlan ChangePlanWithActivity(ActivityWithTimebox activityToInsert)
		{
			switch (activityToInsert.Activity.Phase)
			{
				case RetroPhase.SetTheStage:
					SetTheStage = activityToInsert;
					break;

				case RetroPhase.GatherData:
					GatherData = activityToInsert;
					break;

				case RetroPhase.GenerateInsights:
					GenerateInsights = activityToInsert;
					break;

				case RetroPhase.DecideWhatToDo:
					DecideWhatToDo = activityToInsert;
					break;

				case RetroPhase.CloseTheRetrospective:
					CloseTheRetrospective = activityToInsert;
					break;
			}
			
			return this;
		}

		public ActivityWithTimebox GetActivityForPhase(RetroPhase phase)
		{
			var allActivities = new Dictionary<RetroPhase, ActivityWithTimebox>
				{
					{ RetroPhase.SetTheStage, SetTheStage },
					{ RetroPhase.GatherData, GatherData },
					{ RetroPhase.GenerateInsights, GenerateInsights },
					{ RetroPhase.DecideWhatToDo, DecideWhatToDo },
					{ RetroPhase.CloseTheRetrospective, CloseTheRetrospective }
			};

			return allActivities[phase];
		}

		public override string ToString()
		{
			return string.Format($"[RetrospectivePlan] >> {SetTheStage} {GatherData} {GenerateInsights} {DecideWhatToDo} {CloseTheRetrospective}");
		}
	}
}
