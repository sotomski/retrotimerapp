﻿using System;

namespace Retrotimer.Model
{
    public class RetrospectivePlanOutdatedEventArgs : EventArgs
    {
        public readonly Func<RetrospectivePlan, RetrospectivePlan> Merger;

        public RetrospectivePlanOutdatedEventArgs(Func<RetrospectivePlan, RetrospectivePlan> mergerFunc)
        {
            Merger = mergerFunc;
        }
    }
}