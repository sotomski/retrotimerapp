﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using Retrotimer.Common;
using Retrotimer.Services;
using Retrotimer.Services.Timer;

namespace Retrotimer.Model
{
	public class RetrospectivePlayer : IRetrospectivePlayer
	{
		static readonly TimeSpan TimerInterval = TimeSpan.FromSeconds(1);
		ICountdownTimerProvider timerProvider;
		readonly List<IRetroPlaylistItem> playlist;
		protected Cursor<IRetroPlaylistItem> cursor;
		ICountdownTimer timer;

		public event EventHandler<EventArgs<IRetroPlaylistItem>> PlayedItemChanged;
		public event EventHandler RetrospectiveFinished;

		bool IsTimerActive => timer != null;

		public RetrospectivePlayer(IList<IRetroPlaylistItem> initialPlaylist, ICountdownTimerProvider timerProvider)
		{
			if (initialPlaylist == null)
				throw new ArgumentNullException(nameof(initialPlaylist));

			if (timerProvider == null)
				throw new ArgumentNullException(nameof(timerProvider));

			this.timerProvider = timerProvider;
			playlist = new List<IRetroPlaylistItem>(initialPlaylist);
			cursor = playlist.GetCursor();
		}

		void StartCurrentItemOrFinish()
		{
			if (cursor.Current == null)
			{
				ZeroAllPlaylistItems();
				InformWorldThatCountdownIsFinished();
			}
			else
			{
				StartCurrentItem();
			}
		}

		void ZeroAllPlaylistItems()
		{
			foreach (var item in playlist)
			{
				item.RemainingTimebox = TimeSpan.Zero;
			}
		}

		void InformWorldThatCountdownIsFinished()
		{
			PublishNotificationMessage(MessageCommunicationProtocol.CountdownCompleted);
			RaisePlayedItemChanged();
			RaiseRetrospectiveFnished();
		}
		
		void StartCurrentItem()
		{
			CountdownInfo countdownInfo = CreateCountdownInfoFromCurrentTimebox();
			StartCountdownWith(countdownInfo);
			
			RaisePlayedItemChanged();
			PublishNotificationMessage(MessageCommunicationProtocol.CountdownStageChanged);
		}

		void PublishNotificationMessage(string msg)
		{
			var notif = new NotificationMessage(this, msg);
			Messenger.Default.Send(notif);
		}

		public void StartOrResume()
		{
			if (playlist.Count <= 0)
			{
				InformWorldThatCountdownIsFinished();
				return;
			}

			if (!IsTimerActive)
			{
				cursor.MoveNext();
				StartCurrentItem();
			}
			else
			{
				timer.Resume();
			}
		}

		CountdownInfo CreateCountdownInfoFromCurrentTimebox()
		{
			var timebox = cursor.Current.OriginalTimebox;
			return CreateCountdownInfo(timebox);
		}

		void StartCountdownWith(CountdownInfo info)
		{
			timer = timerProvider.CreateTimer();
			timer.StartCountdown(info);
		}

		void RaisePlayedItemChanged()
		{
			var item = cursor.Current;
			var args = new EventArgs<IRetroPlaylistItem>(item);
			PlayedItemChanged?.Invoke(this, args);
		}

		CountdownInfo CreateCountdownInfo(TimeSpan countdown)
		{
			return new CountdownInfo
			{
				UpdateInterval = TimerInterval,
				Countdown = countdown,
				UpdateRemainingTimeCallback = OnUpdateRemainingTime,
				FinishCallback = OnItemCountdownFinished
			};
		}

		void OnUpdateRemainingTime(TimeSpan remainingTime)
		{
			var current = cursor.Current;
			current.RemainingTimebox = remainingTime;
		}

		void OnItemCountdownFinished()
		{
			cursor.Current.RemainingTimebox = TimeSpan.Zero;
			cursor.MoveNext();
			StartCurrentItemOrFinish();
		}

		void RaiseRetrospectiveFnished()
		{
			RetrospectiveFinished?.Invoke(this, new EventArgs());
		}

		public void Pause()
		{
			if (IsTimerActive)
			{
				timer.Pause();
			}
		}

		public void Reset()
		{
			DeactivateTimer();
			ResetPlaylistItems();
			RaisePlayedItemChanged();
		}

		void DeactivateTimer()
		{
			if (IsTimerActive)
				timer.Stop();

			timer = null;
		}

		void ResetPlaylistItems()
		{
			cursor.Reset();
			foreach (var item in playlist)
			{
				item.RemainingTimebox = item.OriginalTimebox;
			}
		}

		public bool CheckIfPlayingFirstItem()
		{
			var first = playlist.FirstOrDefault();
			return CheckIfPlayingItem(first);
		}

		public bool CheckIfPlayingLastItem()
		{
			var last = playlist.LastOrDefault();
			return CheckIfPlayingItem(last);
		}

		bool CheckIfPlayingItem(IRetroPlaylistItem item)
		{
			if (!IsTimerActive)
			{
				return false;
			}

			return cursor.Current == item;
		}

		public void MoveForwards()
		{
			ReconfigurePlaybackByPerforming(() =>
			{
				cursor.Current.RemainingTimebox = TimeSpan.Zero;
				cursor.MoveNext();
			});
		}

		public void MoveBackwards()
		{
			ReconfigurePlaybackByPerforming(() =>
			{
				ResetCurrentTimebox();
				cursor.MovePrevious();
				ResetCurrentTimebox();
			});
		}

		void ResetCurrentTimebox()
		{
			var curr = cursor.Current;
			if (curr != null)
			{
				curr.RemainingTimebox = curr.OriginalTimebox;
			}
		}

		void ReconfigurePlaybackByPerforming(Action configureAction)
		{
			try
			{
				RetainTimerPauseWhilePerforming(() =>
				{
					DeactivateTimer();
					configureAction();
					StartCurrentItemOrFinish();
				});
			}
			catch (Exception ex)
			{
				throw new InvalidOperationException("Timer is not active.", ex);
			}
		}
		
		void RetainTimerPauseWhilePerforming(Action action)
		{
			var wasTimerPaused = timer.IsPaused;

			action();

			if (wasTimerPaused)
			{
				timer.Pause();
			}
		}
	}
}
