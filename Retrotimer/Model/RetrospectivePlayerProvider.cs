﻿using System.Collections.Generic;
using Retrotimer.Services.Timer;

namespace Retrotimer.Model
{
    public interface IRetrospectivePlayerProvider
	{
		IRetrospectivePlayer CreatePlayerForPlan(IList<IRetroPlaylistItem> initialPlaylist);
	}

	public class RetrospectivePlayerProvider : IRetrospectivePlayerProvider
	{
		readonly ICountdownTimerProvider timerProvider;

		public RetrospectivePlayerProvider(ICountdownTimerProvider timerProvider)
		{
			this.timerProvider = timerProvider;
		}

		public IRetrospectivePlayer CreatePlayerForPlan(IList<IRetroPlaylistItem> initialPlaylist)
		{
			return new RetrospectivePlayer(initialPlaylist, timerProvider);
		}
	}
}
