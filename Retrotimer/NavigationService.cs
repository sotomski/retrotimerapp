﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Retrotimer.View;
using Xamarin.Forms;

namespace Retrotimer
{
    /// <summary>
    /// Implementation of <c>INavigationService</c> meant to be used in View Models 
    /// to control navigation between pages.
    /// </summary>
    public class NavigationService : INavigationService
    {
        readonly INavigation navigation;
        readonly Application currentApp;

        public NavigationService(INavigation navigation, Application app)
        {
            this.navigation = navigation;
            currentApp = app;
        }

        public Task PushModalAsync(NavigationPages page, bool animated, INotifyPropertyChanged bindingContext = null)
        {
            var destPage = GetPage(page);
            if (bindingContext != null)
            {
                destPage.BindingContext = bindingContext;
            }

            return navigation.PushModalAsync(destPage, animated);
        }

        /// <summary>
        /// Gets the Xamarin.Forms page based on the provided page enum
        /// </summary>
        /// <returns>The Forms page instance.</returns>
        /// <param name="pageName">Page enum.</param>
        Page GetPage(NavigationPages pageName)
        {
            switch (pageName)
            {
                case NavigationPages.InfoPage:
                    var newView = new AboutView();
                    return new NavigationPage(newView);

                case NavigationPages.BrowseStagePage:
                    return new StageBrowserView();

                case NavigationPages.StartPage:
                    return new StartView();

                case NavigationPages.RetroPlanPage:
                    return new RetroPlanView();

                default:
                    throw new ArgumentException("Unknown navigation page value.", nameof(pageName));
            }
        }

        public async Task PopModalAsync()
        {
            await navigation.PopModalAsync();
        }

        public Task PushAsync(NavigationPages page, bool animated, INotifyPropertyChanged bindingContext = null)
        {
            var destPage = GetPage(page);
            if (bindingContext != null)
            {
                destPage.BindingContext = bindingContext;
            }

            return navigation.PushAsync(destPage, animated);
        }

        public async Task PopAsync()
        {
            await navigation.PopAsync();
        }
    }
}
