﻿using System.Collections.Generic;

namespace Retrotimer.Services.Analytics
{
    public class ActivityRefreshFailEvent : IAnalyticsEvent
    {
        public string Name { get; }
        public string ErrorType { get; set; }
        public string Message { get; set; }

        public ActivityRefreshFailEvent()
        {
            Name = "StartSynchronizationFailed";
        }
        
        public IDictionary<string, string> ToProperties()
        {
            return new Dictionary<string, string>
            {
                { "Message", Message },
                { "ErrorType", ErrorType }
            };
        }
    }
}
