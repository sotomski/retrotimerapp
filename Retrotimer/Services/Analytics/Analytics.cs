﻿using System;

namespace Retrotimer.Services.Analytics
{
    public abstract class Analytics
    {
        public static IAnalytics Instance { get; private set; }

        public static void OverrideDefaultInstance(IAnalytics analytics)
        {
            Instance = analytics;
        }

        public static void Reset()
        {
            Instance = null;
        }
    }
}
