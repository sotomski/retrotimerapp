﻿namespace Retrotimer.Services.Analytics
{
    public interface IAnalytics
    {
        bool IsEnabled { get; set; }
        void LogEvent(IAnalyticsEvent analyticsEvent);
    }
}
