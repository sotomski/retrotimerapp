﻿using System.Collections.Generic;

namespace Retrotimer.Services.Analytics
{
    public interface IAnalyticsEvent
    {
        string Name { get; }
        IDictionary<string, string> ToProperties();
    }
}
