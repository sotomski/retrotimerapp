﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Azure.Mobile;
using Microsoft.Azure.Mobile.Crashes;
using AzureAnalytics = Microsoft.Azure.Mobile.Analytics.Analytics;
using Retrotimer.Common;

namespace Retrotimer.Services.Analytics
{
    public sealed class MobileCenterAnalytics : IAnalytics
    {
        //https://docs.microsoft.com/en-us/mobile-center/analytics/handled-errors-events
        const int MaxStringLength = 64;
        const string KeyForiOS = "YourMobileCenteriOSSecret";

        public bool IsEnabled
        {
            get => MobileCenter.Enabled;
            set => MobileCenter.Enabled = value;
        }

        public MobileCenterAnalytics()
        {
			IsEnabled = false;

            var key = $"ios={KeyForiOS};";
            MobileCenter.Start(
                key,
                typeof(AzureAnalytics), typeof(Crashes));
        }

        public void LogEvent(IAnalyticsEvent analyticsEvent)
        {
            var name = analyticsEvent.Name;
            var props = analyticsEvent
                .ToProperties()
                .Select(pair =>
                    new KeyValuePair<string, string>(
                        pair.Key.Truncate(MaxStringLength), 
                        pair.Value.Truncate(MaxStringLength)))
                .ToDictionary(p => p.Key, p => p.Value);
            
            AzureAnalytics.TrackEvent(name, props);
        }
    }
}
