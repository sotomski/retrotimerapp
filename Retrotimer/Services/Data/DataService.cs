﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Retrotimer.Data;
using Retrotimer.Model;

namespace Retrotimer.Services.Data
{
    public class DataService : IDataService
    {
        private readonly IDataRepository localRepo;
        private readonly IDataSource remoteSource;
        private IEnumerable<Activity> inMemoryActivityBuffer;
        
        public event EventHandler<EventArgs<IEnumerable<Activity>>> NewActivitiesAvailable = delegate { };
        
        public DataService(IDataRepository localRepo, IDataSource remoteSource)
        {
            this.localRepo = localRepo ?? throw new ArgumentNullException(nameof(localRepo));
            this.remoteSource = remoteSource ?? throw new ArgumentNullException(nameof(remoteSource));
            inMemoryActivityBuffer = new List<Activity>();
        }
        
        /// <summary>
        /// Reads activities directly from local repository. 
        /// If there are activities available locally, it does not contact the remote source.
        /// If there are no local activities, it tries to download them from the remote source.
        /// If all above attempts failed, it throws an exception.
        /// </summary>
        /// <returns>All available activities.</returns>
        /// <exception cref="NotImplementedException">Thrown when no activities were found 
        /// in any of the available sources.</exception>
        public async Task<IEnumerable<Activity>> ReadActivitiesAsync()
        {
            var activities = inMemoryActivityBuffer;
			Debug.WriteLine(GetType() + $" ReadActivitiesAsync: current memory buffer count: {activities.Count()}.");

            if (!activities.Any())
            {
                activities = await ReadLocalActivities();
                Debug.WriteLine(GetType() + $" ReadActivitiesAsync: loaded local count: {activities.Count()}.");
            }

            var shouldRaiseNewActivitiesAvailable = false;

            if (!activities.Any())
            {
				activities = await ReadRemoteActivities();
				await SaveActivitiesLocally(activities);
                Debug.WriteLine(GetType() + $" ReadActivitiesAsync: loaded remote count: {activities.Count()}.");
                shouldRaiseNewActivitiesAvailable = true;
            }

            inMemoryActivityBuffer = new List<Activity>(activities);

            if (shouldRaiseNewActivitiesAvailable)
            {
				NewActivitiesAvailable(this, new EventArgs<IEnumerable<Activity>>(inMemoryActivityBuffer));
            }

            return activities;
        }

        private async Task<IEnumerable<Activity>> ReadLocalActivities()
        {
            IEnumerable<Activity> localActivities;
            
            try
            {
                Debug.WriteLine(GetType() + " ReadLocalActivities starting...");
                localActivities = await localRepo.ReadAllAsync<Activity>();
                Debug.WriteLine(
                    GetType() + $" ReadLocalActivities successful with {localActivities.Count()} activities.");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(GetType() + " ReadLocalActivities failed with: " + ex.Message);
                localActivities = new List<Activity>();
            }
            
            return localActivities;
        }
        
        private async Task<IEnumerable<Activity>> ReadRemoteActivities()
        {
            IEnumerable<Activity> remoteActivities;
            
            try
            {
                Debug.WriteLine(GetType() + " ReadRemoteActivities starting...");
                remoteActivities = await remoteSource.GetAllAsync<Activity>();
                Debug.WriteLine(
                    GetType() + $" ReadRemoteActivities successful with {remoteActivities.Count()} activities.");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(GetType() + " ReadLocalActivities failed with: " + ex.Message);
                throw new DataServiceException("Fetching activities from remote source failed.", ex);
            }

            return remoteActivities;
        }
        
        private async Task SaveActivitiesLocally(IEnumerable<Activity> activities)
        {
            try
            {
                Debug.WriteLine(GetType() + " Saving activites starting...");
                await localRepo.SaveAllAsync(activities);
                Debug.WriteLine(GetType() + $" Saving activites successful with {activities.Count()} activities.");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(GetType() + " Saving activities failed with: " + ex.Message);
            }
        }

        /// <summary>
        /// Refreshes local activities by downloading them from remote source and storing locally for later.
        /// </summary>
        /// <returns>A collection of fetched activites.</returns>
        /// <exception cref="NotImplementedException">Thrown when remote activities cannot be downloaded.</exception>
        public async Task<IEnumerable<Activity>> RefreshActivitiesAsync()
        {
            var remoteData = await ReadRemoteActivities();
            await SaveActivitiesLocally(remoteData);

            inMemoryActivityBuffer = remoteData;
            NewActivitiesAvailable(this, new EventArgs<IEnumerable<Activity>>(inMemoryActivityBuffer));

            return remoteData;
		}
    }
}
