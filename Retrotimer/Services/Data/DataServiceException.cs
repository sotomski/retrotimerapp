﻿using System;

namespace Retrotimer.Services
{
    public class DataServiceException : Exception
    {
        public DataServiceException(string message, Exception innerException) 
            : base(message, innerException)
        {
            //
        }        
    }
}
