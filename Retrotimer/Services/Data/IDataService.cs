﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Retrotimer.Model;

namespace Retrotimer.Services.Data
{
    public interface IDataService
    {
        event EventHandler<EventArgs<IEnumerable<Activity>>> NewActivitiesAvailable;

        Task<IEnumerable<Activity>> ReadActivitiesAsync();
        Task<IEnumerable<Activity>> RefreshActivitiesAsync();
    }
}
