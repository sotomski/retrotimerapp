using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Reflection;

namespace Retrotimer.Services
{
    public class EmbeddedResources : IEmbeddedResources
	{
        public async Task<Stream> OpenStreamToResource(string resourceId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await Task.Factory.StartNew(() =>
            {
                var assembly = typeof(EmbeddedResources).GetTypeInfo().Assembly;
                Stream stream = assembly.GetManifestResourceStream(resourceId);
                return stream;
            });
        }
	}
}
