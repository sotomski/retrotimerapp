﻿using System.Threading.Tasks;

namespace Retrotimer.Services
{
	public enum Sounds
	{
		Chime,
		DoubleChime
	}

	public interface IAudioPlayer
	{
		void PlaySound(Sounds sound);
		Task PlaySoundAsync(Sounds sound);
	}
}
