﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Retrotimer.Services
{
    public interface IDialogService
    {
        Task<bool> ShowDialogAsync(
            string message, string title = null, string okText = null, string cancelText = "Cancel", 
            CancellationToken? cancelToken = null);
    }
}
