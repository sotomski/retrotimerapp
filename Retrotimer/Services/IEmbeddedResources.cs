using System.IO;
using System.Threading;
using System.Threading.Tasks;


namespace Retrotimer.Services
{
	public interface IEmbeddedResources
	{
        /// <summary>
        /// Opens the stream to the embedded resource.
        /// </summary>
        /// <returns>The stream to resource.</returns>
        /// <param name="resourceId">Resource identifier.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        Task<Stream> OpenStreamToResource(string resourceId, CancellationToken cancellationToken = default(CancellationToken));
	}
}
