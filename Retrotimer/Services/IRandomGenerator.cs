﻿namespace Retrotimer.Services
{
	public interface IRandomGenerator
	{
		/// <summary>
		/// Gets the next random number from the predefined range.
		/// </summary>
		/// <returns>The next.</returns>
		int GetInt(int min, int max);
	}
}
