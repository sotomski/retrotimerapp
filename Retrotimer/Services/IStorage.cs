﻿using PCLStorage;

namespace Retrotimer.Services
{
	public interface IStorage
	{
		/// <summary>
		/// Gets the file system abstraction from PCLStorage library.
		/// </summary>
		/// <value>The file system.</value>
		IFileSystem FileSystem { get; }

		/// <summary>
		/// Gets my own abstraction of accessing resources embedded in the assembly. This is also PCL-friendly.
		/// </summary>
		/// <value>The embedded resources.</value>
		IEmbeddedResources EmbeddedResources { get; }
	}
}

