﻿namespace Retrotimer.Services
{
	/// <summary>
	/// Message communication protocol contains definitions of all messages
	/// sent by message bus clients.
	/// </summary>
	public static class MessageCommunicationProtocol
	{
		// Activity view model
		public const string TitleTappedMessage = "ActivityViewModel.TitleTapped";
		public const string FocusNavigationRequest = "ActivityViewModel.FocusNavigation";

		public const string CountdownStageChanged = "IRetrospectivePlayer.CountdownStageChanged";
		public const string CountdownCompleted = "IRetrospectivePlayer.CountdownCompleted";

		// App lifecycle
		public const string AppDidStart = "Application.OnStart";
		public const string AppWillSleep = "Application.OnSleep";
		public const string AppDidResume = "Application.OnResume";
	}
}
