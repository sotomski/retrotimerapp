﻿using System;

namespace Retrotimer.Services
{
    public class RandomGenerator : IRandomGenerator
    {
        readonly Random rand;

        public RandomGenerator()
        {
            rand = new Random((int)DateTime.Now.Ticks);
        }
            
        public int GetInt(int min, int max)
        {
            return rand.Next(min, max);
        }
    }
}

