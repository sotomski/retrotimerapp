﻿namespace Retrotimer.Services.Settings
{
    public interface ISettings
    {
        bool IsCleanAppStart { get; set; }
        bool IsAnalyticsAllowed { get; set; }
    }
}
