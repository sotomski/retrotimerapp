﻿using System;
using Plugin.Settings;

namespace Retrotimer.Services.Settings
{
    public class XamarinSettings : ISettings
    {
        /// <summary>
        /// The key specifying if analytics are allowed.
        /// </summary>
        /// <remarks>
        /// Also used in iOS Settings.bundle.
        /// </remarks>
        const string KeyIsAnalyticsAllowed = "key_is_analytics_allowed";
        const string KeyIsCleanAppStart = "key_is_clean_app_start";

        static Plugin.Settings.Abstractions.ISettings Settings => CrossSettings.Current;

        public bool IsAnalyticsAllowed
        {
            get
            {
                return Settings.GetValueOrDefault(KeyIsAnalyticsAllowed, false);
            }    
            set 
            {
                Settings.AddOrUpdateValue(KeyIsAnalyticsAllowed, value);
            }
        }

        public bool IsCleanAppStart 
        {
            get
            {
                return Settings.GetValueOrDefault(KeyIsCleanAppStart, true);
            }
            set
            {
               Settings.AddOrUpdateValue(KeyIsCleanAppStart, value); 
            }
        }
    }
}
