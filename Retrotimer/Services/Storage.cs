﻿using PCLStorage;

namespace Retrotimer.Services
{
	public class Storage : IStorage
	{
		readonly EmbeddedResources embeddedRes;

		/// <summary>
		/// Gets the file system abstraction from PCLStorage library.
		/// </summary>
		/// <value>The file system.</value>
		public IFileSystem FileSystem
		{
			get
			{
				return PCLStorage.FileSystem.Current;
			}
		}

		/// <summary>
		/// Gets my own abstraction of accessing resources embedded in the assembly. This is also PCL-friendly.
		/// </summary>
		/// <value>The embedded resources.</value>
		public IEmbeddedResources EmbeddedResources
		{
			get
			{
				return embeddedRes;
			}
		}

		public Storage()
		{
			embeddedRes = new EmbeddedResources();
		}
	}
}
