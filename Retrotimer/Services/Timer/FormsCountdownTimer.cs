﻿using System;

namespace Retrotimer.Services.Timer
{
    public class FormsCountdownTimer : ICountdownTimer
	{
		TimeSpan remainingTime = TimeSpan.Zero;
		bool isStopRequested;
		bool isPauseRequested;

		public bool IsPaused => isPauseRequested;

		/// <summary>
		/// Starts the countdown if no countdowns in progress. Does nothing otherwise.
		/// </summary>
		/// <returns>The countdown.</returns>
		/// <param name="countdownInfo">Countdown info.</param>
		public void StartCountdown(CountdownInfo countdownInfo)
		{
			// Do not allow multiple countdowns in one timer instance.
			if (remainingTime != TimeSpan.Zero)
				return;

			remainingTime = countdownInfo.Countdown;

			Xamarin.Forms.Device.StartTimer(countdownInfo.UpdateInterval, () =>
			{
				if (isStopRequested)
				{
					// Stop means stop - no discussion
					ResetTimer();
					return false;
				}

				bool shouldEndCountdown = false;

				// No updates if timer is paused - internal timer keeps ticking
				if (!isPauseRequested)
				{
					// Update remaining time
					remainingTime -= countdownInfo.UpdateInterval;
					shouldEndCountdown = (remainingTime <= TimeSpan.Zero);

					if (!shouldEndCountdown)
					{
						if (countdownInfo.UpdateRemainingTimeCallback != null)
							countdownInfo.UpdateRemainingTimeCallback(remainingTime);
					}
					else
					{
						countdownInfo.FinishCallback();
						ResetTimer();
					}
				}

				// Timer runs until false is returned.
				return !shouldEndCountdown;
			});
		}

		void ResetTimer()
		{
			remainingTime = TimeSpan.Zero;
			isStopRequested = false;
		}

		public void Stop()
		{
			// This should only work when Start has been inovked (setting remainingTime).
			isStopRequested |= remainingTime >= TimeSpan.Zero;
		}

		public void Pause()
		{
			isPauseRequested = true;
		}

		public void Resume()
		{
			isPauseRequested = false;
		}
	}
}
