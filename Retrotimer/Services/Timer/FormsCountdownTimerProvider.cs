﻿﻿namespace Retrotimer.Services.Timer
{
    public class FormsCountdownTimerProvider : ICountdownTimerProvider
	{
		public ICountdownTimer CreateTimer()
		{
			return new FormsCountdownTimer();
		}
	}
}
