using System;

namespace Retrotimer.Services.Timer
{
	/// <summary>
	/// Countdown info for the timer.
	/// </summary>
	public struct CountdownInfo
	{
		/// <summary>
		/// Timespan from which the countdown should start.
		/// </summary>
		public TimeSpan Countdown;

		/// <summary>
		/// Interval for invoking update callback.
		/// </summary>
		public TimeSpan UpdateInterval;

		/// <summary>
		/// Update callback invoked every update inerval. Takes remaining time as an argument.
		/// </summary>
		public Action<TimeSpan> UpdateRemainingTimeCallback;

		/// <summary>
		/// Callback invoked when the countdown is finished.
		/// </summary>
		public Action FinishCallback;
	}

	public interface ICountdownTimer
	{
		/// <summary>
		/// Gets a value indicating whether this <see cref="T:Retrotimer.Services.Timer.ICountdownTimer"/> is paused.
		/// </summary>
		/// <value><c>true</c> if is paused (with active countdown); otherwise, <c>false</c>.</value>
		bool IsPaused { get; }

		/// <summary>
		/// Starts the countdown timer.
		/// </summary>
		void StartCountdown(CountdownInfo countdownInfo);

		/// <summary>
		/// Stop the countdown timer.
		/// </summary>
		void Stop();

		/// <summary>
		/// Pauses the countdown timer.
		/// Has no effect if invoked on an already paused timer.
		/// </summary>
		/// <remarks>
		/// While paused, timer does not invoke any callbacks. Also, remaining time is not updated.
		/// It is however possible, that the underlying timer keeps running.
		/// </remarks>
		void Pause();

		/// <summary>
		/// Resumes the countdown from paused state. 
		/// Has no effect if invoked on a non-paused timer.
		/// </summary>
		void Resume();
	}
	
}
