namespace Retrotimer.Services.Timer
{
    public interface ICountdownTimerProvider
	{
		ICountdownTimer CreateTimer();
	}
	
}
