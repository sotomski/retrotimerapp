﻿using System.ComponentModel;
using Retrotimer.ViewModel;
using Xamarin.Forms;

namespace Retrotimer.View
{
	public partial class ActivityBasicViewCell : ViewCell
	{
		Color textColorWhenNotSelected = Color.Transparent;

		public ActivityBasicViewCell()
		{
			InitializeComponent();

			BindingContextChanged += (_, e) => 
			{
				var context = BindingContext as INotifyPropertyChanged;
                if (context != null)
                {
                    context.PropertyChanged += (sender, ee) => Context_PropertyChanged(sender);

                    Context_PropertyChanged(context);
                }
			};
		}

		void Context_PropertyChanged(object sender)
		{
			var binding = sender as ISelectable;

			// Note: Direct assignment to TextColor breaks Xamarin Forms
			// style set in XAML using binding.

			if (binding.IsSelected)
			{
				textColorWhenNotSelected = lblId.TextColor;
				lblId.TextColor = lblName.TextColor = Color.Black;
			}
			else if(textColorWhenNotSelected != Color.Transparent)
			{
				lblId.TextColor = lblName.TextColor = textColorWhenNotSelected;
			}
		}
	}
}
