﻿using System;
using Xamarin.Forms;
using System.Text;

namespace Retrotimer.View
{
	/// <summary>
	/// Css style descriptor.
	/// Use this class to inject font and color settings into the HTML generated
	/// for displaying the content of retro activities.
	/// </summary>
	public class CssStyleDescriptor
	{
		const int ColorFactor = 255;

		public Color TextColor = Color.Black;
		public string FontFamily = "Helvetica Neue";
		public int FontSize = 12;
		public Color BackgroundColor = Color.White;

		/// <summary>
		/// Generates the css style without the <c><style></c> markup.
		/// </summary>
		/// <returns>The css style.</returns>
		public override string ToString()
		{
			var css = new StringBuilder();
			css.Append("body{");
			css.Append("width:100%;");
			css.Append("margin:0px;");
			css.Append("white-space:wrap;");
			css.AppendFormat("color:#{0,2:X2}{1,2:X2}{2,2:X2};", GetColorComponent(TextColor.R),
				GetColorComponent(TextColor.G), GetColorComponent(TextColor.B));
			css.AppendFormat("background-color:#{0,2:X2}{1,2:X2}{2,2:X2};", GetColorComponent(BackgroundColor.R),
				GetColorComponent(BackgroundColor.G), GetColorComponent(BackgroundColor.B));
			css.AppendFormat("font-family:\"{0}\";", FontFamily);
			css.AppendFormat("font-size:{0}px;", FontSize);
			css.Append("}");

			return css.ToString();
		}

		/// <summary>
		/// Converts color component from range [0-1] to [0-255].
		/// </summary>
		/// <returns>The color component to be converted.</returns>
		/// <param name="colorComponent">Converted color component.</param>
		static byte GetColorComponent(double colorComponent)
		{
			byte ret;
			try
			{
				ret = Convert.ToByte(ColorFactor * colorComponent);
			}
			catch
			{
				ret = 255;
			}

			return ret;
		}
	}

	public class ActivityHtmlFormatter
	{
		public HtmlWebViewSource FormatHtmlWithDescription(string description, CssStyleDescriptor style)
		{
			var html = new HtmlWebViewSource
			{
				Html = FormatHtml(description, style)
			};

			return description == null ? null : html;
		}

		public static string FormatHtml(string description, CssStyleDescriptor cssStyleDescriptor)
		{
			var html = new StringBuilder();
			html.Append("<!DOCTYPE html>");
			html.Append("<html>");

			if (cssStyleDescriptor != null)
			{
				html.Append("<style type=\"text/css\">");
				html.Append(cssStyleDescriptor.ToString());
				html.Append("</style>");
			}

			html.Append("<head>");
			html.Append("<meta name=\"viewport\" content=\"width=device-width,height=device-height,user-scalable=no,initial-scale=1.0\"/>");
			html.Append("</head>");
			html.Append("<body>");
			html.Append(description);
			html.Append("</body>");
			html.Append("</html>");
			html.Append("</!DOCTYPE>");

			return html.ToString();
		}
	}
}
