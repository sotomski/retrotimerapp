﻿using GalaSoft.MvvmLight.Messaging;
using Retrotimer.Services;
using Xamarin.Forms;

namespace Retrotimer.View
{
	public partial class ActivityView : ContentView
    {
		public ActivityView()
        {
            InitializeComponent();

			Messenger.Default.Register<NotificationMessage>(this, (msg) =>
			{
				if(msg.Sender == BindingContext
				   && msg.Notification == MessageCommunicationProtocol.FocusNavigationRequest)
				{
					FocusNavigation();
				}
			});
        }

		void FocusNavigation()
		{
			picker.Focus();
		}
	}
}