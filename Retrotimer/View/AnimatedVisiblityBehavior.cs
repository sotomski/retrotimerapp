﻿using Xamarin.Forms;

namespace Retrotimer.View
{
	public class AnimatedVisiblityBehavior : Behavior<VisualElement>
	{
		protected override void OnAttachedTo(VisualElement bindable)
		{
			base.OnAttachedTo(bindable);
			bindable.PropertyChanging += OnVisualElementPropertyChanging;
		}

		protected override void OnDetachingFrom(VisualElement bindable)
		{
			bindable.PropertyChanging -= OnVisualElementPropertyChanging;
			base.OnDetachingFrom(bindable);
		}

		protected void OnVisualElementPropertyChanging(object sender, PropertyChangingEventArgs e)
		{
			if (e.PropertyName == nameof(VisualElement.IsVisible))
			{
				var element = sender as VisualElement;
				if (element.IsVisible)
				{
					// Element is disappearing
					element.FadeTo(0.0, 250, Easing.Linear);
				}
				else
				{
					// Element is appearing
					element.FadeTo(1.0, 250, Easing.Linear);
				}
			}
		}
	}
}
