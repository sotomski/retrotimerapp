﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Retrotimer.View
{
	public class InvertBoolConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				var input = (bool)value;
				return !input;
			}
			catch
			{
				// Something has to be wrong with the value.
				throw new ArgumentOutOfRangeException(nameof(value));
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new InvalidOperationException("This method is not supported.");
		}
	}
}
