﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Retrotimer.Model;
using Xamarin.Forms;

namespace Retrotimer.View
{
	public class RetroPhaseToStringConverter : IValueConverter
	{
		static readonly Dictionary<RetroPhase, string> PhaseWordingMap = new Dictionary<RetroPhase, string>
		{
			{ RetroPhase.SetTheStage, Wording.SetTheStage },
			{ RetroPhase.GatherData, Wording.GatherData },
			{ RetroPhase.GenerateInsights, Wording.GenerateInsights },
			{ RetroPhase.DecideWhatToDo, Wording.DecideWhatToDo },
			{ RetroPhase.CloseTheRetrospective, Wording.CloseTheRetrospective }
		};

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(!(value is RetroPhase))
				throw new ArgumentException("value has to be RetroPhase", nameof(value));

			var phase = (RetroPhase)value;
			string prettyName;
			return PhaseWordingMap.TryGetValue(phase, out prettyName) ? prettyName : phase.ToString();
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
