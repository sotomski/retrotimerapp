﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Retrotimer.View
{
	public class TimeSpanToMinutesConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(value == null || !(value is TimeSpan))
				throw new ArgumentException("Value of TimeSpan is expected.", nameof(value));
			
			var timespan = (TimeSpan)value;
			return Math.Truncate(timespan.TotalMinutes);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new InvalidOperationException();
		}
	}
}

