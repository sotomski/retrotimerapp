﻿namespace Retrotimer.View
{
	using System;
	using Xamarin.Forms;
	using ViewModel;
	using Vsm = StyleManager;
	using System.Globalization;

	public class VisualStyleToLabelStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(targetType != typeof(Style))
            {
                throw new ArgumentException("Target type has to be a Style");
            }

            if (!(value is VisualStyle))
            {
                throw new ArgumentException("Value has to be a visual style");
            }
            var style = (VisualStyle)value;

            // Parse label style.
            Vsm.LabelStyle labelType;
            if ((parameter is Vsm.LabelStyle))
            {
                labelType = (Vsm.LabelStyle)parameter;
            }
            else if (!Enum.TryParse(parameter as string, out labelType))
            {
                throw new ArgumentException("Parameter has to be a LabelStyle value");
            }

            bool isDarkStyle = style == VisualStyle.LightBlue || style == VisualStyle.LightOrange;

            return Vsm.GetLabelStyle(labelType, !isDarkStyle);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class VisualStyleToViewStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(targetType != typeof(Style))
            {
                throw new ArgumentException("Target type has to be a Style");
            }

            if (!(value is VisualStyle))
            {
                throw new ArgumentException("Value has to be a visual style");
            }

            var style = (VisualStyle)value;

            var result = Vsm.GetViewStyle(style);

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

	public class VisualStyleToCountdownPickerConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (targetType != typeof(Style))
			{
				throw new ArgumentException("Target type has to be a Style");
			}

			if (!(value is VisualStyle))
			{
				throw new ArgumentException("Value has to be a visual style");
			}

			var style = (VisualStyle)value;
			bool isDarkStyle = style == VisualStyle.LightBlue || style == VisualStyle.LightOrange;

			var result = Vsm.GetCountdownPickerStyle(style, !isDarkStyle);

			return result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	public class VisualStyleToStyleDescriptorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(targetType != typeof(CssStyleDescriptor))
            {
                throw new ArgumentException("Target type has to be a Css style descriptor");
            }

            if(!(value is VisualStyle))
            {
                throw new ArgumentException("Value has to be a visual style");
            }

            var style = (VisualStyle)value;
            bool isDarkStyle = style == VisualStyle.LightBlue || style == VisualStyle.LightOrange;

            return Vsm.GetCssStyleDescriptor(style, !isDarkStyle);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
