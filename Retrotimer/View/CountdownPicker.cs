﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Xamarin.Forms;

namespace Retrotimer.View
{
	public class CountdownPicker : Xamarin.Forms.View
	{
		public static readonly BindableProperty TimeProperty = BindableProperty.Create(nameof(TimeProperty), typeof(TimeSpan), typeof(CountdownPicker), TimeSpan.Zero, BindingMode.TwoWay, null, null, null, null, null);

		public static readonly BindableProperty TextColorProperty = BindableProperty.Create(nameof(TextColorProperty), typeof(Color), typeof(CountdownPicker), Color.Black, BindingMode.TwoWay, null, null, null, null, null);

		public IList<ToolbarItem> InputAccessoryToolbarItems { get; private set; }

		public TimeSpan Time
		{
			get
			{
				return (TimeSpan)GetValue(TimeProperty);
			}
			set
			{
				SetValue(TimeProperty, value);
			}
		}

		public Color TextColor
		{
			get
			{
				return (Color)GetValue(TextColorProperty);
			}
			set
			{
				SetValue(TextColorProperty, value);
			}
		}

		public CountdownPicker ()
		{	
			var toolbar = new ObservableCollection<ToolbarItem>();
			toolbar.CollectionChanged += OnAccessoryViewToolbarCollectionChanged;
			InputAccessoryToolbarItems = toolbar;
		}

		void OnAccessoryViewToolbarCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
		{
			if (args.Action != NotifyCollectionChangedAction.Add)
				return;
			foreach (Element item in args.NewItems)
			{
				item.Parent = this;
				item.BindingContext = BindingContext;
			}
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();

			foreach (var item in InputAccessoryToolbarItems)
			{
				item.BindingContext = BindingContext;
			}
		}
	}
}
