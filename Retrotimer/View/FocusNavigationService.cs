﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Retrotimer.View
{
	public interface IFocusNavigatable
	{
		event EventHandler<EventArgs<IFocusNavigatable>> FocusPreviousRequested;
		event EventHandler<EventArgs<IFocusNavigatable>> FocusNextRequested;

		int FocusNavigationIndex { get; set; }

		void FocusNavigation();
	}

	public interface IFocusNavigationService
	{
		// You don't do much here...
		// I am seriously having a feeling that I could have handled 
		// the code design a lot better in this case.
		// Opted for consistency here.
	}

	public class FocusNavigationService : IFocusNavigationService
	{
		readonly List<IFocusNavigatable> items;

		public FocusNavigationService(IList<IFocusNavigatable> items)
		{
			if(items == null)
				throw new ArgumentNullException();

			var uniqueIndexes = items.Select(i => i.FocusNavigationIndex).Distinct().Count();
			if (uniqueIndexes != items.Count)
				throw new ArgumentException("FocusNavigationIndex has to be unique.");

			foreach (var item in items)
			{
				item.FocusNextRequested += OnFocusNextRequested;
				item.FocusPreviousRequested += OnFocusPrevRequested;
			}

			this.items = items.OrderBy((i) => i.FocusNavigationIndex).ToList();
		}

		void OnFocusPrevRequested(object sender, EventArgs<IFocusNavigatable> args)
		{
			if (items.Count == 1)
			{
				args.Value.FocusNavigation();
			}
			else if (items.Count > 1)
			{
				var current = args.Value;
				items.Last(i => current == items.First() ? i == items.Last() 
				           : i.FocusNavigationIndex < current.FocusNavigationIndex)
				     .FocusNavigation();
			}
		}

		void OnFocusNextRequested(object sender, EventArgs<IFocusNavigatable> args)
		{
			if(items.Count == 1)
			{
				args.Value.FocusNavigation();
			}
			else if(items.Count > 1)
			{
				var current = args.Value;
				items.First(i => current == items.Last() ? i == items.First() 
				            : i.FocusNavigationIndex > current.FocusNavigationIndex)
				     .FocusNavigation();
			}
		}
	}
}
