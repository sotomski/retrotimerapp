﻿using System.Collections.Generic;

namespace Retrotimer.View
{
	public interface IFocusNavigationServiceProvider
	{
		IFocusNavigationService CreateFocusNavigationService(IList<IFocusNavigatable> navigatables);
	}

	public class FocusNavigationServiceProvider : IFocusNavigationServiceProvider
	{
		public IFocusNavigationService CreateFocusNavigationService(IList<IFocusNavigatable> navigatables)
		{
			return new FocusNavigationService(navigatables);
		}
	}
}

