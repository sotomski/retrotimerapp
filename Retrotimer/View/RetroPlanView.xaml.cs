﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using Retrotimer.Services;
using Retrotimer.ViewModel;
using Xamarin.Forms;

namespace Retrotimer.View
{
	public partial class RetroPlanView
	{
		const double OverlayVisibilityOn = 0.75;
		const double OverlayVisibilityOff = 0.0;
		bool isFirstStart = true;

		RetroPlanViewModel ViewModel => BindingContext as RetroPlanViewModel;

		public RetroPlanView()
		{
			InitializeComponent();

			NavigationPage.SetBackButtonTitle(this, Wording.Back);
		}

		protected override void OnBindingContextChanged()
		{
			ViewModel.PropertyChanged -= ViewModel_PropertyChanged;

			base.OnBindingContextChanged();

			ViewModel.PropertyChanged += ViewModel_PropertyChanged;
		}

		void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(RetroPlanViewModel.CurrentlyPlayedActivity))
			{
				UpdateActivityOverlay();
			}
		}

		private void UpdateActivityOverlay()
		{
			var currentlyPlayedActivity = ViewModel.CurrentlyPlayedActivity;
			var currentlyPlayedView = GetActivityViewFrom(currentlyPlayedActivity);

			if (currentlyPlayedView != null)
			{
				SetOverlayVisibilityTo(true);
				MoveOverlayToHighlight(currentlyPlayedView.Bounds);
			}
			else
			{
				SetOverlayVisibilityTo(false);
				MoveOverlayToHighlight(SetTheStageView.Bounds, false);
			}
		}

		ActivityView GetActivityViewFrom(ActivityFullViewModel currentlyPlayedActivity)
		{
			if (currentlyPlayedActivity == null)
			{
				return null;
			}
			
			var retroViews = new List<ActivityView>
			{
				SetTheStageView,
				GatherDataView,
				GenerateInsightsView,
				DecideWhatToDoView,
				CloseTheRetrospectiveView
			};

			return retroViews.FirstOrDefault(v => v.BindingContext == currentlyPlayedActivity);
		}

		void SetOverlayVisibilityTo(bool shouldBeVisible)
		{
			var opactiy = shouldBeVisible ? OverlayVisibilityOn : OverlayVisibilityOff;

			OverlayTop.FadeTo(opactiy);
			OverlayBottom.FadeTo(opactiy);
		}

		void MoveOverlayToHighlight(Rectangle bounds, bool animated = true)
		{
			var topBounds = new Rectangle(bounds.X, bounds.Top - Height, Width, Height);
			var bottomBounds = new Rectangle(bounds.X, bounds.Bottom, Width, Height);

			if (animated)
			{
				OverlayTop.LayoutTo(topBounds);
				OverlayBottom.LayoutTo(bottomBounds);
			}
			else
			{
				OverlayTop.Layout(topBounds);
				OverlayBottom.Layout(bottomBounds);
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (isFirstStart)
			{
				// Load activities from disk, since the view is almost ready
				ViewModel.LoadRetrospectiveCommand.Execute(null);

				isFirstStart = false;
			}

			Messenger.Default.Register<NotificationMessage>(this, (notif) =>
			{
				if (notif.Notification == MessageCommunicationProtocol.AppWillSleep)
				{
					ViewModel.SaveRetroPlanToDiskCommand.Execute(null);
				}
			});

			UpdateActivityOverlay();
		}

		protected override void OnDisappearing()
		{
			Messenger.Default.Unregister(this);

			ViewModel.SaveRetroPlanToDiskCommand.Execute(null);

			base.OnDisappearing();
		}
	}
}
