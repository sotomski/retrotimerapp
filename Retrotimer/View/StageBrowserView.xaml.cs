﻿using Retrotimer.ViewModel;
using Xamarin.Forms;

namespace Retrotimer.View
{
	public partial class StageBrowserView : ContentPage
	{
        StageBrowserViewModel ViewModel => BindingContext as StageBrowserViewModel;

		public StageBrowserView()
		{
			InitializeComponent();

			ListView.ItemSelected += ListView_ItemSelected;
		}

		void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem != null)
			{
				ListView.ScrollTo(e.SelectedItem, ScrollToPosition.MakeVisible, false);
			}
		}

        protected override void OnAppearing()
        {
	        base.OnAppearing();
	        ViewModel.LoadActivitiesCommand.Execute(null);
        }
	}
}
