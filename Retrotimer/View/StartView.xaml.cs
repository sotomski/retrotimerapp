﻿using System.Threading;
using System.Threading.Tasks;
using Retrotimer.ViewModel;
using Xamarin.Forms;

namespace Retrotimer.View
{
    public partial class StartView : ContentPage
    {
        StartViewModel ViewModel => BindingContext as StartViewModel;

        public StartView()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var offset = -Height * 0.20;

            var mainScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            logo.TranslateTo(0, offset, 250, Easing.Linear)
                .ContinueWith((prev) => message.FadeTo(1.0, 100))
                .ContinueWith(
                    (prev) => ViewModel.SynchronizeCommand.Execute(null), 
                    CancellationToken.None, 
                    TaskContinuationOptions.None, 
                    mainScheduler);
        }

        protected override void OnDisappearing()
        {
            Navigation.RemovePage(this);

            base.OnDisappearing();
        }
    }
}
