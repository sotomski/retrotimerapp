﻿namespace Retrotimer.View
{
	using Xamarin.Forms;
	using System.Collections.Generic;
	using ViewModel;

	public static class StyleManager
	{
		public enum LabelStyle
		{
			Micro,
			Normal,
			NormalBold,
			Large,
			Huge
		}

		static readonly Dictionary<VisualStyle, Color> colors = new Dictionary<VisualStyle, Color>
		{
			{ VisualStyle.LightBlue, Color.FromHex("#B9E7E7") },
			{ VisualStyle.LightOrange, Color.FromHex("#D3B256") },
			{ VisualStyle.Blue, Color.FromHex("#017F7C") },
			{ VisualStyle.DarkBlue, Color.FromHex("#023838") },
			{ VisualStyle.DarkOrange, Color.FromHex("#AF7500") }
		};

		static readonly Color LightTextColor = Color.FromHex("#FFFFFF");
		static readonly Color DarkTextColor = Color.FromHex("#000000");

		static readonly Dictionary<LabelStyle, int> fontSizes = new Dictionary<LabelStyle, int>
		{
			{ LabelStyle.Micro, 11 },
			{ LabelStyle.Normal, 13 },
			{ LabelStyle.NormalBold, 13 },
			{ LabelStyle.Large, 17 },
			{ LabelStyle.Huge, 21 }
		};

		// For now, iOS only
		const string FontFamily = "-apple-system";

		public static Style GetLabelStyle(LabelStyle labelStyle = LabelStyle.Micro, bool shouldBeLight = false)
		{
			var style = new Style(typeof(Label))
			{
				Setters =
				{
					new Setter { Property = Label.FontSizeProperty, Value = fontSizes[labelStyle] },
					new Setter { Property = Label.TextColorProperty, Value = shouldBeLight ? LightTextColor : DarkTextColor }
				}
			};

			// Bold font?
			if (labelStyle == LabelStyle.NormalBold || labelStyle == LabelStyle.Large || labelStyle == LabelStyle.Huge)
			{
				style.Setters.Add(new Setter { Property = Label.FontAttributesProperty, Value = FontAttributes.Bold });
			}

			return style;
		}

		public static Style GetViewStyle(VisualStyle style)
		{
            return new Style(typeof(View))
			{
				Setters =
				{
					new Setter { Property = VisualElement.BackgroundColorProperty, Value = colors[style] }
				}
			};
		}

		public static Style GetCountdownPickerStyle(VisualStyle style, bool shouldBeLight = false)
		{
			return new Style(typeof(CountdownPicker))
			{
				Setters =
				{
					new Setter { Property = CountdownPicker.TextColorProperty, Value = shouldBeLight ? LightTextColor : DarkTextColor }
				}
			};
		}

		public static CssStyleDescriptor GetCssStyleDescriptor(VisualStyle style, bool shouldBeLight = false)
		{
			return new CssStyleDescriptor
			{
				BackgroundColor = colors[style],
				FontFamily = FontFamily,
				FontSize = fontSizes[LabelStyle.Normal],
				TextColor = shouldBeLight ? LightTextColor : DarkTextColor
			};
		}
	}
}
