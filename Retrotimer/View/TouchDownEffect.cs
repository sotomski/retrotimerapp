﻿using System;
using Xamarin.Forms;

namespace Retrotimer.View
{
	public class TouchDownEffect : RoutingEffect
	{
		public Color HighlightColor { get; set; }

		public TouchDownEffect() : base("dev.sotomski.TouchDownEffect")
		{
		}
	}
}
