﻿using System.Windows.Input;
using Xamarin.Forms;
using System;
using Retrotimer.Common;

namespace Retrotimer.ViewModel
{
    public class AboutViewModel : ViewModelBase
    {
        readonly INavigationService navService;
        readonly IVersionNumberProvider verProvider;
        readonly IDevice device;

        public string VersionNumber
        {
            get
            {
                return verProvider.GetAppVersionNumber();
            }
        }

        public VisualStyle CurrentVisualStyle
        {
            get => VisualStyle.LightBlue;
        }
        
        public ICommand CloseCommand
        {
            get;
            private set;
        }

        public ICommand OpenUriCommand
        {
            get;
            private set;
        }

        public AboutViewModel(INavigationService navigationService, IVersionNumberProvider versionProvider, IDevice device)
        {
            navService = navigationService;
            verProvider = versionProvider;
            this.device = device;
            
            CloseCommand = new Command(CloseCommand_Execute);
            OpenUriCommand = new Command<string>((stringUri) => {
                var uri = new Uri(stringUri);
                OpenUriCommand_Execute(uri);
            });
        }

        void CloseCommand_Execute()
        {
            navService.PopModalAsync();
        }

        void OpenUriCommand_Execute(Uri uriToOpen)
        {
            device.LaunchUriAsync(uriToOpen);
        }
    }
}
