﻿using System.Net;
using Retrotimer.Model;

namespace Retrotimer.ViewModel
{
	public class ActivityBasicViewModel : ViewModelBase, IStyleable
	{
		protected readonly Activity model;
		
		public ActivityBasicViewModel(Activity activity)
		{
			model = activity;
		}

        public Activity Model => model;

		public int Id
		{
			get
			{
				return model.ID;
			}
		}

		public string Name
		{
			get
			{
				return WebUtility.HtmlDecode(model.Name);
			}
		}

		VisualStyle visualStyle;
		public VisualStyle CurrentVisualStyle
		{
			get
			{
				return visualStyle;
			}
			set
			{
				if (value != visualStyle)
				{
					visualStyle = value;
					RaisePropertyChanged(nameof(CurrentVisualStyle));
				}
			}
		}
	}
}
