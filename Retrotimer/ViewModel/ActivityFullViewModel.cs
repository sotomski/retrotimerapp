﻿using System;
using System.Net;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using Retrotimer.Model;
using Retrotimer.Services;
using Retrotimer.View;
using Xamarin.Forms;

namespace Retrotimer.ViewModel
{
	public class ActivityFullViewModel : ActivityBasicViewModel, IRetroPlaylistItem, IFocusNavigatable
	{		
		public int FocusNavigationIndex { get; set; }

		public event EventHandler<EventArgs<IFocusNavigatable>> FocusPreviousRequested;
		public event EventHandler<EventArgs<IFocusNavigatable>> FocusNextRequested;

		public ActivityFullViewModel(ActivityWithTimebox item) : base(item.Activity)
		{
			remainingTimebox = originalTimebox = item.Timebox;

			InitializeCommands();
		}

		public RetroPhase Phase
		{
			get
			{
				return model.Phase;
			}
		}

		bool isEditMode;
		public bool IsEditMode
		{
			get
			{
				return isEditMode;
			}
			set
			{
				if(value != isEditMode)
				{
					isEditMode = value;
					RaisePropertyChanged(nameof(IsEditMode));
				}
			}
		}

		TimeSpan originalTimebox;
		public TimeSpan OriginalTimebox
		{
			get
			{
				return originalTimebox;
			}
			set
			{
				if(value != originalTimebox)
				{
					originalTimebox = value;
					RaisePropertyChanged(nameof(OriginalTimebox));
					RemainingTimebox = value;
				}
			}
		}

		TimeSpan remainingTimebox;
		public TimeSpan RemainingTimebox
		{
			get
			{
				return remainingTimebox;
			}
			set
			{
				if (ValidateRemainingTimeboxInput(value))
				{
					remainingTimebox = value;
					RaisePropertyChanged(nameof(RemainingTimebox));
				}
			}
		}

		bool ValidateRemainingTimeboxInput(TimeSpan input)
		{
			if (input < TimeSpan.Zero)
			{
				throw new ArgumentOutOfRangeException(nameof(input), "Value cannot be lower than TimeSpan.Zero");
			}

			if (input > OriginalTimebox)
			{
				throw new ArgumentOutOfRangeException(nameof(input), "Value cannot be higher than Original timebox");
			}

			return input != remainingTimebox;
		}

		public ICommand ProcessTitleTapCommand
		{
			get;
			private set;
		}

		public ICommand NavigateFocusToPreviousElementCommand
		{
			get;
			private set;
		}

		public ICommand NavigateFocusToNextElementCommand
		{
			get;
			private set;
		}

		void InitializeCommands()
		{
			ProcessTitleTapCommand = new Command(ProcessTitleTapCommandExecute);
			NavigateFocusToPreviousElementCommand = new Command(NavigateFocusToPreviousElementCommandExecute);
			NavigateFocusToNextElementCommand = new Command(NavigateFocusToNextElementCommandExecute);
		}

		void ProcessTitleTapCommandExecute()
		{
			var msg = new NotificationMessage(this, MessageCommunicationProtocol.TitleTappedMessage);
			Messenger.Default.Send(msg);
		}

		void NavigateFocusToPreviousElementCommandExecute()
		{
			FocusPreviousRequested?.Invoke(this, new EventArgs<IFocusNavigatable>(this));
		}

		void NavigateFocusToNextElementCommandExecute()
		{
			FocusNextRequested?.Invoke(this, new EventArgs<IFocusNavigatable>(this));
		}
		
		public void FocusNavigation()
		{
			var msg = new NotificationMessage(this, MessageCommunicationProtocol.FocusNavigationRequest);
			Messenger.Default.Send(msg);
		}

		public override string ToString()
		{
			return $"[ActivityViewModel: Id={Id}, Name={Name}, Phase={Phase}, CurrentVisualStyle={CurrentVisualStyle}, ProcessTapCommand={ProcessTitleTapCommand}]";
		}
	}
}
