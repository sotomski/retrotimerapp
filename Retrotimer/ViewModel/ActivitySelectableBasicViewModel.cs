﻿using Retrotimer.Model;

namespace Retrotimer.ViewModel
{
	public interface ISelectable
	{
		bool IsSelected { get; }
	}

	public class ActivitySelectableBasicViewModel : ActivityBasicViewModel, ISelectable
	{
		public ActivitySelectableBasicViewModel(Activity activity) 
			: base(activity)
		{ }

		bool isSelected;
		public bool IsSelected
		{
			get
			{
				return isSelected;
			}
			set
			{
				if(isSelected != value)
				{
					isSelected = value;
					RaisePropertyChanged(nameof(IsSelected));
				}
			}
		}
	}
}
