﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Retrotimer.Model;

namespace Retrotimer.ViewModel
{
	public interface IStageBrowserViewModel : INotifyPropertyChanged
	{
		RetroPhase Phase { get; set; }

		ActivitySelectableBasicViewModel SelectedActivity { get; set; }
		List<ActivitySelectableBasicViewModel> ActivitiesInPhase { get; }
	}
}
