﻿namespace Retrotimer.ViewModel
{
	public interface IStyleable
	{
		VisualStyle CurrentVisualStyle { get; set; }
	}
}
