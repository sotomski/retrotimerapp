﻿using System.Collections.Generic;

namespace Retrotimer.ViewModel
{
    public interface IVisualStyleGenerator
	{
        IEnumerable<VisualStyle> GenerateVisualStylesWithoutIdenticalNeighbours(uint numberOfStylesToGenerate);
	}
}
