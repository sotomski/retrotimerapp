﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using Retrotimer.Data.State;
using Retrotimer.Model;
using Retrotimer.Services;
using Retrotimer.View;
using Xamarin.Forms;

namespace Retrotimer.ViewModel
{
	/// <summary>
	/// Public interface for retro plan view model.
	/// </summary>
	public interface IRetroPlanViewModel : INotifyPropertyChanged
	{
		ActivityFullViewModel CurrentSetTheStageActivity { get; }
		ActivityFullViewModel CurrentGatherDataActivity { get; }
		ActivityFullViewModel CurrentGenerateInsightsActivity { get; }
		ActivityFullViewModel CurrentDecideWhatToDoActivity { get; }
		ActivityFullViewModel CurrentCloseTheRetrospectiveActivity { get; }
		bool IsCountdownActive { get; }

		ICommand LoadRetrospectiveCommand { get; }
		ICommand GenerateRandomRetrospectiveCommand { get; }
		ICommand StartRetrospectiveCommand { get; }
		ICommand PauseRetrospectiveCommand { get; }
		ICommand ResetRetrospectiveCommand { get; }
		ICommand NavigateToAboutViewCommand { get; }
		ICommand SaveRetroPlanToDiskCommand { get; }

		/// <summary>
		/// Updates the current retro plan to include the activity
		/// passed as parameter. This operation effectively substitutes
		/// activity for a given phase.
		/// </summary>
		/// <param name="activity">Activity to be included in the current retro plan.</param>
		void UpdateRetroPlanWith(Activity activity);
	}

	public class RetroPlanViewModel : ViewModelBase, IRetroPlanViewModel
	{
		[Flags]
		enum RetroCountdownStatus
		{
			Inactive = 0,
			Running = 2,
			Paused = 4
		}

		RetrospectivePlan retro;
		IRetrospectivePlayer player;
		IFocusNavigationService focusNavigationService;
		readonly IRetrospectiveInteractor interactor;
		readonly INavigationService navigation;
		readonly IRetrospectivePlayerProvider retroPlayerProvider;
		readonly IFocusNavigationServiceProvider focusNavigationServiceProvider;
		readonly IVisualStyleGenerator styleGen;
		readonly IViewModelLocator viewModelLocator;

		public RetroPlanViewModel(
			IRetrospectiveInteractor retroInteractor,
			INavigationService navService,
			IVisualStyleGenerator visualStyleGenerator,
			IRetrospectivePlayerProvider retroPlayerProvider,
			IFocusNavigationServiceProvider focusNavigationServiceProvider,
			IViewModelLocator viewModelLocator)
		{
			interactor = retroInteractor;
			interactor.RetrospectivePlanOutdated += HandleRetrospectivePlanOutdated;
			
			navigation = navService;
			styleGen = visualStyleGenerator;
			this.retroPlayerProvider = retroPlayerProvider;
			this.focusNavigationServiceProvider = focusNavigationServiceProvider;
			this.viewModelLocator = viewModelLocator;

			InitializeCommands();

			Messenger.Default.Register<NotificationMessage>(this, DispatchIncomingNotifications);
		}

		ActivityFullViewModel currentSetTheStageActivity;

		public ActivityFullViewModel CurrentSetTheStageActivity
		{
			get
			{
				Debug.WriteLine("RetroPlanViewModel::CurrentSetTheStageActivity::get >> {0}", currentSetTheStageActivity);
				return currentSetTheStageActivity;
			}
			private set
			{
				if (value != currentSetTheStageActivity)
				{
					currentSetTheStageActivity = value;
					RaisePropertyChanged(nameof(CurrentSetTheStageActivity));
				}
			}
		}

		ActivityFullViewModel currentGatherDataActivity;

		public ActivityFullViewModel CurrentGatherDataActivity
		{
			get { return currentGatherDataActivity; }
			private set
			{
				if (value != currentGatherDataActivity)
				{
					currentGatherDataActivity = value;
					RaisePropertyChanged(nameof(CurrentGatherDataActivity));
				}
			}
		}

		ActivityFullViewModel currentGenerateInsightsActivity;

		public ActivityFullViewModel CurrentGenerateInsightsActivity
		{
			get => currentGenerateInsightsActivity;
			private set
			{
				if (value != currentGenerateInsightsActivity)
				{
					currentGenerateInsightsActivity = value;
					RaisePropertyChanged(nameof(CurrentGenerateInsightsActivity));
				}
			}
		}

		ActivityFullViewModel currentDecideWhatToDoActivity;

		public ActivityFullViewModel CurrentDecideWhatToDoActivity
		{
			get { return currentDecideWhatToDoActivity; }
			private set
			{
				if (value != currentDecideWhatToDoActivity)
				{
					currentDecideWhatToDoActivity = value;
					RaisePropertyChanged(nameof(CurrentDecideWhatToDoActivity));
				}
			}
		}

		ActivityFullViewModel currentCloseTheRetrospectiveActivity;

		public ActivityFullViewModel CurrentCloseTheRetrospectiveActivity
		{
			get { return currentCloseTheRetrospectiveActivity; }
			private set
			{
				if (value != currentCloseTheRetrospectiveActivity)
				{
					currentCloseTheRetrospectiveActivity = value;
					RaisePropertyChanged(nameof(CurrentCloseTheRetrospectiveActivity));
				}
			}
		}

		/// <summary>
		/// Gets the countdown status.
		/// </summary>
		/// <value><c>true</c> if countdown is active; <c>false</c> otherwise</value>
		public bool IsCountdownActive => CountdownStatus == RetroCountdownStatus.Running;

		// TODO: Refactor out or add unit tests!!!
		public bool IsRetrospectiveInProgress => CountdownStatus != RetroCountdownStatus.Inactive;

		// TODO: Refactor this enum away - there is no need for it.
		RetroCountdownStatus countdownStatus;

		RetroCountdownStatus CountdownStatus
		{
			get { return countdownStatus; }
			set
			{
				if (value != countdownStatus)
				{
					countdownStatus = value;
					RaisePropertyChanged(nameof(CountdownStatus));
					RaisePropertyChanged(nameof(IsCountdownActive));
					RaisePropertyChanged(nameof(IsRetrospectiveInProgress));

					ChangeIsEditModeForAllActivities(value == RetroCountdownStatus.Inactive);

					TriggerCanExecuteRefreshOf(MoveToPreviousActivity);
					TriggerCanExecuteRefreshOf(MoveToNextActivity);
				}
			}
		}

		ActivityFullViewModel currentlyPlayedActivity;

		public ActivityFullViewModel CurrentlyPlayedActivity
		{
			get { return currentlyPlayedActivity; }
			private set
			{
				if (value != currentlyPlayedActivity)
				{
					currentlyPlayedActivity = value;
					RaisePropertyChanged(nameof(CurrentlyPlayedActivity));
				}
			}
		}

		public ICommand LoadRetrospectiveCommand { get; private set; }
		public ICommand GenerateRandomRetrospectiveCommand { get; private set; }
		public ICommand StartRetrospectiveCommand { get; private set; }
		public ICommand PauseRetrospectiveCommand { get; private set; }
		public ICommand MoveToPreviousActivity { get; private set; }
		public ICommand MoveToNextActivity { get; private set; }
		public ICommand ResetRetrospectiveCommand { get; private set; }
		public ICommand NavigateToAboutViewCommand { get; private set; }
		public ICommand SaveRetroPlanToDiskCommand { get; private set; }
		

		void InitializeCommands()
		{
			LoadRetrospectiveCommand = new Command(LoadRetrospectiveCommandExecute);
			GenerateRandomRetrospectiveCommand = new Command(GenerateRandomRetrospectiveCommandExecute);
			StartRetrospectiveCommand = new Command(StartRetrospectiveCommandExecute);
			PauseRetrospectiveCommand = new Command(PauseRetrospectiveCommandExecute);
			MoveToPreviousActivity = new Command(MoveToPreviousActivityExecute, MoveToPreviousActivityCanExecute);
			MoveToNextActivity = new Command(MoveToNextActivityExecute, MoveToNextActivityCanExecute);
			ResetRetrospectiveCommand = new Command(ResetRetrospectiveCommandExecute);
			NavigateToAboutViewCommand = new Command(NavigateToAboutViewCommandExecute);
			SaveRetroPlanToDiskCommand = new Command(SaveRetroPlanToDiskCommandExecute);
		}
		
		private void HandleRetrospectivePlanOutdated(
			object sender, RetrospectivePlanOutdatedEventArgs eventArgs)
		{
			var newPlan = eventArgs.Merger(retro);
			UpdateCurrentActivityViewModelsBasedOn(newPlan);
		}

		void HandleChangeOfOriginalTimebox(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(ActivityFullViewModel.OriginalTimebox))
			{
				var avm = (ActivityFullViewModel) sender;
				var currentActivity = retro.GetActivityForPhase(avm.Phase);
				var newActivity = new ActivityWithTimebox(currentActivity.Activity, avm.OriginalTimebox);
				UpdateRetroPlanWith(newActivity);
			}
		}

		async void LoadRetrospectiveCommandExecute()
		{
			var retrievedState = await GetSavedStateOrRandom();
			LoadRetroPlanState(retrievedState);
		}

		async void GenerateRandomRetrospectiveCommandExecute()
		{
			var randomState = await GetRandomState();
			LoadRetroPlanState(randomState);
		}

		void LoadRetroPlanState(RetroPlanState state)
		{
			ResetPlayer();

			retro = state.ToRetrospectivePlan();
			UpdateCurrentActivityViewModelsBasedOn(retro);

			var styles = state.ToVisualStyles().ToList();
			UpdateCurrentActivitiesStyles(styles);
		}

		void ResetPlayer()
		{
			if (player != null)
				player.Reset();

			CountdownStatus = RetroCountdownStatus.Inactive;
		}

		async Task<RetroPlanState> GetSavedStateOrRandom()
		{
			try
			{
				return await interactor.ReadAsync();
			}
			catch (StateStorageException ssex)
			{
				Debug.WriteLine("RetroPlanViewModel::Loading saved state failed: " + ssex.Message);

				return await GetRandomState();
			}
		}

		async Task<RetroPlanState> GetRandomState()
		{
			var plan = await interactor.GenerateRetrospectivePlan();
			var styles = styleGen.GenerateVisualStylesWithoutIdenticalNeighbours(5);
			return new RetroPlanState(plan, styles);
		}

		void UpdateCurrentActivityViewModelsBasedOn(RetrospectivePlan plan)
		{
			CurrentSetTheStageActivity = CreateNewViewModelWithActivity(CurrentSetTheStageActivity, plan.SetTheStage);
			CurrentSetTheStageActivity.FocusNavigationIndex = 1;

			CurrentGatherDataActivity = CreateNewViewModelWithActivity(CurrentGatherDataActivity, plan.GatherData);
			CurrentGatherDataActivity.FocusNavigationIndex = 2;

			CurrentGenerateInsightsActivity =
				CreateNewViewModelWithActivity(CurrentGenerateInsightsActivity, plan.GenerateInsights);
			CurrentGenerateInsightsActivity.FocusNavigationIndex = 3;

			CurrentDecideWhatToDoActivity = CreateNewViewModelWithActivity(CurrentDecideWhatToDoActivity, plan.DecideWhatToDo);
			CurrentDecideWhatToDoActivity.FocusNavigationIndex = 4;

			CurrentCloseTheRetrospectiveActivity =
				CreateNewViewModelWithActivity(CurrentCloseTheRetrospectiveActivity, plan.CloseTheRetrospective);
			CurrentCloseTheRetrospectiveActivity.FocusNavigationIndex = 5;

			var navigatables = GetCurrentActivityViewModelsAsList().ToList<IFocusNavigatable>();
			focusNavigationService = focusNavigationServiceProvider.CreateFocusNavigationService(navigatables);

			ChangeIsEditModeForAllActivities(true);
		}

		void ChangeIsEditModeForAllActivities(bool val)
		{
			CurrentSetTheStageActivity.IsEditMode = val;
			CurrentGatherDataActivity.IsEditMode = val;
			CurrentGenerateInsightsActivity.IsEditMode = val;
			CurrentDecideWhatToDoActivity.IsEditMode = val;
			CurrentCloseTheRetrospectiveActivity.IsEditMode = val;
		}

		void UpdateCurrentActivitiesStyles(IEnumerable<VisualStyle> styles)
		{
			var stylesList = styles.ToList();
			var vms = GetCurrentActivityViewModelsAsList().Cast<IStyleable>().ToList();

			for (int i = 0; i < styles.Count(); i++)
			{
				vms[i].CurrentVisualStyle = stylesList[i];
			}
		}

		ActivityFullViewModel CreateNewViewModelWithActivity(ActivityFullViewModel avm, ActivityWithTimebox activity)
		{
			var style = default(VisualStyle);
			var timebox = activity.Timebox;

			if (avm != null)
			{
				if (avm.Id == activity.Activity.ID && avm.OriginalTimebox == activity.Timebox)
				{
					return avm;
				}

				style = avm.CurrentVisualStyle;
				timebox = avm.OriginalTimebox;
				avm.PropertyChanged -= HandleChangeOfOriginalTimebox;
			}

			var updatedAvm = new ActivityFullViewModel(activity)
			{
				CurrentVisualStyle = style,
				OriginalTimebox = timebox
			};
			updatedAvm.PropertyChanged += HandleChangeOfOriginalTimebox;

			return updatedAvm;
		}

		void StartRetrospectiveCommandExecute()
		{
			if (CountdownStatus == RetroCountdownStatus.Inactive)
			{
				var playlist = GetCurrentActivityViewModelsAsList().Cast<IRetroPlaylistItem>().ToList();
				player = retroPlayerProvider.CreatePlayerForPlan(playlist);

				player.PlayedItemChanged += (sender, e) =>
				{
					TriggerCanExecuteRefreshOf(MoveToPreviousActivity);
					TriggerCanExecuteRefreshOf(MoveToNextActivity);
					CurrentlyPlayedActivity = (ActivityFullViewModel) e.Value;
				};
				player.RetrospectiveFinished += (sender, e) => ResetPlayer();
			}

			CountdownStatus = RetroCountdownStatus.Running;
			player.StartOrResume();
		}

		void PauseRetrospectiveCommandExecute()
		{
			if (CountdownStatus == RetroCountdownStatus.Running)
			{
				player.Pause();
				CountdownStatus = RetroCountdownStatus.Paused;
			}
		}

		bool MoveToPreviousActivityCanExecute()
		{
			if (!IsCountdownActive)
				return false;

			var isNotPlayingFirstItem = !player?.CheckIfPlayingFirstItem();
			return isNotPlayingFirstItem ?? false;
		}

		void MoveToPreviousActivityExecute()
		{
			player.MoveBackwards();
			//TriggerCanExecuteRefreshOf(MoveToPreviousActivity);
		}

		bool MoveToNextActivityCanExecute()
		{
			if (!IsCountdownActive)
				return false;

			var isNotPlayingLastItem = !player?.CheckIfPlayingLastItem();
			return isNotPlayingLastItem ?? false;
		}

		void MoveToNextActivityExecute()
		{
			player.MoveForwards();
			//TriggerCanExecuteRefreshOf(MoveToNextActivity);
		}

		void TriggerCanExecuteRefreshOf(ICommand command)
		{
			((Command) command).ChangeCanExecute();
		}

		void ResetRetrospectiveCommandExecute()
		{
			ResetPlayer();
		}

		/// <summary>
		/// Helper method to compose a list out of activities in
		/// current retrospective plan. It's much easier to manipulate
		/// them in this way.
		/// </summary>
		/// <returns>The activity view models as list.</returns>
		List<ActivityFullViewModel> GetCurrentActivityViewModelsAsList()
		{
			return new List<ActivityFullViewModel>
			{
				CurrentSetTheStageActivity,
				CurrentGatherDataActivity,
				CurrentGenerateInsightsActivity,
				CurrentDecideWhatToDoActivity,
				CurrentCloseTheRetrospectiveActivity
			};
		}

		void UpdateRetroPlanWith(ActivityWithTimebox activityWithTimebox)
		{
			retro = retro.ChangePlanWithActivity(activityWithTimebox);
			UpdateCurrentActivityViewModelsBasedOn(retro);
		}

		async void DispatchIncomingNotifications(NotificationMessage notification)
		{
			if (notification.Notification == MessageCommunicationProtocol.TitleTappedMessage)
			{
				// Only present stage browser (list) during retro preparation.
				var sender = notification.Sender as ActivityFullViewModel;
				await NavigateToStageBrowser(sender);
			}
		}

		private async Task NavigateToStageBrowser(ActivityFullViewModel activityToSelect)
		{
			if (CountdownStatus == RetroCountdownStatus.Inactive)
			{
				if (GetCurrentActivityViewModelsAsList().Contains(activityToSelect))
				{
					var stageBrowserVm = viewModelLocator.GetViewModel<IStageBrowserViewModel>();
					stageBrowserVm.SelectedActivity = new ActivitySelectableBasicViewModel(activityToSelect.Model);
					stageBrowserVm.Phase = activityToSelect.Phase;

					await navigation.PushAsync(NavigationPages.BrowseStagePage, true, stageBrowserVm);
				}
			}
		}

		public void UpdateRetroPlanWith(Activity activity)
		{
			var current = retro.GetActivityForPhase(activity.Phase);
			var newAwt = new ActivityWithTimebox(activity, current.Timebox);
			UpdateRetroPlanWith(newAwt);
		}

		async void NavigateToAboutViewCommandExecute()
		{
			var vm = viewModelLocator.GetViewModel<AboutViewModel>();
			await navigation.PushModalAsync(NavigationPages.InfoPage, true, vm);
		}

		void SaveRetroPlanToDiskCommandExecute()
		{
			Debug.WriteLine("Retrotimer.ViewModel.RetroPlanViewModel >> Saving application state to disk...");
			var styles = GetCurrentActivityViewModelsAsList().Select(m => m.CurrentVisualStyle);
			var state = new RetroPlanState(retro, styles);
			interactor.SaveAsync(state);
		}
	}
}
