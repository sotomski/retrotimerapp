﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Retrotimer.Model;
using Retrotimer.Services.Analytics;
using Retrotimer.Services.Data;
using Xamarin.Forms;

namespace Retrotimer.ViewModel
{
    public class StageBrowserViewModel : ViewModelBase, IStageBrowserViewModel
    {
        readonly IDataService dataService;
        readonly IVisualStyleGenerator styleGenerator;
        readonly IViewModelLocator viewModelLocator;
        RetroPhase phase;
        List<ActivitySelectableBasicViewModel> activitiesInPhase;
        ActivitySelectableBasicViewModel selectedActivity;
        List<ActivitySelectableBasicViewModel> allActivities;
        private bool isRefreshingActivities;

        public RetroPhase Phase
        {
            get => phase;
            set
            {
                if (phase != value)
                {
                    phase = value;
                    UpdateActivitiesInPhase();
                }
            }
        }

        public ActivitySelectableBasicViewModel SelectedActivity
        {
            get => selectedActivity;
            set
            {
                if (value != null)
                {
                    if (selectedActivity != value)
                    {
                        if (selectedActivity != null)
                            selectedActivity.IsSelected = false;

                        selectedActivity = value;
                        selectedActivity.IsSelected = true;
                        
                        var retroPlanVm = viewModelLocator.GetViewModel<IRetroPlanViewModel>();
                        retroPlanVm.UpdateRetroPlanWith(selectedActivity.Model);
                    }
                }

                // Ensure there is always a selected item on the listview. How?
                // Xamarin.Forms.ListView allows de-selecting all items. As a result, SelectedItem is null.
                // This view model ignores null value because it represents illegal state for the app.
                // So, if ListView tries to set SelectedActivity to null, it is ignored and
                // property changed event is raised to set current SelectedActivity as SelectedItem of the ListView.
                // The downside is that PropertyChanged is raised even if the property wasn't changed. Bummer.
                // Simple, right?
                RaisePropertyChanged(nameof(SelectedActivity));
            }
        }

        public List<ActivitySelectableBasicViewModel> ActivitiesInPhase
        {
            get => activitiesInPhase;
            set
            {
                if (activitiesInPhase != value)
                {
                    activitiesInPhase = value;
                    RaisePropertyChanged(nameof(ActivitiesInPhase));
                }
            }
        }

        public bool IsRefreshingActivities
        {
            get => isRefreshingActivities;
            private set
            {
                if (isRefreshingActivities != value)
                {
                    isRefreshingActivities = value;
                    RaisePropertyChanged(nameof(IsRefreshingActivities));
                }
            }
        }

        public ICommand LoadActivitiesCommand { get; }
        public ICommand RefreshActivitiesCommand { get; }

        public StageBrowserViewModel(IDataService dataService, 
            IViewModelLocator viewModelLocator, 
            IVisualStyleGenerator styleGenerator)
        {
            activitiesInPhase = new List<ActivitySelectableBasicViewModel>();

            this.dataService = dataService ?? throw new ArgumentNullException(nameof(dataService));
            this.styleGenerator = styleGenerator ?? throw new ArgumentNullException(nameof(styleGenerator));
            this.viewModelLocator = viewModelLocator ?? throw new ArgumentNullException(nameof(viewModelLocator));

            LoadActivitiesCommand = new Command(ExecuteLoadActivitiesCommand);
            RefreshActivitiesCommand = new Command(ExecuteRefreshActivitiesCommand);

            Phase = RetroPhase.SetTheStage;
        }

        async void ExecuteLoadActivitiesCommand()
        {
			if (allActivities == null)
            {
				await LoadAllActivities();
				UpdateActivitiesInPhase();
            }
        }

		async Task LoadAllActivities()
		{
		    var query = await dataService.ReadActivitiesAsync();
		    allActivities = query.Select(a => new ActivitySelectableBasicViewModel(a)).ToList();
        }

        void UpdateActivitiesInPhase()
        {
            if (allActivities == null)
                return;

            var viewModelsToExpose = allActivities.Where(a => a.Model.Phase == Phase);
            ActivitiesInPhase = new List<ActivitySelectableBasicViewModel>(viewModelsToExpose);
            GenerateAndApplyVisualStylesToActivities(ActivitiesInPhase);

            UpdateSelectedActivity();
        }

		void GenerateAndApplyVisualStylesToActivities(List<ActivitySelectableBasicViewModel> activitiesToStyle)
		{
			var styleCount = Convert.ToUInt32(activitiesToStyle.Count);
			var styles = styleGenerator.GenerateVisualStylesWithoutIdenticalNeighbours(styleCount).ToList();
			for (var i = 0; i < styleCount; i++)
			{
				activitiesToStyle[i].CurrentVisualStyle = styles[i];
			}
		}

        void UpdateSelectedActivity()
        {
            if (SelectedActivity != null)
            {
                var instanceToSelect = activitiesInPhase.FirstOrDefault(a => a.Id == SelectedActivity.Id);
                
                SelectedActivity = instanceToSelect ?? activitiesInPhase.FirstOrDefault();
            }
        }
        
        private async void ExecuteRefreshActivitiesCommand()
        {
            try
            {
                IsRefreshingActivities = true;

                await RefreshAllActivities();
                UpdateActivitiesInPhase();
            }
            catch (Exception ex)
            {
                var logEvent = new ActivityRefreshFailEvent
                {
                    ErrorType = ex.GetType().ToString(),
                    Message = ex.Message
                };
                Analytics.Instance.LogEvent(logEvent);
            }
            finally
            {
                IsRefreshingActivities = false;                
            }
        }

        private async Task RefreshAllActivities()
        {
            var refreshedActivities = await dataService.RefreshActivitiesAsync();
            allActivities = refreshedActivities.Select(a => new ActivitySelectableBasicViewModel(a)).ToList();
        }
    }
}
