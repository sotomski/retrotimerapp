﻿using System;
using System.Windows.Input;
using System.Diagnostics;
using Retrotimer.Services.Analytics;
using Retrotimer.Services.Data;
using Xamarin.Forms;

namespace Retrotimer.ViewModel
{
    public class StartViewModel : ViewModelBase
    {
        readonly IDataService dataService;
        readonly INavigationService navService;
        readonly IViewModelLocator viewModelLocator;

        public VisualStyle CurrentVisualStyle => VisualStyle.DarkBlue;

        SyncStatus status;
        SyncStatus Status
        {
            get => status;
            set
            {
                if (value != status)
                {
                    status = value;
                    RaisePropertyChanged(nameof(Status));
                    RaisePropertyChanged(nameof(IsRetryAvailable));
                    RaisePropertyChanged(nameof(IsSynchronizing));
                    RaisePropertyChanged(nameof(Message));
                }
            }
        }

        public bool IsRetryAvailable => Status == SyncStatus.Error;

        public bool IsSynchronizing => Status == SyncStatus.IsSynchronizing;

        public string Message 
        {
            get
            {
                if (Status == SyncStatus.Idle)
				{
					return "";
				}
                if (Status == SyncStatus.Error)
                {
                    return "Synchronization failed. Please try again later.";
                }

                return "Synchronizing with the Retromat website...";
            }
        }

        public ICommand SynchronizeCommand { get; }

        public StartViewModel(IDataService dataService, 
                              INavigationService navService, 
                              IViewModelLocator viewModelLocator)
        {
            this.dataService = dataService;
            this.navService = navService;
            this.viewModelLocator = viewModelLocator;

            SynchronizeCommand = new Command(SynchronizeExecute);

            Status = SyncStatus.Idle;
        }

        async void SynchronizeExecute()
        {
            Status = SyncStatus.IsSynchronizing;

            try
            {
				await dataService.RefreshActivitiesAsync();
                var retroPlanVm = viewModelLocator.GetViewModel<IRetroPlanViewModel>();
                await navService.PushAsync(NavigationPages.RetroPlanPage, false, retroPlanVm);

                Status = SyncStatus.Success;
            } 
            catch (Exception ex)
            {
                Status = SyncStatus.Error;
                
                Debug.WriteLine(
                    GetType() + $" Synchronization with the website failed with {ex.GetType()}:{ex.Message}");
                Analytics.Instance.LogEvent(new ActivityRefreshFailEvent
                {
                    Message = ex.Message,
                    ErrorType = ex.GetType().ToString()
                });
            }
        }

        enum SyncStatus { Idle, IsSynchronizing, Error, Success }
    }
}
