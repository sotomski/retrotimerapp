﻿using System.ComponentModel;
using System.Diagnostics;

namespace Retrotimer.ViewModel
{
	public abstract class ViewModelBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged(string propertyName)
		{
			if (PropertyChanged == null) return;
			
			Debug.WriteLine("ViewModelBase::RaisePropertyChanged >> Property {0} changed", propertyName);
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
