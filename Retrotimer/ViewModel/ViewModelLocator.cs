﻿using System.ComponentModel;
using Retrotimer.Common;

namespace Retrotimer.ViewModel
{
    public interface IViewModelLocator
	{
		T GetViewModel<T>(object param1 = null) where T : class, INotifyPropertyChanged;
	}
	
	public class ViewModelLocator : IViewModelLocator
	{
		readonly IDependencyContainer container;
		
		public ViewModelLocator(IDependencyContainer container)
		{
            this.container = container;
		}

		public T GetViewModel<T>(object param1 = null) where T : class, INotifyPropertyChanged
		{
			return container.Resolve<T>();
		}
	}
}
