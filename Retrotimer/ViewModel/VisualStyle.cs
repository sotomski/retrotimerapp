﻿
namespace Retrotimer.ViewModel
{
    /// <summary>
    /// Visual style used by view models and converters to define view style.
    /// </summary>
    public enum VisualStyle
    {
        LightOrange     = 0,
        DarkOrange      = 1,
        LightBlue       = 2,
        Blue            = 3,
        DarkBlue        = 4
    }
}
