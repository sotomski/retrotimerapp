﻿using System.Collections.Generic;
using Retrotimer.Services;

namespace Retrotimer.ViewModel
{
    public class VisualStyleFastGenerator : IVisualStyleGenerator
    {
        static readonly VisualStyle[] StyleSource = { 
            VisualStyle.Blue, VisualStyle.DarkOrange, VisualStyle.LightBlue, VisualStyle.DarkBlue, 
            VisualStyle.LightOrange, VisualStyle.LightBlue, VisualStyle.DarkOrange, VisualStyle.Blue,
            VisualStyle.LightBlue, VisualStyle.LightOrange };

        readonly IRandomGenerator rand;

        public VisualStyleFastGenerator(IRandomGenerator randomizer)
        {
            rand = randomizer;
        }

        public IEnumerable<VisualStyle> GenerateVisualStylesWithoutIdenticalNeighbours(uint numberOfStylesToGenerate)
        {
            var generatedStyles = new VisualStyle[numberOfStylesToGenerate];
            var sourceLength = StyleSource.Length;
            var offset = rand.GetInt(0, sourceLength);

            for (var i = 0; i < numberOfStylesToGenerate; i++)
            {
                generatedStyles[i] = StyleSource[(i + offset) % sourceLength];
            }

            return generatedStyles;
        }
    }
}
