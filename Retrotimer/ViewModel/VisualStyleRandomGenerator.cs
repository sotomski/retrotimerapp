﻿using System;
using System.Collections.Generic;
using System.Linq;
using Retrotimer.Services;

namespace Retrotimer.ViewModel
{
	public class VisualStyleRandomGenerator : IVisualStyleGenerator
	{
		readonly IRandomGenerator rand;

		public VisualStyleRandomGenerator(IRandomGenerator randomizer)
		{
			rand = randomizer ?? throw new ArgumentNullException();
		}

        public IEnumerable<VisualStyle> GenerateVisualStylesWithoutIdenticalNeighbours(uint numberOfStylesToGenerate)
		{
			var availableStyles = Enum.GetValues(typeof(VisualStyle)).Cast<VisualStyle>().ToList();
			var randomStyles = new List<VisualStyle>();

			while (randomStyles.Count < numberOfStylesToGenerate)
			{
				var previous = randomStyles.LastOrDefault();
				var stylesWithoutPrevious = availableStyles.Where(i => i != previous).ToList();
				randomStyles.Add(GetRandomVisualStyle(stylesWithoutPrevious));
			}

			return randomStyles;
		}

		VisualStyle GetRandomVisualStyle(IList<VisualStyle> availableStyles)
		{
			return availableStyles.ElementAt(rand.GetInt(0, availableStyles.Count()));
		}
	}
}
