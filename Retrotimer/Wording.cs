﻿namespace Retrotimer
{
	/// <summary>
	/// Simple approach of sharing wording strings between
	/// app components.
	/// In the future, it will probably be
	/// used by a "translation converter" between used
	/// in data binding.
	/// For now, exposing Wording via this static class should be enough.
	/// </summary>
	public static class Wording
	{
		public static string Back = "Back";

		public static string SetTheStage = "Set the stage";
		public static string GatherData = "Gather data";
		public static string GenerateInsights = "Generate insights";
		public static string DecideWhatToDo = "Decide what to do";
		public static string CloseTheRetrospective = "Close the retrospective";
	}
}
