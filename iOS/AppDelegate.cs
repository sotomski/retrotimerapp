﻿using Acr.UserDialogs;
using Foundation;
using Retrotimer.Common;
using Retrotimer.iOS.Dependencies;
using Retrotimer.Services;
using UIKit;

namespace Retrotimer.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App(InitializeContainer()));

			// Keeps device screen always on
			app.IdleTimerDisabled = true;

            return base.FinishedLaunching(app, options);
        }

        /// <summary>
        /// Initializes the container without setting any resolvers.
        /// Resolver will be set in PCL Application instance in order to register common
        /// dependencies as well.
        /// </summary>
        /// <returns>The initialized container.</returns>
        protected static IDependencyContainer InitializeContainer()
        {
            var container = new MvvMLightDependencyContainer();

			container.Register<IVersionNumberProvider, VersionNumberProvider>();
			container.Register<IAudioPlayer, AudioPlayer>();
            container.Register<IDialogService, DialogService>();

            return container;
        }
    }
}
