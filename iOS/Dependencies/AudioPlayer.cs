﻿using System.Collections.Generic;
using System.IO;
using AVFoundation;
using Foundation;
using Retrotimer.Services;
using System.Threading.Tasks;

namespace Retrotimer.iOS.Dependencies
{
	public class AudioPlayer : IAudioPlayer
	{
		const string SoundDir = @"Sounds";
		static readonly Dictionary<Sounds, string> soundPaths = new Dictionary<Sounds, string>
		{
			{ Sounds.Chime, Path.Combine(SoundDir, "Chime.mp3") },
			{ Sounds.DoubleChime, Path.Combine(SoundDir, "DoubleChime.mp3") }
		};

		public void PlaySound(Sounds sound)
		{
			var url = new NSUrl(soundPaths[sound]);

            AVAudioPlayer player;
            NSError error;

            player = new AVAudioPlayer(url, "mp3", out error);
			player.Play();
		}

		public async Task PlaySoundAsync(Sounds sound)
		{
			await Task.Run(() => PlaySound(sound));
		}
	}
}
