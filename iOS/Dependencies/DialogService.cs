﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Retrotimer.Services;

namespace Retrotimer.iOS.Dependencies
{
    public class DialogService : IDialogService
    {
        public Task<bool> ShowDialogAsync(
            string message, string title = null, string okText = null, string cancelText = "Cancel", 
            CancellationToken? cancelToken = default(CancellationToken?))
        {
            return UserDialogs.Instance.ConfirmAsync(message, title, okText, cancelText, cancelToken);
        }
    }
}
