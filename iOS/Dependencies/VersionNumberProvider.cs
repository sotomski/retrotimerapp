﻿using System;
using Foundation;

namespace Retrotimer.iOS.Dependencies
{
    public class VersionNumberProvider : IVersionNumberProvider
    {
        public string GetAppVersionNumber()
        {
            var version = NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"].ToString();
            var build = NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].ToString();

            return String.Format("{0} ({1})", version, build);
        }
    }
}
