﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Retrotimer.iOS.Effects;
using UIKit;
using System.Linq;
using forms = Retrotimer.View;

[assembly:ResolutionGroupName("dev.sotomski")]
[assembly:ExportEffect(typeof(TouchDownEffect), "TouchDownEffect")]
namespace Retrotimer.iOS.Effects
{
	public class TouchDownEffect : PlatformEffect
	{
		UIColor highlightColor;
		UIColor standardColor;

		protected override void OnAttached()
		{
			var routingEffect = (forms.TouchDownEffect)Element.Effects.FirstOrDefault(e => e is forms.TouchDownEffect);
			highlightColor = routingEffect.HighlightColor.ToUIColor();
			standardColor = Control.BackgroundColor;

			var uiControl = (UIControl)Control;
			uiControl.TouchDown += (sender, e) => uiControl.BackgroundColor = highlightColor;
			uiControl.TouchUpInside += (sender, e) => uiControl.BackgroundColor = standardColor;
		}

		protected override void OnDetached()
		{
		}
	}
}
