﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using Foundation;
using Retrotimer.iOS.Renderers;
using Retrotimer.View;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CountdownPicker), typeof(CountdownPickerRenderer))]
namespace Retrotimer.iOS.Renderers
{
	/// <summary>
	/// Editable label that allows to add input view and input accessory view.
	/// Exists because I needed a convenient way to show date picker.
	/// </summary>
	public class UIEditableLabel : UILabel
	{
		UIView __inputView;
		UIView __inputAccessoryView;

		public event EventHandler WillResignFirstResponder;
		public event EventHandler DidResignFirstResponder;
		public event EventHandler DidBecomeFirstResponder;

		public UIEditableLabel()
		{
			Layer.CornerRadius = 5.0f;
			Layer.BorderWidth = 2f;
		}

		public override bool CanBecomeFirstResponder
		{
			get
			{
				return true;
			}
		}

		public override UIView InputView
		{
			get
			{
				if (__inputView == null)
					return base.InputView;
				return __inputView;
			}
		}

		public void SetInputView(UIView view)
		{
			__inputView = view;
		}

		public override UIView InputAccessoryView
		{
			get
			{
				if (__inputAccessoryView == null)
					return base.InputAccessoryView;
				return __inputAccessoryView;
			}
		}

		public void SetInputAccessoryView(UIView view)
		{
			__inputAccessoryView = view;
		}

		public override bool ResignFirstResponder()
		{
			WillResignFirstResponder?.Invoke(this, EventArgs.Empty);

			var retVal = base.ResignFirstResponder();
			if(retVal)
			{
				Animate(0.3, 0.05, UIViewAnimationOptions.CurveEaseIn, () => Layer.BackgroundColor = UIColor.Clear.CGColor, null);
				DidResignFirstResponder?.Invoke(this, EventArgs.Empty);
			}

			return retVal;
		}

		public override bool BecomeFirstResponder()
		{
			var retVal = base.BecomeFirstResponder();
			if(retVal)
			{
				Layer.BackgroundColor = UIColor.White.ColorWithAlpha(0.4f).CGColor;
				DidBecomeFirstResponder?.Invoke(this, EventArgs.Empty);
			}

			return retVal;
		}
	}

	public class CountdownPickerRenderer : ViewRenderer<CountdownPicker, UIEditableLabel>, IUIGestureRecognizerDelegate
	{
		const int TimerInterval = 150;

		/// <summary>
		/// Flag used to stop a timer associated with UIDatePicker.
		/// </summary>
		/// <remarks>
		/// This flag exists because of years-old bug in UIDatePicker in Countdown mode 
		/// that causes the control not to raise valuechanged event on first "rotation".
		/// To prevent that, the renderer uses a timer to read the value out of UIDatePicker with
		/// a specified interval.
		/// http://stackoverflow.com/questions/20181980/uidatepicker-bug-uicontroleventvaluechanged-after-hitting-minimum-internal#20204225
		/// </remarks>
		bool shouldInvalidateTimer;
		UIDatePicker datePicker;
		UITapGestureRecognizer tapRecognizer;

		IElementController ElementController => Element as IElementController;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				tapRecognizer.Delegate = null;
				datePicker.ValueChanged -= OnPickerValueChanged;
			}

			base.Dispose(disposing);
		}

		UIEditableLabel CreateTimeLabel()
		{
			var entry = new UIEditableLabel();
			tapRecognizer = new UITapGestureRecognizer(OnLabelTapped);
			tapRecognizer.NumberOfTapsRequired = 1;
			tapRecognizer.NumberOfTouchesRequired = 1;
			entry.AddGestureRecognizer(tapRecognizer);
			tapRecognizer.Delegate = this;

			entry.WillResignFirstResponder += OnLabelWillResignFirstResponder;
			entry.DidResignFirstResponder += OnLabelDidResignFirstResponder;
			entry.DidBecomeFirstResponder += OnLabelDidBecomeFirstResponder;

			entry.UserInteractionEnabled = true;
			entry.Lines = 0;
			entry.LineBreakMode = UILineBreakMode.WordWrap;
			entry.TextAlignment = UITextAlignment.Center;
			var biggerFont = entry.Font.WithSize(20);
			entry.Font = biggerFont;

			return entry;
		}

		void UpdateInputAccessoryToolbar(UIEditableLabel entry)
		{
			var width = UIScreen.MainScreen.Bounds.Width;
			var toolbar = new UIToolbar(new RectangleF(0, 0, (float)width, 44)) { BarStyle = UIBarStyle.Default, Translucent = true };
			var toolbarItems = Element.InputAccessoryToolbarItems;
			var uibuttonitems = toolbarItems.Select(ti => ti.ToUIBarButtonItem());

			// Done button should be added no matter what
			var spacer = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);
			var doneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done, (o, a) => entry.ResignFirstResponder());

			uibuttonitems = uibuttonitems.Concat(new[] { spacer, doneButton });
			toolbar.SetItems(uibuttonitems.ToArray(), false);
			entry.SetInputAccessoryView(toolbar);
		}

		void CreateInputView(UIEditableLabel entry)
		{	
			datePicker = new UIDatePicker { Mode = UIDatePickerMode.CountDownTimer, TimeZone = new NSTimeZone("UTC") };
			datePicker.ValueChanged += OnPickerValueChanged;
			entry.SetInputView(datePicker);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<CountdownPicker> e)
		{
			if (e.NewElement != null)
			{
				if (Control == null)
				{
					var entry = CreateTimeLabel();
					CreateInputView(entry);
					UpdateInputAccessoryToolbar(entry);

					// Make sure to update it again if collection changes
					Element.BindingContextChanged += (sender, args) => 
					{
						Control.ResignFirstResponder();
						UpdateInputAccessoryToolbar(entry);
					};

					SetNativeControl(entry);
				}

				UpdateTime();
			}

			base.OnElementChanged(e);

			UpdateTextColor();
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			// Saves picker value when Play button is pressed
			if(e.PropertyName == VisualElement.IsVisibleProperty.PropertyName)
				Control.ResignFirstResponder();

			if (e.PropertyName == CountdownPicker.TimeProperty.PropertyName)
				UpdateTime();

			if (e.PropertyName == CountdownPicker.TextColorProperty.PropertyName)
				UpdateTextColor();
		}

		public override void DidUpdateFocus(UIFocusUpdateContext context, UIFocusAnimationCoordinator coordinator)
		{
			base.DidUpdateFocus(context, coordinator);
		}

		void OnLabelTapped()
		{
			ElementController.SetValueFromRenderer(VisualElement.IsFocusedProperty, true);
			Control.BecomeFirstResponder();
		}
		
		void SetTimePropertyOnElement()
		{
			ElementController.SetValueFromRenderer(CountdownPicker.TimeProperty, datePicker.Date.ToDateTime() - new DateTime(1, 1, 1));
		}

		void OnPickerValueChanged(object sender, EventArgs e)
		{
			SetTimePropertyOnElement();
		}

		void OnLabelDidBecomeFirstResponder(object sender, EventArgs args)
		{
			SetTimePropertyOnElement();

			shouldInvalidateTimer = false;
			NSTimer.CreateRepeatingScheduledTimer(TimeSpan.FromMilliseconds(TimerInterval), (NSTimer timer) =>
			{
				System.Diagnostics.Debug.WriteLine("NSTimer >>>>>>>>>>>>>>>>>>>>>>>>> {0}", timer.Handle);
				if(shouldInvalidateTimer)
				{
					timer.Invalidate();
				}
				else
				{
					SetTimePropertyOnElement();
				}
			});
		}
		
		void OnLabelWillResignFirstResponder(object sender, EventArgs args)
		{
			// Saves picker value to viewmodel when Prev/Next buttons are pressed
			SetTimePropertyOnElement();
		}

		void OnLabelDidResignFirstResponder(object sender, EventArgs e)
		{
			shouldInvalidateTimer = true;
		}

		void UpdateTextColor()
		{
			var textColor = Element.TextColor;
			Control.TextColor = textColor.ToUIColor();
			Control.Layer.BorderColor = textColor.ToCGColor();
		}

		void UpdateTime()
		{
			datePicker.Date = new DateTime(1, 1, 1).Add(Element.Time).ToNSDate();
			Control.Text = DateTime.Today.Add(Element.Time).ToString("HH\n:mm");
		}
	}
}
