﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using Retrotimer.iOS.Renderers;
using Retrotimer.View;
using Retrotimer.ViewModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(RetroPlanView), typeof(RetroPlanViewRenderer))]
namespace Retrotimer.iOS.Renderers
{
	public class RetroPlanViewRenderer : PageRenderer
	{
		/// <summary>
		/// The epsilon for copmarison of floating points.
		/// </summary>
		const double Epsilon = 0.05;
		/// <summary>
		/// The margin between top of keyboard frame and bottom of first responder.
		/// </summary>
		const double KeyboardMargin = 8.0;

		NSObject observerKeyboardWillShow;
		NSObject observerKeyboardWillHide;

		List<Tuple<ToolbarItem, UIBarButtonItem>> mapToolbarItems;

		public new RetroPlanView Element
		{
			get { return (RetroPlanView)base.Element; }
		}

		RetroPlanViewModel Context
		{
			get { return (RetroPlanViewModel)Element.BindingContext; }
		}

		void UpdateNavigationItems(UINavigationItem navigationItem)
		{
			var buttonTextToHide = Context.IsRetrospectiveInProgress ? "New plan" : "Reset";

			var itemsThatWantToBeLeft = mapToolbarItems.Where(t => t.Item1.Priority == 0 && t.Item1.Text != buttonTextToHide).Select(t => t.Item2);
			navigationItem.SetLeftBarButtonItems(itemsThatWantToBeLeft.Reverse().ToArray(), false);

			var itemsThatWanToBeRight = mapToolbarItems.Where(t => t.Item1.Priority == 1).Select(t => t.Item2);
			navigationItem.SetRightBarButtonItems(itemsThatWanToBeRight.Reverse().ToArray(), false);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			// Establish the mapping between Xamarin Forms and UIKit world for later.
			var navigationItem = NavigationController.TopViewController.NavigationItem;

			if (mapToolbarItems == null)
			{
				// This mapping is very fragile - depends on declaration order in XAML as well as priority.
				mapToolbarItems = Element.ToolbarItems.Zip(navigationItem.RightBarButtonItems.Reverse(), (xfItem, uiItem) => Tuple.Create(xfItem, uiItem)).ToList();

				// Subscribe to counting update events.
				Context.PropertyChanged += (sender, e) =>
				{
					if (e.PropertyName == nameof(RetroPlanViewModel.IsRetrospectiveInProgress))
					{
						UpdateNavigationItems(navigationItem);
					}
				};
			}

			// Ensure that toolbar items are positioned correctly.
			UpdateNavigationItems(navigationItem);

			// Keyboard related setup
			observerKeyboardWillShow = UIKeyboard.Notifications.ObserveWillShow(OnKeyboardWillShowNotification);
			observerKeyboardWillHide = UIKeyboard.Notifications.ObserveWillHide(OnKeyboardWillHideNotification);
		}

		public override void ViewWillDisappear(bool animated)
		{
			NSNotificationCenter.DefaultCenter.RemoveObserver(observerKeyboardWillShow);
			NSNotificationCenter.DefaultCenter.RemoveObserver(observerKeyboardWillHide);
			
			base.ViewWillDisappear(animated);
		}

		void OnKeyboardWillShowNotification(object sender, UIKeyboardEventArgs e)
		{
			if (!IsViewLoaded) return;

			var keyFrameEnd = UIScreen.MainScreen.CoordinateSpace.ConvertRectToCoordinateSpace(e.FrameEnd, View);

			// Ensure that responder will be fully visible
			var responder = FindFirstResponderStartingFrom(View);

			var responderBoundsInViewCoordinates = responder.ConvertRectToView(responder.Bounds, View);
			responderBoundsInViewCoordinates.Offset(0.0, -View.Frame.Y);
			var newTop = Math.Min(0.0, keyFrameEnd.Top - responderBoundsInViewCoordinates.Bottom - KeyboardMargin);
			var bounds = Element.Bounds;
			var newBounds = new Rectangle(bounds.Left, newTop, bounds.Width, bounds.Height);
			Element.Layout(newBounds);
		}

		void OnKeyboardWillHideNotification(object sender, UIKeyboardEventArgs e)
		{
			if (!IsViewLoaded) return;

			var bounds = Element.Bounds;
			var KeyFrameBegin = e.FrameBegin;
			var newBounds = new Rectangle(bounds.Left, 0.0, bounds.Width, bounds.Height);
			Element.Layout(newBounds);
		}

		UIView FindFirstResponderStartingFrom(UIView root)
		{
			if (root.IsFirstResponder)
				return root;

			return root.Subviews.Select(v => FindFirstResponderStartingFrom(v))
					   .FirstOrDefault(v => v != null);
		}
	}
}