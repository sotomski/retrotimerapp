﻿using CoreAnimation;
using CoreGraphics;
using Foundation;
using Retrotimer.iOS.Renderers;
using Retrotimer.View;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(NavigationPage), typeof(StyledNavigationPageRenderer))]
namespace Retrotimer.iOS.Renderers
{
	public class StyledNavigationPageRenderer : NavigationRenderer
	{
		const int NavigationBarHeight = 44;
		const int StatusBarHeight = 20;
		static readonly UIColor DarkGreen = UIColor.FromRGB(0x28, 0x3D, 0x30);
		static readonly UIColor AlmostBlack = UIColor.FromRGB(0x00, 0x14, 0x0D);

		public override void ViewWillAppear(bool animated)
		{
			// Following adjustments are for all the navigation pages

			// Status bar
			UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);

			// Navigation bar
			var gradientLayer = CreateGradientForNavigationBar();
			NavigationBar.Layer.Sublayers[0].InsertSublayer(gradientLayer, 0);

			UINavigationBar.Appearance.TintColor = UIColor.White;
			UINavigationBar.Appearance.TitleTextAttributes = new UIStringAttributes
			{
				ForegroundColor = UIColor.White
			};

			// Start page-specific configuration
			// Following adjustments are on page basis
			var navPage = (NavigationPage)Element;
			PerformPageBasedConfig(navPage.CurrentPage);

			base.ViewWillAppear(animated);
		}

		void PerformPageBasedConfig(Page page)
		{
			if (page.GetType() == typeof(RetroPlanView))
			{
				var image = new UIImage("Logo");
				var imageView = new UIImageView(image);

				NavigationBar.TopItem.TitleView = imageView;
			}
		}

		static CAGradientLayer CreateGradientForNavigationBar()
		{
			var gradientLayer = new CAGradientLayer();
			var screenWidth = UIScreen.MainScreen.Bounds.Width;
			gradientLayer.Frame = new CGRect(0, 0, screenWidth, NavigationBarHeight + StatusBarHeight);
			var colorsCG = new[] {
				DarkGreen.CGColor,
				AlmostBlack.CGColor
			};
			var locations = new NSNumber[] { 0, 0.84 };
			gradientLayer.Colors = colorsCG;
			gradientLayer.Locations = locations;

			return gradientLayer;
		}

		public override UIStatusBarStyle PreferredStatusBarStyle()
		{
			return UIStatusBarStyle.LightContent;
		}
	}
}
